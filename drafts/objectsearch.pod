=head1 Object searching in Zaaksysteem

Enables users to find any sort of object in Zaaksysteem using an autocomplete
style interface. The functionality is inspired on Apple Spotlight and Google Search.

Every object registers itself in the database. A string representation of the object
is created and saved in a separate column. By making this column part of a base-class
'objectsearch' an up-to-date search index is more easily ensured, because DELETE queries
are managed implicitly.

Every time an object is altered its search index representation is renewed. This is
done by linking a DBix::Class component to the table, and overriding the insert() and
update() routines. The use of having this functionality in the code domain is that 
information from related table can be used in it's search index. E.g. to index a 
zaaktype, its current title is important and that's found in the 'zaaktype_node' table.
This also means that any update on the linked table should lead to a re-index of the
target table.

When the functionality is enabled in an existing Zaaksysteem the indexing script 
/bin/objectsearch_reindex is run. This simply updates every record in each table that 
is search-enabled. The update function must trigger a renewal of the search index entry
as well as an update to any related search-enabled table.


=head2 Phase one

The first phase of search involves maintaining an up-to-date string representation in one
central table (called 'searchable'), and allowing for three basic search routines to use
this information: 
- objectsearch: which searches across all indexed tables
- objectsearch_contact: which searches contacts, namely 'bedrijf' and 
  'natuurlijk_persoon'
- objectsearch_address: supporting finding a city address in the database. since cities have
  a limited number of addresses such a search is useful, finding an address ensures that
  the request is for an address that is under the jurisdiction of the city. (As opposed
  to a nationwide address search)
  
  
=head2 Making a table searchable

- Drop the 'searchable_id' column of searchable. It will be re-added shortly - this 
  is necessary to turn on inheritance.
    ALTER TABLE searchable DROP searchable_id;
    
- Add all other columns of searchable to the new table
    ALTER TABLE new_table ADD search_index tsvector;
    ALTER TABLE new_table ADD search_term text;
    ALTER TABLE new_table ADD object_type varchar(100) DEFAULT 'new_table';
    
- Make the new table inherit 'searchable'
    ALTER TABLE new_table INHERIT searchable;
    
- Recreate the searchable_id for searchable
    ALTER TABLE searchable ADD searchable_id serial;
    
- Create a component class for the new table (if it's not already there)
    vim lib/Zaaksysteem/DB/Component/NewTable.pm

- Override the insert() and update() routines to fill it's search_term field.
    sub insert {
        my ($self) = shift;
        $self->search_term('string representation of my new object goes here');
    }
    
- Configure the OBJECTSEARCH_TABLENAMES table config in Constants.pm to add the 
  results to the search bar. 
  
- Run /bin/objectsearch_reindex to populate the search_term field.


=head2 Configuration per table

- Define which properties should be indexed. This is what's going into the
  search_term field
- Define how an object should look in the search_results
- Define the url of the object, so when it's selected it can be shown

=item Zaak

Zaaktype, Zaaknummer, Aanvrager, Behandelaar, Registratiedatum, Extra informatie,
Trefwoorden, Zaakadres (TODO)

=item Contacten

- Naam
- Adres 
- Burgerservicenummer 
- Geboortedatum 
- E-mailadres

--> natuurlijk_persoon en bedrijf -> hoe wordt dit geimporteerd? Als het via 
non-DBIX class is dan is een update coalesce query beter, als het via dbixclass 
is dan is een component beter.

=item Documenten

For both indexing and display use filename. link goes directly to the document
or to the zaak or queue it's in.

=head2 Why table inheritance?

Advantages of table inheritance:
- Forces an exact format for the table fields
- Forces an application wide unique id for search results
- Possibilities for union querys without gut wrenching code (no need to 
  make table look alike that really don't look alike)
- Possibilities for free-text search with free results ranking. The best match
  between different object types can be found in one query, allowing for paging.



---------------------- working -----------------------------
  
- alleen zoekresultaten die binnen je jurisdictie vallen kun je inzien
    zie oplossing voor zaken zoeken

- in de huidige spotenlighter db zit de bag info van bussum, en de zaken zijn gericht 
    op de bilt. dus adressen worden niet gevonden. actie: db merge afmaken, bussum_beheer
    importeren.

- bij focus() en thumbnail van het item laten zien? na timeout van seconde?

- Contactmomenten, producten, vragen toevoegen

    
wat is nodig om de databases te mergen:
- cleane gegevens database importeren.
- database redeploy_script runnen
- oude schema files deleten
- code doorlopen op referenties naar oude schema (bijv. componentzaakbag)
- config files aanpassen

