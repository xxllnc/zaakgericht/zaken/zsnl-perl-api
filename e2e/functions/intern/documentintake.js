import {
    fillCreateCase
} from './plusMenu';

export const startCaseRegistration = documentName => {
    const intakeDocuments = $$('.object-type-file');

    intakeDocuments
        .filter(intakeDocument =>
            intakeDocument
                .$('.list-view-entity-name')
                .getText()
                .then(name =>
                    name === documentName
                )
        ).each(element =>
            element.$('.intake-document-register').click()
        );
};

export const createCase = ( documentName, data ) => {
    startCaseRegistration(documentName);
    fillCreateCase(data);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
