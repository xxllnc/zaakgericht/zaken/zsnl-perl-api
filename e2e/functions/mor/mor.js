import waitForElement from './../common/waitForElement';
import {
    scroll
} from './../common/mouse';

export const header = $('mor-nav');
export const tabs = $('.mor-nav__tabs');
export const activeTab = $('mor-app ui-view');
export const buttonBar = $('.mor-nav__button-bar');
export const searchUnit = $('.mor-nav__search');
export const results = $$('[data-name="result"] vorm-radio-group label');
export const supportlink = $('.case-list-view__supportlink a');
export const caseGroup = $$('mor-app ui-view .case-group');
export const caseViewButton = $('case-detail-view .case-detail-view__button-container button');
export const caseCompleteButton = $('case-complete-view .case-detail-view__button-container button');
export const loadMore = $('.case-group-loadmore button');
export const searchResultItems = $$('zs-suggestion-list .suggestion');
export const closeCaseButton = $('.case-detail-view__header .mdi-arrow-left');
export const closeCaseCompleteViewButton = $('.casecomplete-modal .case-detail-view__header .mdi-arrow-left');

export const getHeaderText = () => header.$('.mor-nav__title h1').getText();

export const openSearch = () => {
    waitForElement('.mor-nav__button-bar');
    buttonBar
        .$('[data-name="search"]')
        .click();
};

export const enterSearchTerm = searchTerm => {
    searchUnit
        .$('input')
        .sendKeys(searchTerm);
};

export const selectSearchTerm = () => {
    searchResultItems
        .first()
        .click();
};

export const getSearchResults = () =>
    searchResultItems
        .map(searchResultItem =>
            searchResultItem.getText()
        );

export const search = searchTerm => {
    const searchTerms = searchTerm.constructor === Array ? searchTerm : [ searchTerm ];

    openSearch();

    searchTerms.forEach(searchTerm => {
        enterSearchTerm(searchTerm);
        selectSearchTerm();
    });
};

export const closeSearch = () => {
    searchUnit
        .$('.mor-nav__clearbutton')
        .click();
};

export const morLogout = () => {
    buttonBar.
        $('[data-name="logout"]')
        .click();
};

export const openView = view => {
    tabs
        .$(`a[href="/mor/zaken/${view}"]`)
        .click();
};

export const countCaselists = () => activeTab.$$('.case-group').count();

export const listCases = ( view = 1 ) =>
    caseGroup
        .get(view - 1)
        .$$('case-list-item-list-item')
        .map(caseItem =>
            caseItem
                .$('a')
                .getAttribute('href')
                .then(href =>
                    Number(href.match('[0-9]+')[0])
                )
        );

export const loadMoreCases = () => {
    scroll(0, 850);
    loadMore.click();
};

export const openCase = caseNumber => {
    waitForElement('case-list-item-list-item');
    $(`case-list-item-list-item a[href*="/!melding/${caseNumber}/"]`).click();
    waitForElement('.case-detail-view__body');
};

export const clickTransitionButton = () => {
    caseViewButton.click();
};

export const clickCompleteButton = () => {
    caseCompleteButton.click();
};

export const getTransitionButtonText = () =>
    caseViewButton
        .getText()
        .then(text => text.toLowerCase());

export const inputAttribute = ( name, value ) => {
    const attribute = $(`vorm-field[data-label="${name}"]`);

    attribute
        .getAttribute('data-type')
        .then(dataType => {

            switch (dataType) {

                case 'option':
                attribute
                    .$(`input[value="${value}"]`)
                    .click();
                break;

                case 'text':
                attribute
                    .$('input')
                    .sendKeys(value);
                break;

                case 'textarea':
                attribute
                    .$('textarea')
                    .sendKeys(value);
                break;

                default:
                break;
            }
    });
};

export const inputAttributes = attributesInfo => {
    attributesInfo.forEach(attributeInfo => {
        const { name, value } = attributeInfo;

        inputAttribute(name, value);
    });
};

export const clearAttribute = name => {
    const attribute = $(`vorm-field[data-label="${name}"]`);

    attribute
        .getAttribute('data-type')
        .then(dataType => {

            switch (dataType) {

                case 'option':
                //impossible
                break;

                case 'text':
                attribute
                    .$('input')
                    .clear();
                break;

                case 'textarea':
                attribute
                    .$('textarea')
                    .clear();
                break;

                default:
                break;
            }
    });
};

export const inputResult = result => {
    const resultOptions = $$('[data-name="result"] .vorm-radio-option');

    resultOptions
        .each(resultOption =>
            resultOption
                .$('span')
                .getText()
                .then(text => {
                    if ( text === result ) {
                        resultOption.$('input').click();
                    }
                })
        );
};

export const completeCase = ( result, data ) => {
    clickTransitionButton();
    inputAttributes(data);
    inputResult(result);
    clickCompleteButton();
};

export const getValue = attributeName =>
    $$('.case-detail-view__body > .case-detail-view__list .case-detail-view__item')
        .filter(attribute =>
            attribute
                .$('.case-detail-view__label')
                .getText()
                    .then(text =>
                        text === attributeName
                    )
        )
        .first()
        .$('.case-detail-view__value')
        .getText();

export const closeCaseView = () => {
    closeCaseButton.click();
};

export const closeCaseCompleteView = () => {
    closeCaseCompleteViewButton.click();
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
