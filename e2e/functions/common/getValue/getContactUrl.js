export default ( type, number ) => {
    const contactTypes = {
        citizen: 'natuurlijk_persoon',
        organisation: 'bedrijf',
        employee: 'medewerker'
    };

    return `/betrokkene/${number}/?gm=1&type=${contactTypes[type]}`;
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
