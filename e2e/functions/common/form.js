import {
    selectFirstSuggestion
} from './select';
import repeat from './repeat';
import inputDate from './input/inputDate';
import getTrailingNumber from './getValue/getTrailingNumber';

export const registrationForm = $('zs-case-registration');
export const skipOption = $('.case-register-ignore-required-fields');
export const reuseValuesBar = $('zs-case-reuse-values');
export const assignSelf = $('[data-name="me"]');
export const assignCoworker = $('[data-name="coworker"]');
export const assignDepartment = $('[data-name="org-unit"]');
export const requestorEmailAddress = $('[data-name="$email"]');
export const requestorPhoneNumber = $('[data-name="$landline"]');
export const requestorMobileNumber = $('[data-name="$mobile"]');
export const assignmentOptions = $$('.allocation-picker-option');
export const assignmentDepartment = $('vorm-allocation [data-name="department-picker"] select');
export const assignmentRole = $('vorm-allocation [data-name="role-picker"] select');

export const navigate = direction => {
    $(`button[data-name="${direction}"]`).click();
};

export const goNext = ( times = 1 ) => {
    repeat(times, () => navigate('next') );
};

export const goBack = ( times = 1 ) => {
    repeat(times, () => navigate('previous') );
};

export const toggleSkipRequiredAttributes = () => {
    skipOption.click();
};

export const getActiveStep = () =>
    $$('.case-register-breadcrumb li')
        .map((element, index) =>
            element
                .getAttribute('class')
                .then(classes =>
                    classes.indexOf('active') !== -1 ? index + 1 : undefined
                )
        )
        .then(indexes =>
            indexes
                .filter(index =>
                    index
                )
                [0]
        );

export const countRequiredMessages = () =>
    $$('.value-validity-list').count();

export const openRegisteredCase = () => {
    $('zs-snackbar .snack-message-content')
        .getText()
        .then(text => {
            const caseUrl = `/intern/zaak/${getTrailingNumber(text)}`;
            
            browser.get(caseUrl);
        });
};

export const assign = ( type, data ) => {
    $(`vorm-allocation [data-name="${type}"]`).click();

    switch (type) {
    case 'me':
        if ( !data.changeDepartment ) {
            registrationForm
                .$('[data-name="change_department"] input')
                .click();
        }
    break;

    case 'coworker':
        selectFirstSuggestion(registrationForm.$('[data-name="coworker"] .suggestion-input'), data.assignee);
        if ( !data.changeDepartment ) {
            registrationForm
                .$('[data-name="change_department"] input')
                .click();
        }
        if ( !data.sendEmail ) {
            registrationForm
                .$('[data-name="inform_assignee"] input')
                .click();
        }
    break;

    case 'org-unit':
        if ( data.department ) {
            registrationForm
                .$('[data-name="department-picker"] select')
                .sendKeys(data.department);
        }
        if ( data.role ) {
            registrationForm
                .$('[data-name="role-picker"] select')
                .sendKeys(data.role);
        }
    break;

    default:
    break;

    }
};

export const getAssignmentType = () =>
    assignmentOptions
        .filter(assignmentOption =>
            assignmentOption
                .getAttribute('class')
                .then(attributeClass =>
                    attributeClass.includes('selected')
                )
        )
        .first()
        .getAttribute('data-name');

export const getAssignmentDepartment = () =>
    assignmentDepartment
        .$('option:checked')
        .getText();

export const getAssignmentRole = () =>
    assignmentRole
        .$('option:checked')
        .getText();

export const addSubject = data => {
    const relationForm = $('zs-vorm-related-subject-form form');

    $('[data-name="related-subject-add"]').click();

    if ( data.subjectType ) {
        const subjectType = data.subjectType === 'organisation' ? 'bedrijf' : 'natuurlijk_persoon';

        relationForm.$(`[data-name="relation_type"] input[value="${subjectType}"]`).click();
    }

    selectFirstSuggestion(relationForm.$('[data-name="related_subject"] input'), data.subjectToRelate);

    if ( data.role ) {
        relationForm.$('[data-name="related_subject_role"] select').sendKeys(data.role);
    }

    if ( data.roleOther ) {
        relationForm.$('[data-name="related_subject_role_freeform"] input').sendKeys(data.roleOther);
    }

    if ( data.authorisation ) {
        relationForm.$('[data-name="pip_authorized"] input').click();
    }

    if ( data.notification ) {
        relationForm.$('[data-name="notify_subject"] input').click();
    }

    relationForm.submit();
};

export const changeConfidentiality = confidentiality => {
    registrationForm.$('[data-name="$confidentiality"] select').sendKeys(confidentiality);
};

export const getRequestorEmailAddress = () => requestorEmailAddress.$('input').getAttribute('value');

export const getRequestorPhoneNumber = () => requestorPhoneNumber.$('input').getAttribute('value');

export const getRequestorMobileNumber = () => requestorMobileNumber.$('input').getAttribute('value');

export const changeRequestorEmailAddress = emailAddress => {
    requestorEmailAddress.$('input').clear().sendKeys(emailAddress);
};

export const changeRequestorPhoneNumber = phoneNumber => {
    requestorPhoneNumber.$('input').clear().sendKeys(phoneNumber);
};

export const changeRequestorMobileNumber = mobileNumber => {
    requestorMobileNumber.$('input').clear().sendKeys(mobileNumber);
};

export const reuseValues = () => {
    reuseValuesBar.$('button').click();
};

export const setDocumentintakeSettings = settings => {
    const { newDocumentName, description, origin, originDate } = settings;

    if ( newDocumentName ) {
        $('[step-name="file"] [data-name="name"] input').clear().sendKeys(newDocumentName);
    }

    if ( description ) {
        $('[step-name="file"] [data-name="description"] input').clear().sendKeys(description);
    }

    if ( origin ) {
        $('[step-name="file"] [data-name="origin"] select').sendKeys(origin);
    }

    if ( originDate ) {
        inputDate($('[step-name="file"] [data-name="origin_date"]'), originDate);
    }
};

export const setDocumentintakeCasedocument = ( caseDocument ) => {
    $('[step-name="file"] [data-name="case_document"] select').sendKeys(caseDocument);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
