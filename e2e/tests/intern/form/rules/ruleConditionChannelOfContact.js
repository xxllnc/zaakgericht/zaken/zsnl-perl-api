import {
    openPageAs
} from './../../../../functions/common/navigate';
import startForm from './../../../../functions/common/startForm';
import {
    getClosedValue
} from './../../../../functions/common/input/caseAttribute';

const choice = $('[data-name="boolean"]');
const contactchannels = [ 'behandelaar', 'balie', 'telefoon', 'post', 'email', 'webformulier', 'sociale media'];

contactchannels.forEach(contactchannel => {
    describe(`when opening a registration form with contactchannel ${contactchannel}`, () => {
        beforeAll(() => {
            openPageAs();

            const data = {
                casetype: 'Systeemvoorwaarde contactkanaal',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: contactchannel
            };

            startForm(data);
            choice.$('[value="Ja"]').click();
        });

        contactchannels.forEach(contactchannelAttribute => {
            const dataName = contactchannelAttribute.replace(' ', '_');
            const state = contactchannel === contactchannelAttribute ? 'True' : 'False';

            it(`the attribute for ${contactchannelAttribute} should be ${state}`, () => {
                expect(getClosedValue($(`[data-name="contactkanaal_${dataName}"]`))).toEqual(state);
            });
        });
    });
});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
