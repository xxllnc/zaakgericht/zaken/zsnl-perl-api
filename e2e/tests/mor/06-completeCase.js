import {
    openPageAs
} from './../../functions/common/navigate';
import {
    listCases,
    loadMoreCases,
    openCase,
    completeCase,
    getValue,
    closeCaseView,
    clickTransitionButton,
    clickCompleteButton,
    results,
    caseCompleteButton,
    inputAttribute,
    inputResult,
    clearAttribute
} from './../../functions/mor/mor';

describe('when opening the mor app and opening an open case and completing it', () => {

    beforeAll(() => {
        const data = [
            {
                name: 'mor beoordeling',
                value: 'Neutraal'
            },
            {
                name: 'mor terugkoppeling',
                value: 'Dit is test tekst'
            }
        ];

        openPageAs('mor', '/mor/');
        openCase(137);
        completeCase('Melding niet verwerkt', data);
    });

    it('it should redirect to the home page', () => {
        expect(browser.getCurrentUrl()).toMatch('/mor/zaken/open');
    });

    it('the open caseList should not contain the case', () => {
        expect(listCases(1)).not.toContain(137);
    });

    describe('and when opening the case again', () => {
        beforeAll(() => {
            browser.get('/mor/zaken/afgehandeld');
            loadMoreCases();
            openCase(137);
        });
    
        it('the completed values should have been set', () => {
            const attributesInfo = [
                {
                    name: 'mor beoordeling',
                    value: 'Neutraal'
                },
                {
                    name: 'mor terugkoppeling',
                    value: 'Dit is test tekst'
                },
                {
                    name: 'Resultaat',
                    value: 'Melding niet verwerkt'
                }
            ];

            attributesInfo.forEach(attributeInfo => {
                expect(getValue(attributeInfo.name)).toEqual(attributeInfo.value);
            });
        });
    
        afterAll(() => {
            closeCaseView();
        });

    });

});

describe('when opening the mor app and opening an open case and initiating the completion', () => {

    beforeAll(() => {
        openPageAs('mor', '/mor/');
        openCase(138);
        clickTransitionButton();
    });
    
    it('the results should be displayed', () => {
        expect(results.count()).toBe(2);
    });

    it('the button to complete with should be present', () => {
        expect(caseCompleteButton.isPresent()).toBe(true);
    });

    it('the button to complete with should be disabled', () => {
        expect(caseCompleteButton.isEnabled()).toBe(false);
    });

    describe('and when filling out the required attribute', () => {
    
        beforeAll(() => {
            inputAttribute('mor verplicht *', 'verplichte tekst');
        });
    
        it('the button to complete with should still be disabled', () => {
            expect(caseCompleteButton.isEnabled()).toBe(false);
        });
    
        afterAll(() => {
            clearAttribute('mor verplicht *');
        });
    
    });

    describe('and when picking a result', () => {
    
        beforeAll(() => {
            inputResult('Melding niet verwerkt');
            browser.sleep(500);
        });

        it('it should receive the active class', () => {
            expect(results.get(1).getAttribute('class')).toContain('active');
        });
    
        it('the button to complete with should still be disabled', () => {
            expect(caseCompleteButton.isEnabled()).toBe(false);
        });
    
    });

    describe('and when filling out the required attribute and picking a result', () => {
    
        beforeAll(() => {
            inputAttribute('mor verplicht *', 'verplichte tekst');
            inputResult('Melding niet verwerkt');
        });
    
        it('the button to complete with should be enabled', () => {
            expect(caseCompleteButton.isEnabled()).toBe(true);
        });

        describe('and when completing', () => {

            beforeAll(() => {
                clickCompleteButton();
            });
        
            it('it should redirect to the home page', () => {
                expect(browser.getCurrentUrl()).toMatch('/mor/zaken/open');
            });

            it('the open caseList should not contain the case', () => {
                expect(listCases(1)).not.toContain(138);
            });
        
        });
    
    });

});

describe('when opening the mor app and opening an open case and trying to complete it without all required attributes visible', () => {

    beforeAll(() => {
        openPageAs('mor', '/mor/');
        openCase(139);
        clickTransitionButton();
    });

    it('should display a warning', () => {
        expect($('.case-detail-view__field-config-error-message').isDisplayed()).toBe(true);
    });

});

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
