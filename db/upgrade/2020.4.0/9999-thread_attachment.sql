BEGIN;

ALTER TABLE thread ADD COLUMN attachment_count INTEGER NOT NULL DEFAULT 0;
ALTER TABLE thread_message_external ADD COLUMN attachment_count INTEGER NOT NULL DEFAULT 0;


UPDATE thread SET attachment_count = (
    SELECT count(1)
    FROM   thread_message_attachment t_m_a
    JOIN   thread_message t_m ON t_m.id = t_m_a.thread_message_id
    WHERE  t_m.thread_id = thread.id)
;

UPDATE thread_message_external SET attachment_count = (
    SELECT count(1)
    FROM   thread_message_attachment t_m_a
    JOIN   thread_message t_m ON t_m.id = t_m_a.thread_message_id
    WHERE  t_m.thread_message_external_id = thread_message_external.id)
;

COMMIT;