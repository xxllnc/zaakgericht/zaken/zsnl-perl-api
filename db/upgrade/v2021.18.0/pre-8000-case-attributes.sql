BEGIN;

  DROP VIEW IF EXISTS case_attributes CASCADE;
  CREATE VIEW case_attributes AS
    SELECT
      z.id AS case_id,
      COALESCE(zk.value, '{}') as value,
      bk.magic_string as magic_string,
      bk.id as library_id,
      bk.value_type as value_type,
      bk.type_multiple as mvp
    FROM zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON
      (bk.id = ztk.bibliotheek_kenmerken_id AND bk.value_type NOT IN ('file', 'appointment'))
    LEFT JOIN
      zaak_kenmerk zk
    ON
      (z.id = zk.zaak_id AND zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id)
    GROUP BY 1,2,3,4,5
 ;

  DROP VIEW IF EXISTS case_attributes_v1 CASCADE;

  CREATE VIEW case_attributes_v1 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_jsonb(value, value_type) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        appointment_attribute_value_to_jsonb(value, reference) as value
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        attribute_value_to_jsonb(value, 'file')
      FROM case_documents
  ;

  DROP VIEW IF EXISTS case_attributes_v0 CASCADE;

  CREATE VIEW case_attributes_v0 AS
    SELECT
      case_id,
      magic_string,
      library_id,
      attribute_value_to_v0(value, value_type, mvp) as value
    FROM
      case_attributes
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        to_jsonb(reference::text)
      FROM
        case_attributes_appointments
    UNION ALL
      SELECT
        case_id,
        magic_string,
        library_id,
        attribute_value_to_v0(value, 'file', true)
      FROM case_documents
  ;

COMMIT;
