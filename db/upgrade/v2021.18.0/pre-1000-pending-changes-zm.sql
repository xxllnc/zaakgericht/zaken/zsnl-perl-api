BEGIN;

  ALTER TABLE zaak_meta ADD COLUMN pending_changes jsonb not null default '{}'::jsonb;

COMMIT;
