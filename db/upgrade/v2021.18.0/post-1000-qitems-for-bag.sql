
BEGIN;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'update_bag_cache_for_migration',
    CONCAT('BAG: Populate BAG cache for case ', zk.zaak_id),
    950,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', zk.zaak_id,
      'library_id', bk.id
    )
  FROM
  zaak_kenmerk zk
    join bibliotheek_kenmerken bk
    on zk.bibliotheek_kenmerken_id = bk.id
    where bk.value_type in ('bag_adres', 'bag_adressen', 'bag_openbareruimte', 'bag_openbareruimtes', 'bag_straat_adres', 'bag_straat_adressen') and zk.value != '{}'

  ;

COMMIT;
