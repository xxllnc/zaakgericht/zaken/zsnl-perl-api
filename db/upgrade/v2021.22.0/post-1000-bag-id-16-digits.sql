
BEGIN;

  UPDATE zaak_bag SET bag_id = lpad(bag_id, 16, '0') where length(bag_id) < 16;

COMMIT;
