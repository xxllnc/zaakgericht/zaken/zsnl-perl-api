BEGIN;

  CREATE OR REPLACE FUNCTION update_requestor(
    IN gm_id int,
    IN type text
  )
  RETURNS void
  LANGUAGE plpgsql
  AS $$
  BEGIN

    UPDATE zaak SET requestor_v1_json = NULL,
      last_modified = NOW()
      WHERE status in ('open', 'new', 'stalled')
      AND aanvrager IN (
        SELECT id from zaak_betrokkenen
        WHERE gegevens_magazijn_id = gm_id
        AND betrokkene_type = type
      );

  END;
  $$;

  CREATE OR REPLACE FUNCTION update_case_requestor_np()
  RETURNS TRIGGER LANGUAGE 'plpgsql'
  AS $$
  BEGIN
    PERFORM update_requestor(NEW.id, 'natuurlijk_persoon');
    RETURN NULL;
  END;
  $$;

  DROP TRIGGER IF EXISTS update_case_requestor_np ON "natuurlijk_persoon";

  CREATE TRIGGER update_case_requestor_np
     AFTER UPDATE
     ON natuurlijk_persoon
     FOR EACH ROW
     EXECUTE PROCEDURE update_case_requestor_np();

  CREATE OR REPLACE FUNCTION update_case_requestor_company()
  RETURNS TRIGGER LANGUAGE 'plpgsql'
  AS $$
  BEGIN
    PERFORM update_requestor(NEW.id, 'bedrijf');
    RETURN NULL;
  END;
  $$;

  DROP TRIGGER IF EXISTS update_case_requestor_company ON "bedrijf";

  CREATE TRIGGER update_case_requestor_company
     AFTER UPDATE
     ON bedrijf
     FOR EACH ROW
     EXECUTE PROCEDURE update_case_requestor_company();

  CREATE OR REPLACE FUNCTION update_case_requestor_contact_details()
  RETURNS TRIGGER LANGUAGE 'plpgsql'
  AS $$
  BEGIN
    IF NEW.betrokkene_type = 1
    THEN
    PERFORM update_requestor(NEW.gegevens_magazijn_id, 'natuurlijk_persoon');
    ELSIF NEW.betrokkene_type = 2
    THEN
      PERFORM update_requestor(NEW.gegevens_magazijn_id, 'bedrijf');
    END IF;
    RETURN NULL;
  END;
  $$;

  DROP TRIGGER IF EXISTS update_case_requestor_contact_details ON "contact_data";
  CREATE TRIGGER update_case_requestor_contact_details
     AFTER UPDATE
     ON contact_data
     FOR EACH ROW
     EXECUTE PROCEDURE update_case_requestor_contact_details();

COMMIT;
