BEGIN;

INSERT INTO municipality_code (
    dutch_code,
    label,
    uuid)
VALUES (
    1992,
    'Voorne aan Zee',
    'd091d725-5aac-4cc7-b7f9-01b6aeeda79f');

UPDATE municipality_code SET historical = true where label IN ('Brielle', 'Hellevoetsluis', 'Westvoorne');

COMMIT;
