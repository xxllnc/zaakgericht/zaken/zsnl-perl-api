BEGIN;

    CREATE INDEX IF NOT EXISTS queue_priority_idx ON queue(priority);
    CREATE INDEX IF NOT EXISTS queue_object_id_idx ON queue(object_id);

COMMIT;
