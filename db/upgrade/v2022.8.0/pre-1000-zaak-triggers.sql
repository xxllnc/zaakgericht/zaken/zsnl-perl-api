BEGIN;

  CREATE OR REPLACE FUNCTION update_phase_term_in_zaak_meta(
    IN case_id int,
    IN orig timestamp,
    IN becomes timestamp
  )
  RETURNS void
  LANGUAGE plpgsql
  AS $$
  DECLARE
    diff int;
    cur jsonb;
    keys int;
  BEGIN

    select into cur current_deadline from zaak_meta where zaak_id = case_id;
    -- Python creates a case with null values ignoring the default *sigh*
    IF cur IS NULL
    THEN
      RETURN;
    END IF;

    -- We can't update the whole lot if we don't have the correct data
    -- structure. We need 5, says, start, current, phase_id and phase_no
    select into keys array_length(array_agg(k), 1) from jsonb_object_keys(cur) as k;
    IF keys IS NULL OR keys < 5
    THEN
      RETURN;
    END IF;

    diff := ((cur->>'days')::int + date_part('day', (becomes - orig)::interval))::int;
    -- If we have positive days, we make that the new terms
    -- Otherwise, we exceeded our terms already and a negative just doesn't
    -- make sense. So we make it equal to the current days, meaning, it needs
    -- to be dealt with straight away
    IF diff > (cur->>'current')::int
    THEN
      UPDATE zaak_meta SET current_deadline = jsonb_set(current_deadline, '{days}', diff::text::jsonb) where zaak_id = case_id;
    ELSE
      UPDATE zaak_meta SET current_deadline = jsonb_set(current_deadline, '{days}', (current_deadline->>'current')::jsonb) where zaak_id = case_id;
    END IF;

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION update_phase_terms()
  RETURNS trigger
  LANGUAGE plpgsql
  AS $$
  BEGIN

    -- When a case comes from stalled we don't want to update the phase terms
    -- as this is dealt with in the code itself
    IF OLD.status = 'stalled'
    THEN
      RETURN NEW;
    END IF;

    IF NEW.streefafhandeldatum IS NOT NULL AND OLD.streefafhandeldatum IS NOT NULL
    THEN
      PERFORM update_phase_term_in_zaak_meta(NEW.id, OLD.streefafhandeldatum, NEW.streefafhandeldatum);
    END IF;
    RETURN NEW;
  END;
  $$;

  DROP TRIGGER IF EXISTS update_phase_term ON zaak;
  CREATE TRIGGER update_phase_term
     AFTER UPDATE
      OF streefafhandeldatum
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE update_phase_terms();

  DROP TRIGGER IF EXISTS update_zaak_percentage ON zaak;
  CREATE TRIGGER update_zaak_percentage
     BEFORE INSERT OR UPDATE
      OF milestone, zaaktype_node_id
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE update_zaak_percentage();

COMMIT;
