BEGIN;
  ALTER TABLE file ADD COLUMN intake_group_id INTEGER;
  ALTER TABLE file ADD CONSTRAINT file_intake_group_id_fk FOREIGN KEY (intake_group_id) REFERENCES groups (id) ON DELETE SET NULL;
  ALTER TABLE file ADD COLUMN intake_role_id INTEGER;
  ALTER TABLE file ADD CONSTRAINT file_intake_role_id_fk FOREIGN KEY (intake_role_id) REFERENCES roles (id) ON DELETE SET NULL;
COMMIT;
