BEGIN;

CREATE TABLE public.thread_message_external
(
    id SERIAL PRIMARY KEY,
    "type" text NOT NULL,
    content text NOT NULL,
    subject text NOT NULL,
    CONSTRAINT external_message_type CHECK ("type" IN ('pip', 'email'))
);

CREATE TABLE public.thread_message_contact_moment
(
    id SERIAL PRIMARY KEY,
    content text NOT NULL,
    contact_channel text NOT NULL,
    direction text, 
    recipient_uuid uuid NOT NULL,
    recipient_displayname text NOT NULL,
    CONSTRAINT contact_moment_channels CHECK (contact_channel IN  ('assignee', 'frontdesk', 'phone',  'mail', 'email', 'webform', 'social_media', 'external_application')),
    CONSTRAINT message_direction_check CHECK (direction IN ('incoming', 'outgoing'))
);

CREATE TABLE public.thread_message_note
(
    id SERIAL PRIMARY KEY,
    content text NOT NULL
);

CREATE TABLE public.thread
(
    id SERIAL PRIMARY KEY,
    uuid uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    contact_uuid uuid,
    contact_displayname text,
    case_id integer,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,    
    thread_type text NOT NULL,
    last_message_cache text NOT NULL,
    FOREIGN KEY (case_id)
        REFERENCES public.zaak (id),
    CONSTRAINT contact_or_case_id_not_null_check CHECK (contact_uuid IS NOT NULL OR case_id IS NOT NULL) NOT VALID,
    CONSTRAINT message_type_check CHECK ( thread_type IN ('note', 'contact_moment', 'external'))
);

CREATE TABLE public.thread_message
(   
    id SERIAL PRIMARY KEY,
    uuid uuid NOT NULL UNIQUE DEFAULT uuid_generate_v4(),
    thread_id integer NOT NULL REFERENCES public.thread(id),
    type text NOT NULL,
    message_slug text NOT NULL, 
    created_by_uuid uuid NOT NULL,
    created_by_displayname text NOT NULL,
    created timestamp without time zone NOT NULL,
    last_modified timestamp without time zone,
    thread_message_note_id integer UNIQUE REFERENCES public.thread_message_note (id),
    thread_message_contact_moment_id integer UNIQUE REFERENCES public.thread_message_contact_moment (id),
    thread_message_external_id integer UNIQUE REFERENCES public.thread_message_external (id),
    CONSTRAINT message_type_check CHECK (type IN ('note', 'contact_moment', 'external')),
    CONSTRAINT check_exactly_one_column CHECK ( 
        ( CASE WHEN thread_message_note_id IS NULL THEN 0 ELSE 1 END
        + CASE WHEN thread_message_contact_moment_id IS NULL THEN 0 ELSE 1 END
        + CASE WHEN thread_message_external_id IS NULL THEN 0 ELSE 1 END) = 1)
);

CREATE TRIGGER thread_insert_trigger
    BEFORE INSERT
    ON public.thread
    FOR EACH ROW
    EXECUTE PROCEDURE public.insert_timestamps();


CREATE TRIGGER thread_update_trigger
    BEFORE UPDATE 
    ON public.thread
    FOR EACH ROW
    EXECUTE PROCEDURE public.update_timestamps();

CREATE TRIGGER thread_message_insert_trigger
    BEFORE INSERT
    ON public.thread_message
    FOR EACH ROW
    EXECUTE PROCEDURE public.insert_timestamps();


CREATE TRIGGER thread_message_update_trigger
    BEFORE UPDATE 
    ON public.thread_message
    FOR EACH ROW
    EXECUTE PROCEDURE public.update_timestamps();

CREATE INDEX thread_message_id_idx ON thread_message(thread_id);
CREATE INDEX thread_message_contact_moment_id_idx ON thread_message(thread_message_contact_moment_id);
CREATE INDEX thread_message_note_id_idx ON thread_message(thread_message_note_id);
CREATE INDEX thread_message_external_id_idx ON thread_message(thread_message_external_id);

CREATE INDEX thread_case_id_idx ON thread(case_id);
CREATE INDEX thread_contact_uuid_idx ON thread(contact_uuid);

COMMIT;
