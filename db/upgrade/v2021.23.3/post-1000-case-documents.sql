BEGIN;
INSERT INTO case_documents_cache (
  case_id,
  value,
  magic_string,
  library_id
)
SELECT
  me.case_id,
  me.value,
  me.magic_string,
  me.library_id
FROM
  case_documents me
where
  me.case_id in (
    select
      distinct (
        case_id
      )
    from
      case_documents
    where
      case_id not in (
        select
          case_id
        from
          case_documents_cache
      )
    limit 500
  )
;
COMMIT;

