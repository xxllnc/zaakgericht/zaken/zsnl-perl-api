BEGIN;
	ALTER TABLE custom_object_type_version ADD COLUMN subtitle text;
	ALTER TABLE custom_object_version ADD COLUMN subtitle text;
COMMIT;
