BEGIN;

  CREATE OR REPLACE FUNCTION get_date_progress_from_case(
    IN zaak hstore,
    OUT percentage text
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    completion_date timestamp with time zone;
    completion_epoch bigint;
    start_epoch bigint;
    target_epoch bigint;

    current_difference bigint;
    max_difference bigint;

    perc bigint;
  BEGIN

    percentage := '';
    IF zaak->'status' = 'stalled'
    THEN
      RETURN;
    END IF;

    IF zaak->'afhandeldatum' IS NOT NULL
    THEN
      completion_date := (zaak->'afhandeldatum')::timestamp without time zone;
    ELSE
      SELECT INTO completion_date NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date);
    SELECT INTO start_epoch date_part('epoch', (zaak->'registratiedatum')::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', (zaak->'streefafhandeldatum')::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    perc := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));
    percentage := perc::text;

    RETURN;
  END;
  $$;


COMMIT;
