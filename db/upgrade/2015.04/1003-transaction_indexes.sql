BEGIN;

CREATE INDEX transaction_date_created_idx ON transaction ( date_created desc ) WHERE date_deleted is null;
CREATE INDEX transaction_record_to_object_transaction_record_id_idx ON transaction_record_to_object(transaction_record_id);

COMMIT;
