BEGIN;
    DROP INDEX IF EXISTS object_data_hstore_to_timestamp_idx;
    DROP INDEX IF EXISTS object_data_hstore_to_timestamp_idx2;

    CREATE INDEX object_data_hstore_to_timestamp_idx ON object_data(hstore_to_timestamp(index_hstore->'case.date_of_completion'));
    CREATE INDEX object_data_hstore_to_timestamp_idx2 ON object_data(CAST(hstore_to_timestamp(index_hstore->'case.date_of_completion') AS DATE));
COMMIT;
