BEGIN;

-- Timestamp signifies the END datetime of the lock
ALTER TABLE file ADD COLUMN lock_timestamp TIMESTAMP WITHOUT TIME ZONE NULL;

-- Subject UUID identifier, not bound to any table yet since subjects are floating objects
ALTER TABLE file ADD COLUMN lock_subject_id UUID NULL;

-- Displayname of subject goes here
ALTER TABLE file ADD COLUMN lock_subject_name TEXT NULL;

-- If a lock is set, require subject fields to be filled
ALTER TABLE file ADD CONSTRAINT lock_fields_check CHECK (
    lock_timestamp IS NULL OR (lock_subject_id IS NOT NULL AND lock_subject_name IS NOT NULL)
);

COMMIT;
