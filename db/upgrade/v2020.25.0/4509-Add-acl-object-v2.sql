BEGIN;
DROP VIEW IF EXISTS custom_object_type_acl;

CREATE VIEW custom_object_type_acl AS
SELECT custom_object_type_id, 
roles.id as role_id, 
groups.id as group_id, 
"authorization" 
FROM 
(								  
    SELECT 
        id AS custom_object_type_id, 
        role::json ->> 'uuid' AS role_uuid, 
        department::json ->> 'uuid' AS group_uuid, 
        "authorization"
    FROM custom_object_type CROSS JOIN LATERAL jsonb_to_recordset(authorization_definition->'authorizations') AS t("authorization" text, role json, department json)
)
AS authorization_subquery
JOIN roles ON roles.uuid::text = role_uuid
JOIN groups ON groups.uuid::text = group_uuid;

COMMIT;