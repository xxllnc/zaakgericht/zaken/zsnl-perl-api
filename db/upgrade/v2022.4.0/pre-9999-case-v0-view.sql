DROP INDEX IF EXISTS zaak_case_acl_assignee_idx;
DROP INDEX IF EXISTS zaak_case_acl_requestor_idx;
DROP INDEX IF EXISTS zaak_case_v0_idx;
DROP INDEX IF EXISTS zaak_case_v0_limit_idx;
DROP INDEX IF EXISTS zaak_case_v0_sub_idx;
DROP INDEX IF EXISTS zaaktype_authorisation_case_acl_idx;
DROP INDEX IF EXISTS zaaktype_case_v0_idx;
DROP INDEX IF EXISTS zaaktype_node_case_v0_idx;
DROP INDEX IF EXISTS zaaktype_status_case_v0_idx;
DROP INDEX IF EXISTS zaak_group_idx;
DROP INDEX IF EXISTS zaak_role_idx;
DROP INDEX IF EXISTS zaak_betrokkene_recipient_idx;
DROP INDEX IF EXISTS zaak_betrokkene_aanvrager_idx;
DROP INDEX IF EXISTS zaaktype_node_zaaktype_definitie_id_idx;
DROP INDEX IF EXISTS zaak_open_idx;
DROP INDEX IF EXISTS open_zaak_idx;
DROP INDEX IF EXISTS case_open_nodes_idx;
DROP INDEX IF EXISTS zaak_open_nodes_idx;
DROP INDEX IF EXISTS zaaktype_category_idx;
DROP INDEX IF EXISTS subject_in_group_idx;

CREATE INDEX zaak_open_nodes_idx ON zaak (id, zaaktype_id, zaaktype_node_id) WHERE deleted IS NULL;
CREATE INDEX zaak_betrokkene_recipient_idx on zaak_betrokkenen(zaak_id) WHERE rol = 'Ontvanger' AND deleted IS NULL;
CREATE INDEX zaak_betrokkene_aanvrager_idx on zaak_betrokkenen(zaak_id, id) WHERE deleted IS NULL;

CREATE INDEX
  zaak_case_v0_idx
ON
  zaak(
    id,
    zaaktype_node_id,
    zaaktype_id,
    resultaat_id,
    milestone,
    route_ou,
    route_role,
    behandelaar_gm_id,
    pid,
    aanvrager,
    locatie_zaak
  )
WHERE
  deleted IS NULL;

CREATE INDEX zaaktype_status_case_v0_idx on zaaktype_status(
  zaaktype_node_id,
  status
);

CREATE INDEX zaaktype_category_idx ON
  zaaktype(id, bibliotheek_categorie_id);

CREATE INDEX zaaktype_authorisation_case_acl_idx
ON
  zaaktype_authorisation(
    zaaktype_id,
    confidential,
    role_id,
    ou_id,
    recht
  );

CREATE INDEX zaak_case_acl_requestor_idx ON
  zaak(aanvrager_gm_id) WHERE aanvrager_type = 'medewerker';

CREATE INDEX zaak_case_acl_assignee_idx ON
  zaak(behandelaar_gm_id);

CREATE INDEX zaaktype_node_zaaktype_definitie_id_idx on zaaktype_node(zaaktype_definitie_id);
CREATE INDEX subject_in_group_idx ON subject(id, group_ids);

CREATE INDEX zaak_open_idx ON zaak(id) WHERE deleted is NULL;

BEGIN;

  DROP VIEW IF EXISTS case_v0;

  CREATE VIEW case_v0 AS
  SELECT

    -- header info
    z.uuid AS id,
    z.id AS object_id,

    -- expensive bits
    build_case_v0_values(
      hstore(z),
      hstore(zm),
      hstore(zt),
      hstore(ztr),
      hstore(zts),
      hstore(zts_next),
      hstore(rpt),
      hstore(ztd),
      hstore(gr),
      hstore(ro),
      hstore(case_location),
      hstore(parent),
      hstore(requestor),
      hstore(recipient),
      bc.naam,
      gassign.name,
      ztn.v0_json
    ) as values,

    build_case_v0_case(hstore(zt), hstore(zm)) as case,

    -- static values
    '{}'::text[] as related_objects,
    'case' as object_type


  FROM zaak z
  JOIN
    zaak_meta zm
  ON
    z.id = zm.zaak_id
  JOIN
    zaaktype zt
  ON
    z.zaaktype_id = zt.id
  INNER JOIN
    zaaktype_node ztn
  ON
    z.zaaktype_node_id = ztn.id
  LEFT JOIN
    zaaktype_resultaten ztr
  ON
    z.resultaat_id = ztr.id
  LEFT JOIN
    zaaktype_status zts
  ON
    ( z.zaaktype_node_id = zts.zaaktype_node_id
      AND zts.status = z.milestone)
  LEFT JOIN
    zaaktype_status zts_next
  ON
    ( z.zaaktype_node_id = zts_next.zaaktype_node_id
      AND zts_next.status = z.milestone + 1)
  LEFT JOIN
    result_preservation_terms rpt
  ON
    ztr.bewaartermijn = rpt.code
  LEFT JOIN
    bibliotheek_categorie bc
  ON
    zt.bibliotheek_categorie_id = bc.id
  LEFT JOIN
    zaaktype_definitie ztd
  ON
    ztn.zaaktype_definitie_id = ztd.id
  LEFT JOIN
    groups gr
  ON
    z.route_ou = gr.id
  LEFT JOIN
    roles ro
  ON
    z.route_role = ro.id
  LEFT JOIN
    zaak_bag case_location
  ON
    (case_location.id = z.locatie_zaak and z.id = case_location.zaak_id)
  LEFT JOIN
    subject assignee
  ON
    z.behandelaar_gm_id = assignee.id
  LEFT JOIN groups gassign
  ON
    assignee.group_ids[1] = gassign.id
  LEFT JOIN
    zaak parent
  ON
    z.pid = parent.id
  LEFT JOIN
    zaak_betrokkenen requestor
  ON
    (z.aanvrager = requestor.id and z.id = requestor.zaak_id and requestor.deleted IS NULL)
  LEFT JOIN
    zaak_betrokkenen recipient
  ON
    (z.id = recipient.zaak_id AND recipient.rol = 'Ontvanger' AND recipient.deleted IS NULL)
  WHERE z.deleted IS NULL
  ;

COMMIT;
