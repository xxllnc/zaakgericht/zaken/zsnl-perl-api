BEGIN;
DROP VIEW IF EXISTS VIEW_CASE_V2;
CREATE VIEW view_case_v2 AS
SELECT
    z.id,
    z.uuid,
    z.onderwerp,
    z.route_ou,
    z.route_role,
    z.vernietigingsdatum,
    z.archival_state,
    z.status,
    z.contactkanaal,
    z.created,
    z.registratiedatum,
    z.streefafhandeldatum,
    z.afhandeldatum,
    z.stalled_until,
    z.milestone,
    z.last_modified,
    z.behandelaar_gm_id,
    z.coordinator_gm_id,
    z.aanvrager,
    z.aanvraag_trigger,
    z.onderwerp_extern,
    z.resultaat,
    z.resultaat_id,
    z.payment_amount,
    z.behandelaar,
    z.coordinator,
    z.urgency,
    z.preset_client,
    z.prefix,
    z.number_master,
    z.html_email_template,
    z.payment_status,
    z.dutch_price AS price,
    z.zaaktype_node_id,
    z.pid AS number_parent,
    z.vervolg_van AS number_previous,
    z.leadtime AS lead_time_real,
    z.aanvrager_gm_id,
    z.aanvrager_type,
    z.zaaktype_id,
    z.deleted,
    z.confidentiality,
    zm.current_deadline,
    zm.deadline_timeline,
    ztr.id AS result_id,
    ztr.resultaat AS result,
    ztr.selectielijst AS active_selection_list,
    CASE WHEN z.status = 'stalled'::text THEN
        zm.opschorten
    ELSE
        NULL::character varying
    END AS suspension_rationale,
    CASE WHEN z.status = 'resolved'::text THEN
        zm.afhandeling
    ELSE
        NULL::character varying
    END AS premature_completion_rationale,
    'Dossier'::text AS aggregation_scope,
    ztr.archiefnominatie AS type_of_archiving,
    rpt.label AS period_of_preservation,
    ztr.label AS result_description,
    ztr.comments AS result_explanation,
    ztr.properties::jsonb -> 'selectielijst_nummer'::text AS result_selection_list_number,
    ztr.properties::jsonb -> 'procestype_nummer'::text AS result_process_type_number,
    ztr.properties::jsonb -> 'procestype_naam'::text AS result_process_type_name,
    ztr.properties::jsonb -> 'procestype_omschrijving'::text AS result_process_type_description,
    ztr.properties::jsonb -> 'procestype_toelichting'::text AS result_process_type_explanation,
    ztr.properties::jsonb -> 'procestype_object'::text AS result_process_type_object,
    ztr.properties::jsonb -> 'procestype_generiek'::text AS result_process_type_generic,
    ztr.properties::jsonb -> 'herkomst'::text AS result_origin,
    ztr.properties::jsonb -> 'procestermijn'::text AS result_process_term,
    z.streefafhandeldatum::date - COALESCE(z.afhandeldatum::date, now()::date) AS days_left,
    get_date_progress_from_case (hstore (z.*)) AS progress_days,
    ARRAY (
        SELECT
            json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'value', zaak_kenmerk.value, 'type', bibliotheek_kenmerken.value_type)
        FROM
            zaak_kenmerk
            JOIN bibliotheek_kenmerken ON zaak_kenmerk.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id
        WHERE
            zaak_kenmerk.zaak_id = z.id) AS custom_fields,
    ARRAY (
        SELECT
            json_build_object('status', zs.status, 'type', ca.type, 'data', ca.data, 'automatic', ca.automatic)
        FROM
            case_action ca
            JOIN zaaktype_status zs ON zs.id = ca.casetype_status_id
        WHERE
            ca.case_id = z.id) AS case_actions,
    is_destructable (z.vernietigingsdatum::timestamp with time zone) AS destructable,
    COALESCE((
        SELECT
            string_agg(concat(f.name, f.extension, ' (', f.document_status, ')'), ', '::text) AS string_agg FROM file f
    WHERE
        f.case_id = z.id
        AND f.date_deleted IS NULL
        AND f.active_version = TRUE), ''::text) AS documents,
    COALESCE((
        SELECT
            string_agg(concat(case_documents.name, case_documents.extension, ' (', case_documents.document_status, ')'), ', '::text) AS string_agg FROM file case_documents
    JOIN file_case_document fcd ON fcd.case_id = case_documents.case_id
        AND case_documents.id = fcd.file_id
    WHERE
        case_documents.case_id = z.id), ''::text) AS case_documents,
    json_build_object('uuid', ro.uuid, 'name', ro.name, 'description', ro.description, 'parent_uuid', gr.uuid, 'parent_name', gr.name) AS case_role,
    json_build_object('opschorten', zm.opschorten, 'afhandeling', zm.afhandeling, 'stalled_since', zm.stalled_since, 'unaccepted_files_count', zm.unaccepted_files_count, 'unaccepted_attribute_update_count', zm.unaccepted_attribute_update_count) AS case_meta,
    json_build_object('id', zts.id, 'zaaktype_node_id', zts.zaaktype_node_id, 'status', zts.status, 'status_type', zts.status_type, 'naam', zts.naam, 'created', zts.created, 'last_modified', zts.last_modified, 'ou_id', zts.ou_id, 'role_id', zts.role_id, 'checklist', zts.checklist, 'fase', zts.fase, 'role_set', zts.role_set) AS case_status,
    (
        SELECT
            json_build_object('uuid', grp.uuid, 'name', grp.name, 'description', grp.description, 'parent_uuid', grp_alias.uuid, 'parent_name', grp_alias.name)
        FROM
            GROUPS grp
        LEFT OUTER JOIN GROUPS grp_alias ON (array_length(grp.path, 1) > 1
            AND grp.path[(array_upper(grp.path, 1) - 1)] = grp_alias.id)
WHERE
    z.route_ou = grp.id) AS case_department,
ARRAY (
    SELECT
        json_build_object('betrokkene_type', zb.betrokkene_type, 'subject_id', zb.subject_id, 'rol', zb.rol)
    FROM
        zaak_betrokkenen zb
    WHERE
        zb.zaak_id = z.id
        AND zb.deleted IS NULL
        AND zb.id <> z.aanvrager
        AND zb.id <> z.behandelaar
        AND zb.id <> z.coordinator) AS case_subjects,
ARRAY ((
        SELECT
            json_build_object('naam', bibliotheek_kenmerken.naam, 'magic_string', bibliotheek_kenmerken.magic_string, 'type', bibliotheek_kenmerken.value_type, 'value', ARRAY ((
                    SELECT
                        json_build_object('md5', filestore.md5, 'size', filestore.size, 'uuid', filestore.uuid, 'filename', file.name || file.extension, 'mimetype', filestore.mimetype, 'is_archivable', filestore.is_archivable, 'original_name', filestore.original_name, 'thumbnail_uuid', filestore.thumbnail_uuid)
                    FROM file_case_document
                    JOIN file ON file_case_document.file_id = file.id
                        AND file_case_document.case_id = z.id
                    JOIN filestore ON file.filestore_id = filestore.id
                    WHERE
                        file_case_document.bibliotheek_kenmerken_id = zaaktype_kenmerken.bibliotheek_kenmerken_id)))
        FROM
            zaaktype_kenmerken
            JOIN bibliotheek_kenmerken ON zaaktype_kenmerken.bibliotheek_kenmerken_id = bibliotheek_kenmerken.id
                AND bibliotheek_kenmerken.value_type = 'file'
            JOIN zaaktype_node ON zaaktype_node.id = zaaktype_kenmerken.zaaktype_node_id
        WHERE
            zaaktype_node.id = z.zaaktype_node_id)) AS file_custom_fields
FROM
    zaak z
    LEFT JOIN zaak_meta zm ON zm.zaak_id = z.id
    LEFT JOIN zaaktype_resultaten ztr ON z.resultaat_id = ztr.id
    LEFT JOIN result_preservation_terms rpt ON ztr.bewaartermijn = rpt.code
    LEFT JOIN zaaktype_status zts ON z.zaaktype_node_id = zts.zaaktype_node_id
        AND zts.status = (z.milestone + 1)
    LEFT JOIN roles ro ON z.route_role = ro.id
    LEFT JOIN GROUPS gr ON ro.parent_group_id = gr.id
WHERE
    z.deleted IS NULL;
COMMIT;

