BEGIN;

  CREATE OR REPLACE FUNCTION timestamp_to_perl_datetime(dt timestamp with time zone)
  RETURNS text
  IMMUTABLE
  LANGUAGE plpgsql
  AS $$
  BEGIN
    RETURN to_char(dt::timestamp with time zone at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"');
  END;
  $$;

  CREATE OR REPLACE FUNCTION timestamp_to_perl_datetime(dt text)
  RETURNS text
  IMMUTABLE
  LANGUAGE plpgsql
  AS $$
  BEGIN
    RETURN timestamp_to_perl_datetime(dt::timestamp with time zone at time zone 'UTC');
  END;
  $$;

COMMIT;
