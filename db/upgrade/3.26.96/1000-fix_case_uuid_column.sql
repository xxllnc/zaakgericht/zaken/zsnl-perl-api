BEGIN;

    ALTER TABLE zaak ALTER COLUMN uuid DROP NOT NULL;
    ALTER TABLE zaak ALTER COLUMN uuid DROP DEFAULT;

    UPDATE zaak SET uuid = (SELECT uuid FROM object_data WHERE object_class = 'case' AND object_id = zaak.id);

    ALTER TABLE zaak DROP CONSTRAINT IF EXISTS zaak_object_data_uuid_fkey;
    ALTER TABLE zaak ADD CONSTRAINT zaak_object_data_uuid_fkey FOREIGN KEY (uuid) REFERENCES object_data(uuid);

COMMIT;
