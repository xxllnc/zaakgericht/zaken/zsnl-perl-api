BEGIN;

    -- Execute post release

    INSERT INTO queue (
        type,
        label,
        priority,
        metadata,
        data
    )
    SELECT
        'touch_casetype',
        'Devops: nieuw contactkanaal',
        3000,
        '{"require_object_model":1, "disable_acl": 1, "target":"backend"}',
        '{"id":' || id || '}'
    FROM
        zaaktype
    WHERE active = true;

COMMIT;

