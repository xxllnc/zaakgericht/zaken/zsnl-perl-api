BEGIN;

    ALTER TABLE zaaktype_node ADD COLUMN uuid UUID DEFAULT uuid_generate_v4() NOT NULL;
    CREATE UNIQUE INDEX zaaktype_node_uuid_idx ON zaaktype_node(uuid);

COMMIT;
