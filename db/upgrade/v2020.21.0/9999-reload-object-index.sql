
BEGIN;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'reload_object_index',
    'devops: fix object index MINTY-5504',
    950,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'object_uuid', uuid
    )
  FROM object_data
    WHERE
      length(text_vector) < 2
    AND
      object_class not in ('vraag', 'product')
    AND
      class_uuid IN (
        SELECT uuid from object_data where object_class = 'type'
      )
  ;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'reload_object_index',
    'devops: Update all indexes for old objects',
    900,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'object_uuid', uuid
    )
  FROM object_data
    WHERE
      length(text_vector) > 1
      AND
      object_class not in ('vraag', 'product')
    AND
      class_uuid IN (
        SELECT uuid from object_data where object_class = 'type'
      )
  ;

  INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'reload_object_index',
    'devops: Update all vraag/products objects',
    800,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'object_uuid', uuid
    )
  FROM object_data
    WHERE
      object_class in ('vraag', 'product')
  ;

COMMIT;
