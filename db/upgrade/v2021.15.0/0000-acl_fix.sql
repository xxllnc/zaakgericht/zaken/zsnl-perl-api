BEGIN;

ALTER TABLE zaak
    ADD aanvrager_type VARCHAR;

UPDATE
    zaak
SET
    aanvrager_type = b.betrokkene_type
FROM
    zaak_betrokkenen b
WHERE
    b.id = aanvrager;

CREATE OR REPLACE FUNCTION update_aanvrager_type ()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    AS $$
BEGIN
    IF NEW.aanvrager IS NOT NULL THEN
        SELECT
            betrokkene_type
        FROM
            zaak_betrokkenen
        WHERE
            id = NEW.aanvrager INTO NEW.aanvrager_type;
    ELSE
        NEW.aanvrager_type := NULL;
    END IF;
    RETURN NEW;
END;
$$;

DROP TRIGGER IF EXISTS zaak_aanvrager_update ON zaak;

CREATE TRIGGER zaak_aanvrager_update
    BEFORE INSERT OR UPDATE OF aanvrager_gm_id ON zaak
    FOR EACH ROW
    EXECUTE PROCEDURE update_aanvrager_type ();

DROP VIEW case_acl;

CREATE VIEW case_acl AS
SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    cam.key AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
FROM
    zaak z
    JOIN zaaktype_authorisation za ON za.zaaktype_id = z.zaaktype_id
        AND za.confidential = z.confidential
    JOIN (subject_position_matrix spm
        JOIN subject s ON spm.subject_id = s.id) ON spm.role_id = za.role_id
        AND spm.group_id = za.ou_id
    JOIN case_authorisation_map cam ON za.recht = cam.legacy_key
UNION ALL
SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
FROM
    zaak z
    JOIN zaak_authorisation za ON za.zaak_id = z.id
        AND za.entity_type = 'position'::text
    JOIN (subject_position_matrix spm
        JOIN subject s ON spm.subject_id = s.id) ON spm."position" = za.entity_id
UNION ALL
SELECT
    z.id AS case_id,
    z.uuid AS case_uuid,
    za.capability AS permission,
    s.id AS subject_id,
    s.uuid AS subject_uuid,
    z.zaaktype_id AS casetype_id
FROM
    zaak z
    JOIN zaak_authorisation za ON za.zaak_id = z.id
        AND za.entity_type = 'user'::text
    JOIN subject s ON s.username::text = za.entity_id;

-- Add covering indices; These contain everything needed later in the query so
-- PostgreSQL doesn't have do a second lookup on the heap.
DROP INDEX IF EXISTS zaak_confidential_covering_idx;

CREATE INDEX zaak_confidential_covering_idx ON zaak (confidential, zaaktype_id, id);

DROP INDEX IF EXISTS zaak_coordinator_covering_idx;

CREATE INDEX zaak_coordinator_covering_idx ON zaak (coordinator_gm_id, id, uuid);

DROP INDEX IF EXISTS zaak_behandelaar_covering_idx;

CREATE INDEX zaak_behandelaar_covering_idx ON zaak (behandelaar_gm_id, id, uuid);

DROP INDEX IF EXISTS zaak_aanvrager_covering_idx;

CREATE INDEX zaak_aanvrager_covering_idx ON zaak (aanvrager_type, aanvrager_gm_id, id, uuid);

DROP INDEX IF EXISTS subject_id_username_covering_idx;

CREATE INDEX subject_id_username_covering_idx ON subject (id, username);

COMMIT;

