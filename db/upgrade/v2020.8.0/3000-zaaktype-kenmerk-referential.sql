
BEGIN;

  ALTER TABLE zaaktype_kenmerken ADD COLUMN referential_new BOOLEAN NOT NULL DEFAULT false;
  UPDATE zaaktype_kenmerken SET referential_new = true where referential = '1';
  ALTER TABLE zaaktype_kenmerken DROP COLUMN referential;
  ALTER TABLE zaaktype_kenmerken RENAME COLUMN referential_new TO referential;

COMMIT;
