BEGIN;

ALTER TABLE transaction ADD COLUMN error_message TEXT;
ALTER TABLE transaction ADD COLUMN text_vector tsvector;

CREATE INDEX ON transaction USING gist(text_vector);

COMMIT;