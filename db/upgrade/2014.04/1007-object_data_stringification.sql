BEGIN;

ALTER TABLE object_data ADD COLUMN stringification TSVECTOR;

COMMIT;
