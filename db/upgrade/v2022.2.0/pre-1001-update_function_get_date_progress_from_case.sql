-- create new function get_date_progress_from_case

BEGIN;

  CREATE OR REPLACE FUNCTION get_date_progress_from_case(
	IN completion_date timestamp without time zone,
	IN target_completion_date timestamp without time zone,
	IN registration_date timestamp without time zone,
    IN case_status text,
    OUT percentage numeric
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    completion_date_local timestamp with time zone;
    completion_epoch bigint;
    start_epoch bigint;
    target_epoch bigint;

    current_difference bigint;
    max_difference bigint;
  BEGIN

    percentage := Null;
    IF case_status = 'stalled'
    THEN
      RETURN;
    END IF;

    IF completion_date IS NOT NULL
    THEN
      completion_date_local := (completion_date)::timestamp without time zone;
    ELSE
      SELECT INTO completion_date_local NOW()::timestamp without time zone;
    END IF;

    SELECT INTO completion_epoch date_part('epoch', completion_date_local);
    SELECT INTO start_epoch date_part('epoch', registration_date::timestamp without time zone);

    current_difference := completion_epoch - start_epoch;

    SELECT INTO target_epoch date_part('epoch', target_completion_date::timestamp without time zone);

    max_difference := target_epoch - start_epoch;
    max_difference := GREATEST(max_difference, 1);

    percentage := ROUND(100 * ( current_difference::numeric / max_difference::numeric ));

    RETURN;
  END;
  $$;

COMMIT;


--  udpate current get_date_progress_from_case function so it will function above so that logic is implemented once 
BEGIN;

  CREATE OR REPLACE FUNCTION get_date_progress_from_case(
    IN zaak hstore,
    OUT percentage text
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    completion_date timestamp without time zone;
    registration_date timestamp without time zone;
    target_completion_date timestamp without time zone;
	status text;
  BEGIN
	SELECT INTO completion_date (zaak->'afhandeldatum')::timestamp without time zone;
	SELECT INTO registration_date (zaak->'registratiedatum')::timestamp without time zone;
    SELECT INTO target_completion_date (zaak->'streefafhandeldatum')::timestamp without time zone;
	SELECT INTO status (zaak->'status')::text;
	
	percentage = get_date_progress_from_case(completion_date, target_completion_date, registration_date, status)::text;
    RETURN;
  END;
  $$;

COMMIT;

DROP INDEX IF EXISTS zaak_registratiedatum_idx;
CREATE INDEX CONCURRENTLY zaak_registratiedatum_idx ON zaak(registratiedatum) WHERE status != 'deleted' AND deleted is null;
