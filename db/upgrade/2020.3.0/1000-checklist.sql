BEGIN;

    ALTER TABLE checklist_item ADD uuid UUID NOT NULL DEFAULT uuid_generate_v4() UNIQUE;
    ALTER TABLE checklist_item ADD due_date DATE;
    ALTER TABLE checklist_item ADD description TEXT;

    ALTER TABLE checklist_item ADD assignee_id INTEGER;
    ALTER TABLE checklist_item
        ADD CONSTRAINT checklist_item_assignee_id_fkey
        FOREIGN KEY(assignee_id) REFERENCES subject(id) ON DELETE SET NULL;

    CREATE INDEX IF NOT EXISTS checklist_item_assignee_idx ON checklist_item(assignee_id);
    CREATE INDEX IF NOT EXISTS checklist_item_due_date_idx ON checklist_item(due_date);

COMMIT;
