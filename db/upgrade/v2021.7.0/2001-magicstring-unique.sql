BEGIN;

  /* This one will fail on certain environments, needs to be fixed on a
   * case by case basis, see zs-knife for more information on how to do
   * this
   */

  DROP INDEX IF EXISTS bibliotheek_kenmerken_magic_string_unique_idx;

  CREATE UNIQUE INDEX bibliotheek_kenmerken_magic_string_unique_id ON bibliotheek_kenmerken (magic_string) WHERE deleted IS NULL;

COMMIT;
