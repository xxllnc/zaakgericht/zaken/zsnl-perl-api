package TestFor::General::Search::Term::ArrayRole;
use base qw(Test::Class);

use TestSetup;

use Moose::Util qw(ensure_all_roles);
use Zaaksysteem::Search::Term::Literal;
use Zaaksysteem::Search::Term::Set;
use Zaaksysteem::Search::Term::ArrayRole;

sub test_array_on_literal : Tests {
    my $lterm = Zaaksysteem::Search::Term::Literal->new(value => 'x');
    my $mock_conditional = Test::MockObject->new();
    $mock_conditional->mock('lterm', sub { return $lterm; });

    my $literal = Zaaksysteem::Search::Term::Literal->new(
        value => 'foobar'
    );

    is_deeply(
        [$literal->evaluate(undef, $mock_conditional)],
        [
            "?",
            [{}, "foobar"]
        ],
        'Literal evaluates properly',
    );

    ensure_all_roles($literal, 'Zaaksysteem::Search::Term::ArrayRole');

    is_deeply(
        [$literal->evaluate(undef, $mock_conditional)],
        [
            "?",
            [{}, ["foobar"]]
        ],
        'Literal-as-array evaluates properly',
    );
}

sub test_array_on_set : Tests {
    my $lterm = Zaaksysteem::Search::Term::Set->new(value => 'x');
    my $mock_conditional = Test::MockObject->new();
    $mock_conditional->mock('lterm', sub { return $lterm; });

    my $literal1 = Zaaksysteem::Search::Term::Literal->new(value => 'foobar1');
    my $literal2 = Zaaksysteem::Search::Term::Literal->new(value => 'foobar2');
    my $set = Zaaksysteem::Search::Term::Set->new(
        values => [ $literal1, $literal2 ]
    );

    is_deeply(
        [$set->evaluate(undef, $mock_conditional)],
        [
            "( ?, ? )",
            [{}, "foobar1"],
            [{}, "foobar2"],
        ],
        'Bare "Set" evaluates properly',
    );

    ensure_all_roles($set, 'Zaaksysteem::Search::Term::ArrayRole');

    is_deeply(
        [$set->evaluate(undef, $mock_conditional)],
        [
            "?",
            [{}, ["foobar1", "foobar2"]]
        ],
        'Literal-as-array evaluates properly',
    );

}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
