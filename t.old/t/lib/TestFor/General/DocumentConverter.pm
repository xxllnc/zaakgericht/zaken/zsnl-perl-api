package TestFor::General::DocumentConverter;
use base qw(ZSTest);

use TestSetup;

use File::Temp;
use Zaaksysteem::DocumentConverter;

sub test_file_conversion : Tests {
    my $converter = Zaaksysteem::DocumentConverter->new();

    {
        my $doc = "t/data/doc/doc1.odt";
        my $tmpfile = File::Temp->new();
        $converter->convert_file(
            'destination_type' => 'pdf',
            'source_filename'  => $doc,
            'destination_filename' => $tmpfile->filename,
        );

        my $firstline = readline($tmpfile);
        like($firstline, qr/^%PDF-1\.4$/, 'PDF file generated with convert_file');
    }

    {
        my $csv = "t/data/doc/sheet1.csv";
        my $tmpfile = File::Temp->new();
        $converter->convert_file(
            'destination_type' => 'ods',
            'source_filename'  => $csv,
            'destination_filename' => $tmpfile->filename,
        );

        my $firstline = readline($tmpfile);
        like($firstline, qr/^%PDF-1\.4$/, 'PDF file generated with convert_file');
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
