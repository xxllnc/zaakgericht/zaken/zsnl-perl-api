package TestFor::General::Zaken::Roles::FaseObjecten;
use base qw(Test::Class);

use TestSetup;

sub zs_zaken_roles_faseobjecten_no_archival_yet : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();

            $case->afhandeldatum(undef);
            my $r = $case->set_vernietigingsdatum;
            is($r, undef, "No afhandeldatum set");

            # Testsuite only creates one resultaat
            my $resultaat
                = $case->zaaktype_node_id->zaaktype_resultaten->search_rs()
                ->first;

            $resultaat->trigger_archival(0);
            $resultaat->update;

            $case->resultaat($resultaat->resultaat);
            my $now = DateTime->now();
            $case->afhandeldatum($now);
            $r = $case->set_vernietigingsdatum;

            is($r, undef, "No 'vernietigingsdatum' returned if 'trigger_archival' flag is not set on result");
            is($case->vernietigingsdatum, undef, "No 'vernietigingsdatum' on case if 'trigger_archival' flag is not set on result");
            is($case->archival_state, undef, "No 'archival_state' on case if 'trigger_archival' flag is not set on result");
        },
        'set_vernietigingdatum',
    );
}

sub zs_zaken_roles_faseobjecten_set_vernietigingsdatum : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();

            $case->afhandeldatum(undef);
            my $r = $case->set_vernietigingsdatum;
            is($r, undef, "No afhandeldatum set");

            # Testsuite only creates one resultaat
            my $resultaat
                = $case->zaaktype_node_id->zaaktype_resultaten->search_rs({}, {order_by => 'id'})
                ->first;

            $case->resultaat($resultaat->resultaat);
            my $now = DateTime->now();
            $case->afhandeldatum($now);
            $r = $case->set_vernietigingsdatum;

            isa_ok($r, "DateTime", "Return value is of correct type");
            is(
                $r,
                $now->clone->add(days => $resultaat->bewaartermijn),
                "Destruction date is correct"
            );
            is($case->archival_state, 'vernietigen', "'archival_state' set correctly on case if 'trigger_archival' flag is set");

            $case->resultaat(undef);
            $r = $case->set_vernietigingsdatum;
            is(
                $r,
                $now->add(years => 1),
                "No result results in a destruction one year from now"
            );

            throws_ok(
                sub {
                    $case->resultaat("This will break stuff");
                    $case->set_vernietigingsdatum;
                },
                qr/Unable to find resultaat/,
                "DB discrepancy found"
            );
        },
        'set_vernietigingdatum',
    );
}

sub zs_zaken_roles_faseobjecten_is_in_phase : Tests {
    $zs->zs_transaction_ok(
        sub {
            my $case = $zs->create_case_ok();
            while ($case->set_volgende_fase) {
            }
            ok($case->is_in_phase('afhandel_fase'), "Zit in afhandelfase");
        },
        'is_in_phase'
    );
}

sub zs_zaken_roles_faseobjecten_date_of_registration_rule : Tests {
    my $self = shift;

    $zs->zs_transaction_ok(
        sub {
            my ($casetype, $node, $kenmerk) = $self->_generate_rule_casetype;
            $node->zaaktype_definitie_id->update(
                {
                    servicenorm => 10,
                    servicenorm_type => 'kalenderdagen',
                    afhandeltermijn => 10,
                    afhandeltermijn_type => 'kalenderdagen',
                }
            );

            my $case     = $zs->create_case_ok(zaaktype => $casetype);

            $case->zaak_kenmerken->update_fields({
                new_values => { $kenmerk->id => '1-1-2015' },
                zaak => $case,
            });

            $case->process_date_of_registration_rule(
                bibliotheek_kenmerken_id => $kenmerk->id,
                recalculate              => 'on',
            );

            is($case->registratiedatum->ymd, '2015-01-01', 'New date of registration is correct');
            is($case->streefafhandeldatum->ymd, '2015-01-11', 'New streefafhandeldatum based on werkdagen');

        },
        'rule: date of registration/target date'
    );

    $zs->zs_transaction_ok(
        sub {
            my ($casetype, $node, $kenmerk) = $self->_generate_rule_casetype;
            $node->zaaktype_definitie_id->update(
                {
                    servicenorm => 10,
                    servicenorm_type => 'werkdagen',
                    afhandeltermijn => 10,
                    afhandeltermijn_type => 'werkdagen',
                }
            );
            my $case     = $zs->create_case_ok(zaaktype => $casetype);

            $case->zaak_kenmerken->update_fields({
                new_values => { $kenmerk->id => '1-1-2015' },
                zaak => $case,
            });

            $case->process_date_of_registration_rule(
                bibliotheek_kenmerken_id => $kenmerk->id,
                recalculate              => 'on',
            );

            is($case->registratiedatum->ymd, '2015-01-01', 'New date of registration is correct');
            is($case->streefafhandeldatum->ymd, '2015-01-15', 'New streefafhandeldatum based on werkdagen');

        },
        'rule: date of registration/target date'
    );
}

sub _generate_rule_casetype {
    my $self            = shift;

    my $zaaktype_node   = $zs->create_zaaktype_node_ok;
    my $casetype        = $zs->create_zaaktype_ok(node => $zaaktype_node);
    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );
    $zs->create_zaaktype_status_ok(
        status => 3,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    ### Generate zaaktype_kenmerken, checkboxes
    my $zt_kenmerk = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $zs->create_bibliotheek_kenmerk_ok(
            naam            => 'date_foo',
            magic_string    => 'date_foo',
            value_type      => 'date',
        )
    );

    return ($casetype, $zaaktype_node, $zt_kenmerk->bibliotheek_kenmerken_id);
}

1;

__END__

=head1 NAME

TestFor::General::Zaken::Roles::FaseObjecten - A zaak fase objecten role tester

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
