package TestFor::General::Backend::Sysin::Modules::GWS4ALL;

use base 'ZSTest';

use TestSetup;

use Zaaksysteem::Backend::Sysin::Modules::GWS4ALL;

sub setup : Test(startup) {
    my $self = shift;

    $self->{module} = Zaaksysteem::Backend::Sysin::Modules::GWS4ALL->new();
    $self->{interface} = $zs->create_named_interface_ok({
        module => 'gws4all',
        name   => 'gws4all',
    });
}

sub test_compile_call : Tests {
    my $self = shift;
    my $module = $self->{module};
    my $interface = $self->{interface};

    my $compiled = $module->_compile_call(
        interface => $interface,
        action    => 'SendJaarOpgaveClient',
        wsdl      => 'JaarOpgaveClient-v0300-b01.wsdl',
        timeout   => '123',
        endpoint  => 'http://example.com',
    );

    isa_ok($compiled, 'CODE', "_compile_call returns a code reference");
}

sub test_compile_call_callers : Tests {
    my $self = shift;
    my $module = $self->{module};
    my $interface = Test::MockObject->new();
    $interface->mock('jpath', sub {
        my $self = shift;
        return 'jpath-'.shift;
    });

    {
        my @compile_call_args;

        no warnings 'redefine';
        local *Zaaksysteem::Backend::Sysin::Modules::GWS4ALL::_compile_call = sub {
            my $self = shift;

            push @compile_call_args, {@_};

            return "_compile_call";
        };

        my $res = $module->compile_jaaropgave_call($interface);
        is($res, "_compile_call", "compile_jaaropgave_call called the correct method");

        is_deeply(
            \@compile_call_args,
            [
                {
                    interface => $interface,
                    action    => 'SendJaarOpgaveClient',
                    wsdl      => 'JaarOpgaveClient-v0300-b01.wsdl',
                    timeout   => 'jpath-$.soap_timeout',
                    endpoint  => 'jpath-$.endpoint_jaaropgave',
                },
            ],
        );
    }

    {
        my @compile_call_args;

        no warnings 'redefine';
        local *Zaaksysteem::Backend::Sysin::Modules::GWS4ALL::_compile_call = sub {
            my $self = shift;

            push @compile_call_args, {@_};

            return "_compile_call";
        };

        my $res = $module->compile_specificatie_call($interface);
        is($res, "_compile_call", "compile_specificatie_call called the correct method");

        is_deeply(
            \@compile_call_args,
            [
                {
                    interface => $interface,
                    action    => 'SendUitkeringsSpecificatieClient',
                    wsdl      => 'UitkeringsSpecificatieClient-v0300-b01.wsdl',
                    timeout   => 'jpath-$.soap_timeout',
                    endpoint  => 'jpath-$.endpoint_specificatie',
                },
            ],
        );
    }
}

sub test_gws_header : Tests {
    my $self = shift;
    my $module = $self->{module};
    my $interface = Test::MockObject->new();
    $interface->mock('jpath', sub {
        my $self = shift;
        return 'jpath-'.shift;
    });

    with_stopped_clock {
        my %gws = $module->_gws_header($interface);

        $gws{wsa_MessageID} = 'foo';
        is_deeply(
            \%gws,
            {
                'headerPart' => {
                    'BerichtIdentificatie' => {
                        'ApplicatieInformatie'  => 'Zaaksysteem',
                        'DatTijdAanmaakRequest' => DateTime->now->iso8601 . 'Z',
                    },
                    'RouteInformatie' => {
                        'Bestemming' =>
                            { 'Gemeentecode' => 'jpath-$.gemeentecode' },
                        'Bron' => {
                            'ApplicatieNaam' => 'Zaaksysteem',
                            'Bedrijfsnaam'   => 'Mintlab'
                        }
                    }
                },
                'wsa_MessageID' => 'foo',
                'wsa_To'        => 'adres onbekend'
            },
            "GWS4all SOAP header constructed correctly",
        );
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
