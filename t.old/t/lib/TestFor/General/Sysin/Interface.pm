package TestFor::General::Sysin::Interface;

# ./zs_prove -v t/lib/TestFor/General/Sysin/Interface.pm
use base qw(ZSTest);

use TestSetup;
use IO::All;

sub create_phased_casetype {
    my $zaaktype = $zs->create_zaaktype_predefined_ok;

    my $kenmerk1 = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'kenmerk1',
        magic_string => 'magic_string_kenmerk1',
        value_type => 'text',
    );
    my $kenmerk2 = $zs->create_bibliotheek_kenmerk_ok(
        naam => 'kenmerk2',
        magic_string => 'magic_string_kenmerk2',
        value_type => 'numeric',
    );

    my $zaaktype_status = $zaaktype->zaaktype_node_id->zaaktype_statuses->search->first;
    my $zt_kenmerk1 = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $kenmerk1,
    );
    my $zt_kenmerk2 = $zs->create_zaaktype_kenmerk_ok(
        status              => $zaaktype_status,
        bibliotheek_kenmerk => $kenmerk2,
    );

    return ($zaaktype, $kenmerk1, $kenmerk2);
}

sub interface_create_case : Tests {
    $zs->zs_transaction_ok(sub {
        $schema->default_resultset_attributes->{delayed_touch} = Zaaksysteem::Zaken::DelayedTouch->new;

        my ($casetype, $kenmerk1, $kenmerk2) = create_phased_casetype;

        my $interface = $zs->create_interface_ok(
            name => 'Test Interface',
            module => 'bagcsv',
            zaaktype => $casetype,
            interface_config => {
                attribute_mapping => [
                    {
                        external_name => 'attribute1',
                        internal_name => {
                            searchable_object_id => $kenmerk1->magic_string
                        }
                    },
                    {
                        external_name => 'attribute2',
                        internal_name => {
                            searchable_object_id => $kenmerk2->magic_string
                        }
                    },
                ]
            }
        );

        my @result = $interface->_map_attributes({});
        is_deeply [@result], [], 'Returns empty list if no values supplied';

        my $value1 = 'my_value_1';
        my $value2 = 'my_value_2';
        my $values = { attribute1 => $value1 };
        my $expected = [{ bibliotheek_kenmerk => $kenmerk1, value => $value1 }];

        @result = $interface->_map_attributes($values);
        is scalar @result, 1,'Returns 1 mapped attribute as expected';
        my ($first) = @result;

        isa_ok $first->{bibliotheek_kenmerk}, 'Zaaksysteem::Model::DB::BibliotheekKenmerken',
            'Got bibliotheek kenmerk';

        is $first->{bibliotheek_kenmerk}->naam, 'kenmerk1',
            'Got right bibliotheek kenmerk naam';

        $values->{some_unmapped_attribute} = 'test';
        is scalar $interface->_map_attributes($values), 1,
            'Returns mapping still as expected, not including unmapped stuff';

        $values->{attribute2} = $value2;
        push @$expected, { bibliotheek_kenmerk => $kenmerk2, value => $value2 };
        is scalar $interface->_map_attributes($values), 2,
            'Mapped second attribute shows up';

        throws_ok(sub {
            $interface->create_case({
                values => {},
                requestor => 'ddfdffd'
            });
        }, qr/Validation of profile failed/, 'Fails when given an incorrect requestor');

        # prepare a transaction record mock
        my $output;
        my $mock_record = Test::MockObject->new;
        $mock_record->mock('output', sub {
            my ($self, $value) = @_;
            $output = $value;
        });

        my $np = $zs->create_natuurlijk_persoon_ok();
        my @kenmerken = $interface->_map_attributes($values);
        my $case = $interface->create_case({
            record => $mock_record,
            values => $values,
            requestor => 'betrokkene-natuurlijk_persoon-' . $np->id,
        });

        is scalar @kenmerken, 2, 'Two attributes stored';
        ok $case, 'Interface succesfully created a case';

        my $field_values = $case->field_values;
        foreach my $kenmerk (@kenmerken) {
            my $bib_kenmerk_id = $kenmerk->{bibliotheek_kenmerk}->id;
            my $value = $kenmerk->{value};

            TODO: {
                local $TODO = 'AUTUMN2015BREAK';
                is $field_values->{$bib_kenmerk_id}, $value,
                    "Value $value stored correctly in case";
            };
        }

        ok $output =~ m/Mogelijk dataverlies.*Numeriek/,
            "Warning produced for type mismatch";
    });
}

sub interface_active_state_callback : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        # Explicit active => 0, create_interface_ok overrides the class's
        # default state (inactive).
        my $interface = $zs->create_interface_ok(active => 0);

        my $ok;

        $interface->module_object->active_state_callback(sub {
            $ok = 1;
        });

        ok !$interface->active, 'interface is inactive by default';

        $interface->interface_update({ name => 'myname' }, $zs->object_model);

        ok !defined $ok, 'interface_update does not call active_state callback when no state is changed';

        $interface->interface_update({ active => 1 }, $zs->object_model);

        ok $ok, 'interface_update calls active_state callback when state changes';
    });
}

sub interface_update_callback : Tests {
    my $self = shift;

    $zs->txn_ok(sub {
        my $interface = $zs->create_interface_ok();

        my $ok;

        $interface->module_object->interface_update_callback(sub {
            $ok = 1;
        });

        $interface->interface_update({ name => 'myname' }, $zs->object_model);

        ok $ok, 'interface_update does call interface_update_callback callback when it updates';
    });
}

sub interface_get_type_warnings : Tests {
    $zs->zs_transaction_ok(sub {
        my $interface = $zs->create_interface_ok;

        my $kenmerk = $zs->create_bibliotheek_kenmerk_ok(
            value_type => 'numeric',
        );

        my @attributes = ({
            value => 'abc',
            bibliotheek_kenmerk => $kenmerk,
        }, {
            value => '123',
            bibliotheek_kenmerk => $kenmerk
        });

        my @warnings = $interface->_get_type_warnings(@attributes);

        is scalar @warnings, 1, "Got one warning";

        my ($warning) = @warnings;
        ok $warning =~ m/abc.*Numeriek/, 'Warning mentions "abc" and "numeriek"';
    });
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

