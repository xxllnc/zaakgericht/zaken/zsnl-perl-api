package TestFor::General::Tie::CallingHandle;
use base 'Test::Class';

use TestSetup;
use Zaaksysteem::Tie::CallingHandle;

sub test_callinghandle : Tests {
    my @result;

    my $handle = Zaaksysteem::Tie::CallingHandle->create(
        write_cb => sub { push @result, $_[0] },
    );

    syswrite($handle, "foo");
    print $handle "bar";

    is_deeply(
        \@result,
        [
            "foo",
            "bar",
        ],
        "Writing to the CallingHandle results in the callback being called."
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
