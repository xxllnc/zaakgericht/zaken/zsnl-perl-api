#! perl
use TestSetup;
initialize_test_globals_ok;

use Zaaksysteem::OutlookMailParser;
use File::Spec::Functions qw(catfile);


my $testFolder = catfile(qw(t inc OutlookTestMails));

sub parse {
    my ($filename, $no_of_files) = @_;
    my $outlook = Zaaksysteem::OutlookMailParser->new();

    my $msg = $outlook->process_outlook_message(
        {
            path          => catfile($testFolder, $filename),
            original_name => $filename
        }
    );
    is(@{$msg->attachments}, $no_of_files, "$no_of_files found in $filename");
    return $msg;
}

$zs->zs_transaction_ok(sub {

    my $outlook = Zaaksysteem::OutlookMailParser->new();
    my $settings = {
        body => 'dit is dan zeg maar de body',
        from => 'en dit de afzender',
        to => 'hierzo de ontvanger',
        subject => 'en hier gaat het over',
    };

    throws_ok( sub {
        $outlook->format({kaas => 1});
    }, qr/Validation of profile failed/, 'Fails when params are not correct');


    my $formatted = $outlook->format($settings);

    for my $key (qw/body from to subject/) {
        my $value = $settings->{$key};
        ok $formatted =~ m|$value|s, "$value is present in formatted string";
    }

    ok $formatted =~ m|Bijlagen\: geen|, "Contains header 'Bijlagen: geen'";

    # now add attachments
    $settings->{attachments} = [{
        'email_filename' => 'Test A.pdf',
        'file_id' => 84,
        'filename' => 'lukeskywalker.pdf'
    }, {
        'email_filename' => 'bla.pdf',
        'file_id' => 85,
        'filename' => 'darthvader.pdf'
    }];

    $formatted = $outlook->format($settings);

    ok $formatted =~ m|Bijlagen\:|, "Contains header 'Bijlagen'";
    ok $formatted =~ m|darthvader.pdf|, "Contains PDF attachment";
    ok $formatted =~ m|lukeskywalker.pdf|, "Contains PDF attachment";

}, 'Test format mail body');


$zs->zs_transaction_ok(sub {

    my $subject = $zs->get_subject_ok;
    my $case = $zs->create_case_ok();


    dies_ok sub {
        my ($body, $attachments) = parse('Non existing file');
    }, 'Should die on non-existing file';


    parse('Tekst only.msg', 1);
    parse('Tekst met bijlage - Word.msg', 2);
    parse('Tekst met bijlages - Plaatje excel word pdf.msg', 5);

    my $msg = parse('Topmail  Headertext.msg', 2);
    my $nested = $msg->nested_messages;
    is(@$nested, 1, "One nested message found");


}, 'Parse Outlook .MSG mail into different parts');

zs_done_testing();
