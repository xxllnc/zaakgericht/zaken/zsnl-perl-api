#! perl
use warnings;
use strict;

use lib 't/inc';
use TestSetup;
initialize_test_globals_ok;


$zs->zs_transaction_ok(sub {

    my $case = $zs->create_case_ok;
    my $second = $zs->create_case_ok;

    my $relation = $schema->resultset('CaseRelation')->add_relation($case->id, $second->id);

    my @relations = $schema->resultset('CaseRelation')->get_sorted($case->id);

    is scalar @relations, 1, "Original case has one relation";

    my ($first_relation) = @relations;
    is $first_relation->case_id, $second->id, "Original case relates to second case";

    my $third = $zs->create_case_ok;

    $case->duplicate_relations({ target => $third });

    my @third_relations = $schema->resultset('CaseRelation')->get_sorted($third->id);

    is scalar @third_relations, 1, "Duplicate also has one relation";
    ($first_relation) = @third_relations;
    is $first_relation->case_id, $second->id, "Duplicate also relates to second case";

}, 'duplicate_relations',);



zs_done_testing();
