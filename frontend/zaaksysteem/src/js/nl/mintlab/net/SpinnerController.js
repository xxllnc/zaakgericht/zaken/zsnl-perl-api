// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.net')
    .controller('nl.mintlab.net.SpinnerController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        $scope.uiBlocked = false;

        smartHttp.subscribe('connect', function () {
          setLoadState();
        });

        smartHttp.subscribe('close', function () {
          setLoadState();
        });

        function setLoadState() {
          $scope.uiBlocked =
            $scope.loading || smartHttp.getBlockingRequests().length > 0;
          if ($scope.$$phase !== '$digest' && $scope.$$phase !== '$apply') {
            $scope.$apply();
          }
        }

        $scope.$watch('loading', function () {
          setLoadState();
        });

        setLoadState();
      },
    ]);
})();
