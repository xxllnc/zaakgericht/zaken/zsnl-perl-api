// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.user')
    .controller('nl.mintlab.user.SearchSubjectController', [
      '$scope',
      function ($scope) {
        $scope.init = function (
          jsversion,
          jsfill,
          method,
          search_filter_post,
          jscontext,
          jstype,
          method,
          url,
          import_datum,
          containerid
        ) {
          $.jsversion = jsversion;
          $.containerid = containerid;
          $.method = method;
          $.search_filter_post = search_filter_post;

          $(document).ready(function () {
            tr_tpl = $('#search_betrokkene_tpl');
            $('#accordion').accordion({
              autoHeight: false,
            });

            $('#search_betrokkene form').submit(function () {
              $('#betrokkene_loader').removeClass('disabled');
              postdata = {
                search: 1,
                jsfill: jsfill,
                jscontext: jscontext,
                jstype: jstype,
                jsversion: jsversion,
                method: method,
                url: url,
                import_datum: import_datum,
              };

              /* NEW STYLE */
              if (postdata.jsversion == 3) {
                // var serialized      = $(this).serializeArray();
                serialized = {};

                var rawserialized = $(this).serializeArray();
                $.each(rawserialized, function () {
                  serialized[this.name] = this.value;
                });

                serialized.search = 1;
                serialized.jsversion = 3;

                _betrokkene_load_result_rows(serialized);
              } else {
                $('#search_betrokkene input').each(function () {
                  if (
                    $(this).attr('type') == 'checkbox' ||
                    $(this).attr('type') == 'radio'
                  ) {
                    if ($(this).attr('checked') == true) {
                      postdata[$(this).attr('name')] = $(this).attr('value');
                    }
                    return;
                  }
                  postdata[$(this).attr('name')] = $(this).attr('value');
                });
                $('#search_betrokkene select').each(function () {
                  selectname = $(this).attr('name');
                  $('#search_betrokkene select option:selected').each(
                    function () {
                      postdata[selectname] = $(this).attr('value');
                    }
                  );
                });

                $('#search_betrokkene_results tbody').load(
                  '/betrokkene/search',
                  postdata,
                  function () {
                    $('#accordion').accordion('option', 'active', 0);

                    $('#betrokkene_loader').addClass('disabled');

                    _betrokkene_load_external_buttons(postdata);
                  }
                );
              }

              return false;
            });

            function _betrokkene_load_result_rows(serialized_form) {
              if (
                serialized_form.external_search.toLowerCase().match(/gba-v/) &&
                !serialized_form['np-burgerservicenummer']
              ) {
                $('#betrokkene_loader, #betrokkene_loader_gba').addClass(
                  'disabled'
                );
                return false;
              }

              $(
                '#betrokkene_results_more_available, #betrokkene_results_no_results'
              ).addClass('disabled');

              $('#betrokkene_loader, #betrokkene_loader_gba').removeClass(
                'disabled'
              );

              $(
                '#external_search_button_gba, #external_search_button_gbav'
              ).attr('disabled', 'disabled');

              $('#search_betrokkene_results tbody').load(
                '/betrokkene/search',
                serialized_form,
                function () {
                  initializeEverything($(this));

                  $('#accordion').accordion('option', 'active', 1);

                  $('#betrokkene_loader, #betrokkene_loader_gba').addClass(
                    'disabled'
                  );
                  $(
                    '#external_search_button_gba, #external_search_button_gbav'
                  ).attr('disabled', null);

                  /* Get error */
                  var external_error = '';
                  if ($('#external_errorid').length) {
                    external_error = $(
                      '#external_errorid input[name="external_errorid"]'
                    ).val();

                    console.log(external_error);
                  }

                  $('.betrokkene_error_messages').addClass('disabled');

                  if (external_error && external_error == 'fail') {
                    $('#betrokkene_results_error').removeClass('disabled');
                  } else if (external_error && external_error == 'multiple') {
                    $('#betrokkene_results_more_available').removeClass(
                      'disabled'
                    );
                  } else {
                    if (
                      $('#search_betrokkene_results tr.betrokkene_keuze')
                        .length == 0
                    ) {
                      $('#betrokkene_results_no_results').removeClass(
                        'disabled'
                      );
                    }
                  }

                  _betrokkene_load_external_buttons(serialized_form);
                }
              );
            }

            function _betrokkene_load_external_buttons(serialized_form) {
              $('.ezra_search_betrokkene_external_button').show();

              // if (serialized_form.external_search.toLowerCase().match(/gba-v/)) {
              //     $('#external_search_button_gba').hide();
              // } else if (serialized_form.external_search.toLowerCase().match(/gba/)) {
              //     $('#external_search_button_gbav').hide();
              // }

              $('.ezra_search_betrokkene_external_button')
                .unbind('click')
                .click(function () {
                  serialized_form.external_search = $(this).attr('value');
                  //$('#accordion').accordion('activate', 0);

                  $('#search_betrokkene input[type="submit"]').val(
                    'Zoek extern'
                  );

                  $('#search_betrokkene input[name="external_search"]').val(
                    serialized_form.external_search
                  );

                  if (
                    !serialized_form['np-burgerservicenummer'] &&
                    !(
                      serialized_form['np-geboortedatum'] &&
                      serialized_form['np-postcode'] &&
                      serialized_form['np-huisnummer']
                    )
                  ) {
                    $('#accordion').accordion('option', 'activate', 0);

                    $(
                      '#search_betrokkene input[name="np-burgerservicenummer"]'
                    ).css('border-color', 'red');
                    $('#search_betrokkene input[name="np-geboortedatum"]').css(
                      'border-color',
                      'red'
                    );
                    $('#search_betrokkene input[name="np-postcode"]').css(
                      'border-color',
                      'red'
                    );
                    $('#search_betrokkene input[name="np-huisnummer"]').css(
                      'border-color',
                      'red'
                    );

                    return false;
                  }

                  $(
                    '#search_betrokkene input[name="np-burgerservicenummer"]'
                  ).css('border-color', '#bbb');
                  $('#search_betrokkene input[name="np-geboortedatum"]').css(
                    'border-color',
                    '#bbb'
                  );
                  $('#search_betrokkene input[name="np-postcode"]').css(
                    'border-color',
                    '#bbb'
                  );
                  $('#search_betrokkene input[name="np-huisnummer"]').css(
                    'border-color',
                    '#bbb'
                  );

                  $('#betrokkene_loader').removeClass('disabled');
                  _betrokkene_load_result_rows(serialized_form);
                });
            }

            function _old_subject_load(elem, options) {
              var formcontainer = options['container'];
              var description = '';

              $(elem)
                .find('td')
                .each(function () {
                  if (
                    $(this).hasClass('result_voorletters') ||
                    $(this).hasClass('result_geslachtsnaam')
                  ) {
                    if (description) {
                      description = description + ' ';
                    }
                    description = description + $(this).html();
                  }
                });

              var selector_identifier = formcontainer
                .find('input[name="ezra_client_info_selector_identifier"]')
                .val();

              var selector_naam = formcontainer
                .find('input[name="ezra_client_info_selector_naam"]')
                .val();

              var subject_identifier = $(elem).attr('id');
              if (options['subject_identifier']) {
                subject_identifier = options['subject_identifier'];
              }

              if ($(selector_identifier).attr('name')) {
                $(selector_identifier).val(subject_identifier);
              } else {
                $(selector_identifier).html(subject_identifier);
              }

              /* Load identifier and name */
              if ($(selector_naam).attr('name')) {
                $(selector_naam).val(description);
              } else {
                $(selector_naam).html(description);
              }
            }

            function _old_subject_results(elem) {
              description = '';

              $(elem)
                .find('td')
                .each(function () {
                  if (
                    $(this).hasClass('result_voorletters') ||
                    $(this).hasClass('result_geslachtsnaam')
                  ) {
                    if (description) {
                      description = description + ' ';
                    }
                    description = description + $(this).html();
                  }
                });

              if ($.method && $.method == 'reload') {
                window.location =
                  url + '?ztc_behandelaar_id=' + $(elem).attr('id');
                $.ztWaitStart();
              } else if ($.method && $.method == 'post') {
                $.ztWaitStart();
                $.post(
                  url,
                  { ztc_behandelaar_id: $(elem).attr('id') },
                  function () {
                    window.location.reload();
                  }
                );
              } else if ($.jsversion == 3) {
                if ($.search_filter_post) {
                  var data =
                    'action=update_filter&filter_type=' +
                    $.search_filter_post +
                    '&nowrapper=1&value=' +
                    $(elem).attr('id');
                  updateSearchFilters(data);
                }
                var formcontainer = $('#search_betrokkene').find('form');
                //console.log("selector nama: " + selector_naam);

                load_options = {
                  container: formcontainer,
                };

                _old_subject_load(elem, load_options);

                initializeEverything();
              } else if ($.jsversion >= 2) {
                $(
                  '#start_' +
                    $.containerid +
                    " .ezra_select_betrokkene input[type='hidden']"
                ).attr('value', $(elem).attr('id'));

                $(
                  '#start_' +
                    $.containerid +
                    " .ezra_select_betrokkene input[type='text']"
                ).attr('value', description);
                $(
                  '#start_' +
                    $.containerid +
                    ' .ezra_select_betrokkene .validate-ok'
                ).css('visibility', 'visible');
              } else {
                $('#' + jscontext + " input[name='" + jsfill + "_id']").attr(
                  'value',
                  $(elem).attr('id')
                );

                $('#' + jscontext + ' input[name=' + jsfill + ']').attr(
                  'value',
                  description
                );
              }

              if ($.method && $.method == 'post') {
                /* Do nothing here, deprecated, we reload the window in
                 * above post
                 */
              } else {
                $('#searchdialog').dialog('close');
              }
            }

            $('#search_betrokkene_results').on(
              'click',
              '.betrokkene_keuze',
              function () {
                $('#search_betrokkene_results').data('select_betrokkene', {
                  requested_subject: $(this),
                });

                if ($(this).hasClass('external_search')) {
                  ezra_dialog(
                    {
                      url: '/page/confirmation',
                      title: 'Bevestiging',
                      layout: 'small',
                      cgi_params: {
                        message:
                          'U staat op het punt om deze persoon te importeren in zaaksysteem, wilt u doorgaan?',
                      },
                    },
                    function (obj) {
                      obj.find('form').submit(function () {
                        var formcontainer = $('#search_betrokkene').find(
                          'form'
                        );
                        var elem = $('#search_betrokkene_results').data(
                          'select_betrokkene'
                        ).requested_subject;

                        var confirmation_form = this;

                        //console.log("selector nama: " + selector_naam);

                        var element_id_matches = elem
                          .attr('id')
                          .match(/external_transaction_id-(.*)$/);

                        $(this).find('input[type="submit"]').attr({
                          value: 'Een ogenblik...',
                          disabled: 'disabled',
                        });

                        /* Import this person */
                        $.ajax({
                          dataType: 'json',
                          url: '/betrokkene/external_import',
                          data: {
                            external_transaction_id: element_id_matches[1],
                          },
                          statusCode: {
                            200: function (data, status) {
                              var items = data.result;

                              var subject = items[0];

                              load_options = {
                                container: formcontainer,
                                subject_identifier:
                                  subject.betrokkene_identifier,
                              };

                              _old_subject_load(elem, load_options);

                              $('#searchdialog').dialog('close');
                              $('#dialog').dialog('close');
                            },
                            500: function (data) {
                              $('#searchdialog .error_message')
                                .show()
                                .find('.error_message_text')
                                .html(
                                  'Helaas: Persoon kan niet geimporteerd worden'
                                );

                              $.ztWaitStop();

                              $('#dialog').dialog('close');
                            },
                          },
                        });

                        return false;
                      });
                    }
                  );
                } else {
                  _old_subject_results(this);
                }
              }
            );
          });
        };
      },
    ]);
})();
