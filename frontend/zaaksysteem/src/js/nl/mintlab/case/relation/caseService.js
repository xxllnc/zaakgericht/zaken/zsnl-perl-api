// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').factory('caseService', [
    '$q',
    '$http',
    function ($q, $http) {
      var caseService = {};

      caseService.resolveCase = function (caseId, result, reason) {
        var deferred = $q.defer();

        $http({
          method: 'POST',
          url: '/zaak/' + caseId + '/update/afhandelen',
          data: {
            reden: reason,
            system_kenmerk_resultaat: result,
            update: 1,
          },
        })
          .success(function (/*response*/) {
            deferred.resolve();
          })
          .error(function (/*response*/) {
            deferred.reject();
          });

        return deferred.promise;
      };

      return caseService;
    },
  ]);
})();
