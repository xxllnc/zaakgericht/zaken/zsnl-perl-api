// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformRichTextField', [
    function () {
      return {
        require: ['^zsCaseWebformField'],
        link: function (scope, element, attrs, controllers) {
          var formField = controllers[0],
            wymIndex = Number(element.attr('wymeditor_index')),
            wymeditor = $.wymeditors(wymIndex),
            initFunc,
            isFocused = false;

          function handleFocus() {
            isFocused = true;
          }

          function handleBlur() {
            isFocused = false;
          }

          // wymeditor doesn't exist in PIP, but we don't need this there
          if (wymeditor) {
            initFunc = wymeditor.initIframe;
            wymeditor.initIframe = function () {
              var doc;
              initFunc.apply(wymeditor, arguments);
              doc = this._doc;

              doc.addEventListener('focus', handleFocus, true);
              doc.addEventListener('blur', handleBlur, true);

              scope.$on('$destroy', function () {
                doc.removeEventListener('focus', handleFocus, true);
                doc.removeEventListener('blur', handleBlur, true);
              });
            };
          }

          formField.setSetter(function (value) {
            if (!wymeditor) {
              element[0].value = value;
            } else if (!wymeditor.ready()) {
              // WYM editor stores the value on init,
              // but only allows you to edit the value
              // after the iFrame is loaded, meaning
              // we're stuck in no man's land. So we
              // modify a private variable.
              wymeditor._html = value;
            } else if (isFocused === false) {
              wymeditor.html(value);
            }
          });
        },
      };
    },
  ]);
})();
