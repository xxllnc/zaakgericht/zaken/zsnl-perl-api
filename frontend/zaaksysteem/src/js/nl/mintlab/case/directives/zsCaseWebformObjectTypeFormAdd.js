// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.case')
    .directive('zsCaseWebformObjectTypeFormAdd', [
      function () {
        return {
          require: [
            'zsCaseWebformObjectTypeFormAdd',
            'zsCaseWebformObjectTypeForm',
            '^zsCaseWebformObjectTypeMutationAdd',
          ],
          controller: [
            function () {
              var ctrl = this,
                zsCaseWebformObjectTypeForm,
                zsCaseWebformObjectTypeMutationAdd;

              ctrl.setControls = function () {
                zsCaseWebformObjectTypeForm = arguments[0];
                zsCaseWebformObjectTypeMutationAdd = arguments[1];
                zsCaseWebformObjectTypeMutationAdd.setForm(
                  zsCaseWebformObjectTypeForm
                );
              };

              return ctrl;
            },
          ],
          controllerAs: 'caseWebformObjectTypeFormAdd',
          link: function (scope, element, attrs, controllers) {
            controllers[0].setControls.apply(
              controllers[0],
              controllers.slice(1)
            );
          },
        };
      },
    ]);
})();
