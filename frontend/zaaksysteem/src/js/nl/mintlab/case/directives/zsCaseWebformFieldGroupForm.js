// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.case').directive('zsCaseWebformFieldGroupForm', [
    function () {
      var cancelEvent = window.zsFetch('nl.mintlab.utils.events.cancelEvent');

      return {
        require: ['zsCaseWebformFieldGroupForm', 'zsCaseWebformFieldGroup'],
        controller: [
          '$scope',
          '$element',
          function ($scope, $element) {
            var ctrl = this,
              zsCaseWebformFieldGroup,
              anchor,
              el;

            ctrl.link = function (controllers) {
              zsCaseWebformFieldGroup = controllers[0];
            };

            function onClick(event) {
              if (!zsCaseWebformFieldGroup.isVisible()) {
                cancelEvent(event, true);
              }
            }

            el = $element[0].querySelectorAll('a');

            anchor = angular.element(el);

            anchor.bind('click', onClick);

            $scope.$on('$destroy', function () {
              anchor.unbind('click', onClick);
            });

            return ctrl;
          },
        ],
        link: function (scope, element, attrs, controllers) {
          controllers[0].link(controllers.slice(1));
        },
      };
    },
  ]);
})();
