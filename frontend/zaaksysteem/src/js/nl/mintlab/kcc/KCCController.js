// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.kcc').controller('nl.mintlab.kcc.KCCController', [
    '$scope',
    'callService',
    'subjectService',
    function ($scope, callService, subjectService) {
      var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
        tabData = [];

      $scope.activeTabData = null;

      function updateTabData() {
        var hasActiveTabData, data, subject;

        tabData.length = 0;

        _.each(callService.getCalls(), function (call) {
          data = {
            type: 'call',
            id: call.id,
            data: {
              phonenumber: call.processor_params.phonenumber,
              contact: call.betrokkene[0],
            },
          };

          tabData.push(data);
          if ($scope.isActiveTab(data)) {
            hasActiveTabData = true;
          }
        });

        if ((subject = subjectService.getSubject())) {
          tabData.unshift({
            type: 'contact',
            id: subject.identifier,
            data: {
              phonenumber: null,
              contact: subject,
            },
          });
        }

        if (!hasActiveTabData) {
          $scope.activeTabData = null;
        }

        if (!$scope.activeTabData) {
          $scope.setActiveTab(tabData[0]);
        }
      }

      $scope.isActiveTab = function (tabData) {
        return (
          $scope.activeTabData &&
          tabData.id === $scope.activeTabData.id &&
          tabData.type === $scope.activeTabData.type
        );
      };

      $scope.getTabData = function () {
        return tabData;
      };

      $scope.setActiveTab = function (tabData) {
        $scope.activeTabData = tabData;
      };

      $scope.getDisplayName = function (obj) {
        var displayName = '',
          contact;

        if (!obj) {
          return displayName;
        }

        contact = obj.data.contact;

        if (!contact) {
          displayName = obj.data.phonenumber;
        } else {
          displayName = contact.name;
        }

        return displayName;
      };

      $scope.getByline = function (tabData) {
        var byline = '',
          contact,
          phonenumber;

        if (!tabData) {
          return byline;
        }

        contact = tabData.data.contact;
        phonenumber = tabData.data.phonenumber;

        if (contact) {
          byline =
            _.filter(
              [
                contact.street,
                contact.postal_code,
                contact.city,
                contact.telephone_numbers && contact.telephone_numbers.length
                  ? contact.telephone_numbers[0].number
                  : null,
                contact.email_addresses ? contact.email_addresses[0] : null,
              ],
              function (part) {
                return !!part;
              }
            ).join(', ') || '';
        }

        return byline;
      };

      $scope.getType = function (tabData) {
        return tabData ? tabData.type : null;
      };

      $scope.hasSubject = function () {
        return !!subjectService.getSubject();
      };

      $scope.$on('call.list.update', function (/*$event, list*/) {
        safeApply($scope, function () {
          updateTabData();
        });
      });

      $scope.$on('subject.change', function (/*$event*/) {
        safeApply($scope, function () {
          updateTabData();
        });
      });

      updateTabData();
    },
  ]);
})();
