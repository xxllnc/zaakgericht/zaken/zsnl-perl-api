// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  angular
    .module('Zaaksysteem.docs')
    .controller('nl.mintlab.docs.VersionController', [
      '$scope',
      'smartHttp',
      function ($scope, smartHttp) {
        $scope.revisions = [];

        $scope.revertTo = function (revision) {
          $scope.entity.updating = true;

          smartHttp
            .connect({
              url: 'file/revert_to',
              method: 'POST',
              data: {
                file_id: revision.file_id,
              },
            })
            .success(function (data) {
              var fileData = data.result ? data.result[0] : null;

              $scope.entity.updateWith(fileData);

              reloadData();
            })
            ['finally'](function () {
              $scope.entity.updating = false;
            });
        };

        $scope.showRevision = function (rev) {
          $scope.revision = rev;
        };

        function reloadData() {
          smartHttp
            .connect({
              url: 'file/version_info/file_id/' + $scope.entity.id,
            })
            .success(function (data) {
              var revs = data.result || [];
              $scope.revisions = revs;
              $scope.showRevision(revs[0]);
            });
        }

        reloadData();
      },
    ]);
})();
