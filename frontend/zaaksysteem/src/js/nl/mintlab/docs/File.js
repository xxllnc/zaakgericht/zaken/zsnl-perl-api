// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  window.zsDefine('nl.mintlab.docs.File', function () {
    var inherit = window.zsFetch('nl.mintlab.utils.object.inherit'),
      StoredEntity = window.zsFetch('nl.mintlab.docs.StoredEntity');

    function File() {
      this._entityType = 'file';
      File.uber.constructor.apply(this, arguments);
    }

    inherit(File, StoredEntity);

    File.prototype.setAsRevision = function (asRevision) {
      this._asRevision = asRevision;
    };

    File.prototype.getAsRevision = function () {
      return this._asRevision;
    };

    return File;
  });
})();
