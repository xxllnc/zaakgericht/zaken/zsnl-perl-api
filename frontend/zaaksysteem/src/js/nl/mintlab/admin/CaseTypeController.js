// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';
  angular
    .module('Zaaksysteem.admin')
    .controller('nl.mintlab.admin.CaseTypeController', [
      '$scope',
      '$window',
      'smartHttp',
      'translationService',
      function ($scope, $window, smartHttp, translationService) {
        var item_url;

        $scope.reloadData = function () {
          smartHttp
            .connect({
              method: 'GET',
              url: item_url + '/GET',
            })
            .success(function onSuccess(data) {
              $scope.pdc_tarief = data.definitie.pdc_tarief;

              var contactChannels = [
                'telefoon',
                'balie',
                'behandelaar',
                'email',
                'post',
              ];

              contactChannels.map(function (contactChannel) {
                var key = 'pdc_tarief_' + contactChannel;
                $scope[key] = data.node.properties[key];
              });
            })
            .error(function onError(/*data*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Tariefinstellingen konden niet worden geladen'
                ),
              });
            });
        };

        $scope.init = function (itemId) {
          $scope.itemId = itemId;
          item_url = 'beheer/zaaktypen/' + $scope.itemId;
          $scope.reloadData();
        };
      },
    ]);
})();
