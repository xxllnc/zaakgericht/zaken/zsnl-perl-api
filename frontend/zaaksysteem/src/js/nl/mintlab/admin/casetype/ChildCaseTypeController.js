// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
angular
  .module('Zaaksysteem.admin.casetype')
  .controller('nl.mintlab.admin.casetype.ChildCaseTypeController', [
    '$scope',
    'smartHttp',
    'translationService',
    function ChildCaseTypeController($scope, smartHttp, translationService) {
      function save() {
        smartHttp
          .connect({
            method: 'POST',
            url: '/api/casetype/' + $scope.caseTypeId + '/save_child_casetypes',
            data: {
              child_casetypes: angular.toJson($scope.children),
            },
          })
          .error(function (/*response*/) {
            $scope.$emit('systemMessage', {
              type: 'error',
            });
          });
      }

      $scope.addCaseType = function addCaseType(casetype) {
        var children = $scope.children || [];
        var isAdded = !!_.find(children, function (child) {
          return child.casetype.id.toString() === casetype.id.toString();
        });

        if (isAdded) {
          $scope.$emit('systemMessage', {
            type: 'error',
            content: translationService.get(
              'Kindzaaktype "%s" al aanwezig.',
              casetype.zaaktype_node_id.titel
            ),
          });

          return;
        }

        $scope.$emit('systemMessage', {
          type: 'info',
          content: translationService.get(
            'Kindzaaktype "%s" toegevoegd.',
            casetype.zaaktype_node_id.titel
          ),
        });

        children.push({
          casetype: {
            id: casetype.id,
            title: casetype.zaaktype_node_id.titel,
          },
          import: true,
          definitie: true,
          authorisaties: true,
          betrokkenen: true,
          actions: true,
          first_phase: false,
          middle_phases: false,
          last_phase: false,
          _collapsed: false,
        });
      };

      $scope.removeRelation = function removeRelation(relation, $event) {
        $scope.children = _.reject($scope.children, function (r) {
          return relation.casetype.id === r.casetype.id;
        });
        $event.stopPropagation();
      };

      $scope.onImportToggleClick = function onImportToggleClick($event) {
        $event.stopPropagation();
      };

      $scope.$watch(
        'children',
        function watchChildren(newValue, oldValue) {
          if (newValue !== oldValue) {
            save();
          }
        },
        true
      );

      $scope.$watch('newCaseType', function watchNewCaseType(newValue) {
        if (newValue) {
          $scope.addCaseType(newValue);
          $scope.newCaseType = null;
        }
      });
    },
  ]);
