// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.admin.casetype')
    .controller('nl.mintlab.admin.casetype.PublicationStatusController', [
      '$scope',
      '$timeout',
      '$window',
      'smartHttp',
      function ($scope, $timeout, $window, smartHttp) {
        var delay = 5000,
          timeoutCancel,
          status = {},
          isAllFinished = false;

        function wait() {
          timeoutCancel = $timeout(function () {
            poll();
          }, delay);
        }

        function poll() {
          smartHttp
            .connect({
              method: 'GET',
              url: '/api/casetype/' + $scope.casetypeId + '/publish_status',
            })
            .success(function (response) {
              status = response.result[0];

              if (!$scope.isAllFinished()) {
                wait();
              } else {
                timeoutCancel = null;
              }
            })
            .error(function () {
              wait();
            });
        }

        function publish() {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/casetype/' + $scope.casetypeId + '/publish',
              data: {
                commit_message: $scope.commitMessage,
                commit_components: $scope.commitComponents,
              },
            })
            .success(function (/*response*/) {
              isAllFinished = true;
              if (timeoutCancel) {
                $timeout.cancel(timeoutCancel);
                timeoutCancel = null;
              }
            })
            .error(function (/*response*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
              });
            });
        }

        $scope.progress = function () {
          const returnUrl = new URL(window.top.location).searchParams.get(
            'return_url'
          );

          if (returnUrl) {
            window.top.location.href = window.top.location.origin + returnUrl;
          } else {
            $window.location = '/beheer/zaaktypen/flush/' + $scope.casetypeId;
          }
        };

        $scope.isCasetypeFinished = function (casetype) {
          var isFinished =
            $scope.isAllFinished() || !!status[casetype.casetype.id.toString()];
          return isFinished;
        };

        $scope.isAllFinished = function () {
          return isAllFinished;
        };

        $scope.$watch('casetypeId', function (nwVal /*, oldVal, scope*/) {
          if (nwVal) {
            publish();
            poll();
          }
        });

        $scope.$on('destroy', function () {
          if (timeoutCancel) {
            $timeout.cancel(timeoutCancel);
          }
        });
      },
    ]);
})();
