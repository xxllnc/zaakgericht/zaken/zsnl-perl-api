// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import controller from './TruncateHtmlController';
import template from './template.html';

export default angular
  .module('zsTruncateHtml', [])
  .component('zsTruncateHtml', {
    bindings: {
      length: '&',
      value: '&',
    },
    controller,
    template,
  }).name;
