// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { expect } from 'chai';
import getRequestOptionsFactory from './getRequestOptionsFactory';

const getRequestOptions = getRequestOptionsFactory(() => ({
  endpoint: 'foo',
  token: {
    access_token: 'quux',
  },
}));

describe('The getRequestOptionsFactory function', () => {
  it('works', () => {
    expect(getRequestOptions('/bar')).to.deep.equal({
      url: 'foo/bar',
      headers: {
        Authorization: 'Bearer quux',
        'X-Client-Type': undefined,
      },
      timeout: 5000,
      withCredentials: false,
    });
  });
});
