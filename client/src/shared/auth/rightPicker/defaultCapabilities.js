// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default () => {
  return [
    {
      name: 'manage',
      label: 'Beheren',
    },
    {
      name: 'write',
      label: 'Behandelen',
    },
    {
      name: 'read',
      label: 'Raadplegen',
    },
    {
      name: 'search',
      label: 'Zoeken',
    },
  ].reverse();
};
