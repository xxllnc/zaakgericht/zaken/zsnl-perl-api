// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import getCaseCreateSnack from '../../../../intern/getCaseCreateSnack';
import getRequestInfoV2 from './getRequestInfoV2';
import getRequestInfoV0 from './getRequestInfoV0';
import getApiValues from './getApiValues';
import getEligibleForV2 from './helpersV2/getEligibleForV2';

const registerCase = (
  $http,
  $state,
  ctrl,
  intakeShowContactInfo,
  allocationData,
  allFieldsReducer,
  snackbarService,
  resetTouched,
  setSubmitting,
  parentCaseId,
  relationType,
  zsConfirm,
  session
) => {
  const vals = ctrl.getValues();
  const fields = allFieldsReducer.data();

  const eligibleForV2 = getEligibleForV2(
    ctrl.casetypeV2,
    ctrl.recipient,
    fields,
    vals
  );

  setSubmitting(true);

  // when type='me' and data.me is 'true' or 'undefined' it should redirect to the case (see getAssignment.js)
  const shouldRedirectToCase =
    get(vals, '$allocation.type') === 'me' &&
    get(vals, '$allocation.data.me') !== false;

  const apiValues = getApiValues(
    pickBy(vals, (value, key) => key.slice(0, 1) !== '$'),
    fields
  );

  const requestInfo = eligibleForV2
    ? getRequestInfoV2(
        ctrl,
        vals,
        intakeShowContactInfo,
        allocationData,
        apiValues
      )
    : getRequestInfoV0(
        ctrl,
        vals,
        intakeShowContactInfo,
        apiValues,
        shouldRedirectToCase,
        parentCaseId,
        relationType
      );

  // v2 only returns the caseUuid, but we need the caseId
  // therefore we do a followup request to get it
  const registerRequest = $http(requestInfo).then((response) => {
    if (!eligibleForV2) {
      return response;
    }

    return $http({
      url: '/api/v2/cm/case/get_case_basic',
      method: 'GET',
      params: {
        case_uuid: get(response, 'data.data.id'),
      },
    });
  });

  const register = (responseHandler, catchHandler) => {
    snackbarService.wait('Uw zaak wordt geregistreerd.', {
      // v0: '/api/v0/case/create'
      // v1: '/api/v1/case/create_delayed'
      // v2: '/api/v2/cm/case/create_case'
      promise: registerRequest,
      then: responseHandler,
      catch: catchHandler,
    });
  };

  const checkAndRegister = (responseHandler, catchHandler) => {
    const departmentId = get(vals, '$allocation.data.unit');
    const roleId = get(vals, '$allocation.data.role');

    const departmentData = allocationData.find(
      (department) => department.org_unit_id == departmentId
    );
    const departmentUuid = departmentData.org_unit_uuid;

    const roles = departmentData.roles;
    const roleData = roles.find((role) => role.role_id == roleId);
    const roleUuid = roleData.role_uuid;

    $http({
      url: '/api/v2/cm/case/allocation/check',
      method: 'GET',
      params: {
        department_uuid: departmentUuid,
        role_uuid: roleUuid,
        casetype_uuid: ctrl.casetype.reference,
      },
    }).then((response) => {
      if (!response.data.data.attributes.check_result) {
        zsConfirm(
          'De combinatie van afdeling & rol heeft geen toegang tot deze zaak.',
          'Doorgaan'
        )
          .then(() => {
            register(responseHandler, catchHandler);
          })
          .catch(() => {
            setSubmitting(false);
          });
      } else {
        register(responseHandler, catchHandler);
      }
    });
  };

  const responseHandler = (response) => {
    resetTouched();

    const pathToCaseNumber = eligibleForV2
      ? 'data.data.attributes.number'
      : shouldRedirectToCase
      ? 'data.result[0].number'
      : 'data.result.instance.data.case_id';
    const caseId = Number(get(response, pathToCaseNumber));
    const status = shouldRedirectToCase ? 'open' : 'new';
    const actions =
      eligibleForV2 && !shouldRedirectToCase
        ? [
            {
              type: 'link',
              label: `Zaak ${caseId} openen`,
              link: $state.href('case', { caseId }),
            },
          ]
        : [];

    if (parentCaseId) {
      $state.go('case', { caseId: parentCaseId });
    } else if (shouldRedirectToCase) {
      $state.go('case', { caseId });
    } else {
      $state.go('home');
    }

    return getCaseCreateSnack({ caseId, status }, actions);
  };

  const catchHandler = (response) => {
    setSubmitting(false);

    const req_id = response.headers()['zs-req-id'];

    const pathToError = eligibleForV2
      ? 'unknown'
      : shouldRedirectToCase
      ? 'data.result[0].type'
      : 'data.result.instance.type';

    const error_type = get(response, pathToError);

    if (error_type === 'node/allocation/permission') {
      return [
        'Zaak kon niet worden aangemaakt.',
        'De betreffende afdeling/rol heeft onvoldoende rechten.',
        'Neem contact op met uw beheerder voor meer informatie.',
      ].join(' ');
    }

    return [
      'Zaak kon niet worden aangemaakt.',
      'Neem contact op met uw beheerder voor meer informatie.',
      req_id !== undefined
        ? `<p><small>(Foutcode "${req_id}")</small></p>`
        : '',
      error_type,
    ].join(' ');
  };

  const hasAssignmentCheck = get(
    session,
    'configurable.assignment_department_check'
  );

  const isAssigningToDepartmentAndRole =
    get(vals, '$allocation.type') === 'org-unit';

  if (hasAssignmentCheck && isAssigningToDepartmentAndRole) {
    checkAndRegister(responseHandler, catchHandler);
  } else {
    register(responseHandler, catchHandler);
  }
};

export default registerCase;
