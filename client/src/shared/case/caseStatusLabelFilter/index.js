// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular
  .module('caseStatusLabelFilter', [])
  .filter('caseStatusLabel', [
    () => {
      return (status) => {
        return {
          resolved: 'Afgehandeld',
          stalled: 'Opgeschort',
          new: 'Nieuw',
          open: 'In behandeling',
        }[status];
      };
    },
  ]).name;
