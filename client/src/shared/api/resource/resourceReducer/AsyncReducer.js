// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import BaseReducer from './BaseReducer';

class AsyncReducer extends BaseReducer {
  constructor(options) {
    super(options);
  }

  $resolve(value) {
    if (this.$destroyed) {
      return;
    }

    this.$setState('resolved');

    this.setSrc(value);
  }

  $reject(err) {
    if (this.$destroyed) {
      return;
    }

    this.$setState('rejected');

    this.$onError.invoke(err);
  }
}

export default AsyncReducer;
