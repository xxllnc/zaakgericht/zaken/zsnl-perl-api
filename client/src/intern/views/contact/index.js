// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import zsContactSummary from './zsContactSummary';
import zsContactNavigation from './zsContactNavigation';
import zsContactSummaryView from './tabs/zsContactSummaryView';
import zsContactCasesView from './tabs/zsContactCasesView';
import zsContactTimelineView from './tabs/zsContactTimelineView';
import zsContactInformationView from './tabs/zsContactInformationView';
import zsMessageList from '../../../shared/ui/zsMessageList';
import './styles.scss';

export default angular.module('Zaaksysteem.intern.contact', [
  zsContactSummary,
  zsContactNavigation,
  zsContactSummaryView,
  zsContactCasesView,
  zsContactTimelineView,
  zsContactInformationView,
  zsMessageList,
]).name;
