// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import zsModalModule from '../../../shared/ui/zsModal';
import template from './index.html';
import './styles.scss';

export default angular
  .module('Zaaksysteem.intern.version.route', [
    angularUiRouterModule,
    zsModalModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider.state('version', {
        url: '/version',
        auxiliary: true,
        controllerAs: 'vm',
        onActivate: [
          '$http',
          '$state',
          '$rootScope',
          '$window',
          '$compile',
          'zsModal',
          function ($http, $state, $rootScope, $window, $compile, zsModal) {
            let openModal = () => {
              let modal,
                unregister,
                scope = $rootScope.$new(true);

              modal = zsModal({
                el: $compile(angular.element(template))(scope),
                title: 'Release notes',
                classes: 'version-modal center-modal',
              });

              modal.onClose(() => {
                $state.go('^');
                return true;
              });

              modal.open();

              unregister = $rootScope.$on('$stateChangeStart', () => {
                $window.requestAnimationFrame(() => {
                  $rootScope.$evalAsync(() => {
                    modal.close().then(() => {
                      scope.$destroy();
                    });

                    unregister();
                  });
                });
              });
            };

            $window.requestAnimationFrame(() => {
              $rootScope.$evalAsync(openModal);
            });
          },
        ],
      });
    },
  ]).name;
