// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import configServiceModule from '../configService';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import fuzzy from 'fuzzy';
import wordNgrams from 'word-ngrams';
import seamlessImmutable from 'seamless-immutable';
import uniq from 'lodash/uniq';
import flattenDeep from 'lodash/flattenDeep';
import identity from 'lodash/identity';
import take from 'lodash/take';
import isArray from 'lodash/isArray';
import get from 'lodash/get';

export default angular
  .module('Zaaksysteem.mor.filterService', [
    configServiceModule,
    composedReducerModule,
  ])
  .factory('filterService', [
    '$rootScope',
    'composedReducer',
    'configService',
    ($rootScope, composedReducer, configService) => {
      const NGRAM_RANGE = [1, 2, 3];

      let index = seamlessImmutable([]),
        results,
        matches,
        indexAttrReducer;

      indexAttrReducer = composedReducer({ scope: $rootScope }, () =>
        configService.getAttributes()
      ).reduce((attributes) => {
        if (attributes) {
          return attributes
            .filter(
              (attr) =>
                attr.external_name !== 'voorstelzaaknummer' &&
                attr.external_name !== 'voorsteldocumenten'
            )
            .map((attr) => attr.object.column_name.replace('attribute.', ''));
        }
      });

      return {
        getAttributesToIndex: indexAttrReducer.data,
        addToIndex: (elements) => {
          let attrsToIndex = configService.getAttributes(); /* Currently, the app only indexes attributes that have been set in the koppelprofiel of the app. */

          let getNGramsFromElement = (el) => {
            return attrsToIndex
              .map(
                (attrName) =>
                  el.instance.attributes[
                    attrName.object.column_name.replace('attribute.', '')
                  ] ||
                  get(
                    el,
                    'instance.case_location.openbareruimte.human_identifier'
                  ) ||
                  get(
                    el,
                    'instance.case_location.nummeraanduiding.human_identifier'
                  ) ||
                  el.instance.subject_external ||
                  el.instance.casetype.instance.name
              )
              .filter(identity)
              .map((elValue) => {
                let stringValue = isArray(elValue) ? elValue.join() : elValue;

                return NGRAM_RANGE.map((nGramLength) => {
                  return wordNgrams.listAllNGrams(
                    wordNgrams.buildNGrams(stringValue, nGramLength, {
                      caseSensitive: false,
                      includePunctuation: false,
                    })
                  );
                });
              });
          };

          index = seamlessImmutable(
            uniq(flattenDeep(elements.map(getNGramsFromElement)))
          );
        },
        getFromIndex: (query) => {
          results = take(fuzzy.filter(query, index.asMutable()), 15);
          matches = results.map((el) => el.string);
          return matches;
        },
      };
    },
  ]).name;
