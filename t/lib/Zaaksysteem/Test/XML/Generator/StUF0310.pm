package Zaaksysteem::Test::XML::Generator::StUF0310;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::Test::XML qw(:all);

use Zaaksysteem::XML::Generator::StUF0310;
use DateTime;

sub test_xml_out {

    my $generator = Zaaksysteem::XML::Generator::StUF0310->new();

    my $now    = DateTime->now();
    my $dt_xml = { _ => $now->strftime('%Y%m%d') };

    my %data = (
        # We can't use generate_stuurgegevens here, as it requires a
        # document to take the recipient from.
        stuurgegevens => {
            user      => 'foo',
            recipient => 'bar',
            zender    => {
                organisatie   => 'organisation',
                gebruiker     => 'gebraucher',
                applicatie    => 'applicatione',
                administratie => 'administratone',
            },
            transaction_id => 666,    # the devil is in the details
            timestamp => $now->strftime('%Y%m%d%H%M%S%3N'),
        },

        parameters => {
            template_name => 'john',
            job_id        => 121.10,
        },

        # typing "documentspecificatie" into the template a ton of times: nope.
        ds => {
            document_id => 42,
            zaak        => {
                identificatie            => { _ => '00001' },
                einddatum                => $dt_xml,
                einddatumGepland         => $dt_xml,
                startdatum               => $dt_xml,
                uiterlijkeEinddatum      => $dt_xml,
                registratiedatum         => $dt_xml,
                datumVernietigingDossier => $dt_xml,

                zaakniveau         => 1,
                deelzakenIndicatie => { _ => 'J' },

                heeft => [ ],

                ## Betrokkene units
                # heeftAlsInitiator
                # heeftAlsUitvoerende
                # heeftAlsVerantwoordelijke
                # heeftAlsGemachtigde
                # heeftAlsBelanghebbende
                # heeftAlsOverigBetrokkene

                ## Document units
                #heeftRelevant => [{ entititeittype => 'ZAKEDC', }],
                isVan         => {
                    gerelateerde => {
                        code         => 'morse',
                        omschrijving => '...',
                        ingangsdatum => $dt_xml,
                    },
                },
            },
            kenmerken => [{
                    naam => 'undef', type => '', value => undef
                },
                {
                    naam => 'foo', type => '', value => 'foo',
                }
            ],
        },
    );

    my $xml_out;
    lives_ok(
        sub {
            $xml_out = $generator->start_document_creation('writer', \%data);
        },
        'XML generation with empty attribute values'
    );

}


1;

__END__

=head1 NAME

Zaaksysteem::Test::XML::Generator::StUF0310 - A test file for XML generators

=head1 DESCRIPTION

=head1 SYNOPSIS

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
