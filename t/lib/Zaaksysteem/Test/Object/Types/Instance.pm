package Zaaksysteem::Test::Object::Types::Instance;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::Instance;

sub test_instance_object {

    my %args = (
        customer_type => 'government',
        owner         => 'betrokkene-bedrijf-42',
        fqdn          => 'test.zaaksysteem.nl',
        label         => 'testsuite instance object',
        template      => 'mintlab',
    );

    my $instance = Zaaksysteem::Object::Types::Instance->new(%args);
    isa_ok($instance, 'Zaaksysteem::Object::Types::Instance');

    foreach (keys %args) {
        is($instance->$_, $args{$_}, "instance has correct $_: $args{$_}");
    }

}

sub test_instance_new_empty {
    my $instance = Zaaksysteem::Object::Types::Instance->new_empty();
    isa_ok($instance, 'Zaaksysteem::Object::Types::Instance');
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::Host - Test a control panel host object

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::Instance

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
