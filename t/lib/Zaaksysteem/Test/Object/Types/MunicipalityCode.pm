package Zaaksysteem::Test::Object::Types::MunicipalityCode;

use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::MunicipalityCode;

sub test_new {
    my $municipality_code;
    lives_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new(
                    dutch_code  => 193,
                    label       => 'Zwolle',
                )
        },
        'Creates a new object'
    );
    isa_ok($municipality_code, 'Zaaksysteem::Object::Types::MunicipalityCode');
    is($municipality_code->dutch_code, '0193', '... and has code "0193"');
}

sub test_new_from_dutch_code {
    my $municipality_code;
    lives_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new_from_dutch_code( 9999 )
        },
        'Creates a new object from dutch_code'
    );
    is($municipality_code->label, 'Testgemeente', '... and is "Testgemeente"');

    undef $municipality_code;
    throws_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new_from_dutch_code( 0 )
        }, qr|object/types/municipalitycode/unknown_dutch_code|,
        'Can not create a new object from zero'
    );

    undef $municipality_code;
    throws_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new_from_dutch_code( 999999999 )
        }, qr|object/types/municipalitycode/unknown_dutch_code|,
        'Can not create one with none existing number'
    );

}

sub test_new_from_name {
    my $municipality_code;
    lives_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new_from_name( 'GRAFT-DE RIJP' )
        },
        'Creates a new object from label "GRAFT-DE RIJP"'
    );
    is($municipality_code->dutch_code, '0365', '... and has code "0365"');

    undef $municipality_code;
    throws_ok(
        sub {
            $municipality_code = Zaaksysteem::Object::Types::MunicipalityCode
                ->new_from_name( 'Skarsterlan' ) # Skarsterlân
        }, qr|object/types/municipalitycode/unknown_name|,
        'Does need to match exactly'
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::MunicipalityCode; - Test MunicipalityCode

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::MunicipalityCode;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
