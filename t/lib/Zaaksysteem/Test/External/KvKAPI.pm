package Zaaksysteem::Test::External::KvKAPI;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::External::KvKAPI;

sub _get_model {
    return Zaaksysteem::External::KvKAPI->new(
        api_key   => 'foobar',
        spoofmode => 1,
    );
}

sub test_api_handling_spoof {
    my $test = shift;

    $test->test_report->plan(2);

    my $model = _get_model();

    my $override = override(
        'WebService::KvKAPI::search' => sub {
            my $self   = shift;
            my %params = @_;
            cmp_deeply(
                \%params,
                { foo => 'bar' },
                "Delegation works for me (tm)"
            );
            return { resultaten => [] };
        }
    );
    my $rv = $model->search(foo => 'bar');
    cmp_deeply($rv, [], "... and no results were found");
}

sub test_params_parsing {

    my $model = _get_model();

    throws_ok(
        sub {
            $model->map_params_to_kvk_query({});
        },
        qr/No KvK query can be build/,
        "Unable to map invalid or non-existent params"
    );

    my $kvk_query = $model->map_params_to_kvk_query(
        {
            company             => 'Mintlab',
            coc_location_number => 12,
            coc_number          => 12,
        }
    );

    cmp_deeply(
        $kvk_query,
        {
            handelsnaam      => 'Mintlab',
            kvkNummer        => 12,
            vestigingsnummer => 12
        },
        "But company => mintlab can be mapped"
    );

}

sub test_parsing_search_response_to_subject {

    my $model = _get_model();

    my $kvk_company = {
        'handelsnaam' => 'Test EMZ Dagobert',
        'kvkNummer'   => '69599084',
        'plaats'      => 'Amsterdam',
        'links'       => [
            {
                'href' =>
                    'https://api.kvk.nl/test/api/v1/basisprofielen/69599084',
                'rel' => 'basisprofiel'
            },
            {
                'href' =>
                    'https://api.kvk.nl/test/api/v1/vestigingsprofielen/000038509504',
                'rel' => 'vestigingsprofiel'
            }
        ],
        'vestigingsnummer' => '000038509504',
        'type'             => 'hoofdvestiging',
        'straatnaam'       => 'Abebe Bikilalaan'
    };

    my $subject = $model->map_kvk_company_to_subject($kvk_company);
    isa_ok($subject, "Zaaksysteem::Object::Types::Subject");
    my $company = $subject->subject;
    lives_ok(
        sub {
            $subject->subject->check_object;
        },
        "Sane object found my dear",
    );

    my $address = $company->address_residence;
    isa_ok(
        $address,
        "Zaaksysteem::Object::Types::AddressIncomplete",
        "Got a residential address"
    );
    is($address->street, 'Abebe Bikilalaan', '.. with the correct streetname');
    is($address->street_number, undef,       '.. and number');
    is($address->zipcode,       undef,       '.. and zipcode');
    is($address->city,          'Amsterdam', '.. and city');
    is($address->country,       undef,       '.. and country');
}

sub test_parsing_basic_profile_response_to_subject {

    my $model = _get_model();

    my $kvk_company = {
        'formeleRegistratiedatum' => '20170710',
        'kvkNummer'               => '69599084',
        'totaalWerkzamePersonen'  => 1,
        'naam'                    => 'Test EMZ Dagobert',
        'links'                   => [
            {
                'href' =>
                    'https://api.kvk.nl/test/api/v1/basisprofielen/69599084',
                'rel' => 'self'
            },
            {
                'rel'  => 'vestigingen',
                'href' =>
                    'https://api.kvk.nl/test/api/v1/basisprofielen/69599084/vestigingen'
            }
        ],
        'materieleRegistratie' => { 'datumAanvang' => '20170108' },
        'indNonMailing'        => 'Ja',
        '_embedded'            => {
            'hoofdvestiging' => {
                'eersteHandelsnaam'       => 'Test EMZ Dagobert',
                'indCommercieleVestiging' => 'Ja',
                'adressen'                => [
                    {
                        'indAfgeschermd' => 'Nee',
                        'straatnaam'     => 'Abebe Bikilalaan',
                        'volledigAdres'  =>
                            'Abebe Bikilalaan 17                                 1034WL Amsterdam',
                        'land'       => 'Nederland',
                        'huisnummer' => 17,
                        'plaats'     => 'Amsterdam',
                        'postcode'   => '1034WL',
                        'type'       => 'bezoekadres',
                        'geoData'    => {
                            'addresseerbaarObjectId' => '0402010001630118',
                            'rijksdriehoekX'         => '140487.719',
                            'gpsLatitude'            => '52.2252061401591',
                            'nummerAanduidingId'     => '0402200001630180',
                            'gpsLongitude'           => '5.17481099331482',
                            'rijksdriehoekY'         => '470813.022',
                            'rijksdriehoekZ'         => '0'
                        },

                    }
                ],
                'links' => [
                    {
                        'rel'  => 'self',
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/basisprofielen/69599084/hoofdvestiging'
                    },
                    {
                        'rel'  => 'vestigingen',
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/basisprofielen/69599084/vestigingen'
                    },
                    {
                        'rel'  => 'basisprofiel',
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/basisprofielen/69599084'
                    },
                    {
                        'rel'  => 'vestigingsprofiel',
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/vestigingsprofielen/000038509504'
                    }
                ],
                'vestigingsnummer'       => '000038509504',
                'indHoofdvestiging'      => 'Ja',
                'kvkNummer'              => '69599084',
                'totaalWerkzamePersonen' => 1
            },
            'eigenaar' => {
                'links' => [
                    {
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/basisprofielen/69599084/eigenaar',
                        'rel' => 'self'
                    },
                    {
                        'rel'  => 'basisprofiel',
                        'href' =>
                            'https://api.kvk.nl/test/api/v1/basisprofielen/69599084'
                    }
                ],
                'rechtsvorm'            => 'Eenmanszaak',
                'uitgebreideRechtsvorm' => 'Eenmanszaak'
            }
        },
        'sbiActiviteiten' => [
            {
                'sbiOmschrijving'    => 'Slachterijen (geen pluimvee-)',
                'indHoofdactiviteit' => 'Ja',
                'sbiCode'            => '1011'
            },
            {
                'sbiOmschrijving'    => 'Pluimveeslachterijen',
                'sbiCode'            => '1012',
                'indHoofdactiviteit' => 'Nee'
            },
            {
                'sbiOmschrijving' => 'Vleesverwerking (niet tot maaltijden)',
                'indHoofdactiviteit' => 'Nee',
                'sbiCode'            => '1013'
            }
        ],
        'handelsnamen' => [
            {
                'naam'     => 'Test EMZ Dagobert',
                'volgorde' => 0
            },
            {
                'volgorde' => 1,
                'naam'     => 'Tweede handelsnaam 1MZ'
            },
            {
                'volgorde' => 2,
                'naam'     => 'Derde handelsnaam 1MZ'
            },
            {
                'naam'     => 'Vierde handelsnaam 1MZ',
                'volgorde' => 3
            }
        ]
    };
    my $subject = $model->map_kvk_company_to_subject($kvk_company);
    isa_ok($subject, "Zaaksysteem::Object::Types::Subject");

    my $company = $subject->subject;
    lives_ok(
        sub {
            $company->check_object;
        },
        "Sane object found my dear",
    );

    my $address = $company->address_residence;
    isa_ok(
        $address,
        "Zaaksysteem::Object::Types::Address",
        "Got a residential address"
    );
    is($address->street, 'Abebe Bikilalaan', '.. with the correct streetname');
    is($address->street_number,  '17',          '.. and number');
    is($address->zipcode,        '1034WL',      '.. and zipcode');
    is($address->city,           'Amsterdam',   '.. and city');
    is($address->country->label, 'Nederland',   '.. and country');
    is($address->latitude,  '52.2252061401591', '.. and correct latitude');
    is($address->longitude, '5.17481099331482', '.. and correct longitude');
    is($address->bag_id,    '0402200001630180', '.. and correct BAG ID');

    my $sbi = $company->main_activity;
    is($sbi->code, '1011', "Main activity has correct code");
    is($sbi->description, 'Slachterijen (geen pluimvee-)', '.. with the correct description');

    $sbi = $company->secondairy_activities;
    is(@$sbi, 2, "Got two other SBI's");

    is($company->company_type->label, 'Eenmanszaak');
}


sub test_parsing_location_profile_response_to_subject {

    my $model = _get_model();

    my $kvk_company = {
        'voltijdWerkzamePersonen' => 1,
        'indHoofdvestiging'       => 'Ja',
        'materieleRegistratie'    => { 'datumAanvang' => '20150828' },
        'formeleRegistratiedatum' => '20200828',
        'kvkNummer'               => '69599084',
        'sbiActiviteiten'         => [
            {
                'sbiOmschrijving'    => 'Slachterijen (geen pluimvee-)',
                'sbiCode'            => '1011',
                'indHoofdactiviteit' => 'Ja'
            },
            {
                'indHoofdactiviteit' => 'Nee',
                'sbiOmschrijving'    => 'Pluimveeslachterijen',
                'sbiCode'            => '1012'
            },
            {
                'sbiCode'         => '1013',
                'sbiOmschrijving' => 'Vleesverwerking (niet tot maaltijden)',
                'indHoofdactiviteit' => 'Nee'
            }
        ],
        'deeltijdWerkzamePersonen' => 0,
        'vestigingsnummer'         => '000038509504',
        'links'                    => [
            {
                'rel'  => 'self',
                'href' =>
                    'https://api.kvk.nl/test/api/v1/vestigingsprofielen/000038509504'
            },
            {
                'rel'  => 'basisprofiel',
                'href' =>
                    'https://api.kvk.nl/test/api/v1/basisprofielen/69599084'
            }
        ],
        'eersteHandelsnaam'       => 'Test EMZ Dagobert',
        'totaalWerkzamePersonen'  => 1,
        'indCommercieleVestiging' => 'Ja',
        'adressen'                => [
            {
                'type'          => 'bezoekadres',
                'plaats'        => 'Amsterdam',
                'land'          => 'Nederland',
                'volledigAdres' =>
                    'Abebe Bikilalaan 17                         1034WL Amsterdam',
                'straatnaam'     => 'Abebe Bikilalaan',
                'huisnummer'     => 17,
                'indAfgeschermd' => 'Nee',
                'postcode'       => '1034WL',
                'geoData'        => {
                    'rijksdriehoekZ'         => '0',
                    'gpsLongitude'           => '5.17481099331482',
                    'nummerAanduidingId'     => '0402200001630180',
                    'gpsLatitude'            => '52.2252061401591',
                    'addresseerbaarObjectId' => '0402010001630118',
                    'rijksdriehoekY'         => '470813.022',
                    'rijksdriehoekX'         => '140487.719'
                },
            }
        ],
        'indNonMailing' => 'Ja'
    };
    my $subject = $model->map_kvk_company_to_subject($kvk_company);
    isa_ok($subject, "Zaaksysteem::Object::Types::Subject");

    my $company = $subject->subject;
    lives_ok(
        sub {
            $company->check_object;
        },
        "Sane object found my dear",
    );

    my $address = $company->address_residence;
    isa_ok(
        $address,
        "Zaaksysteem::Object::Types::Address",
        "Got a residential address"
    );
    is($address->street, 'Abebe Bikilalaan', '.. with the correct streetname');
    is($address->street_number,  '17',          '.. and number');
    is($address->zipcode,        '1034WL',      '.. and zipcode');
    is($address->city,           'Amsterdam',   '.. and city');
    is($address->country->label, 'Nederland',   '.. and country');
    is($address->latitude,  '52.2252061401591', '.. and correct latitude');
    is($address->longitude, '5.17481099331482', '.. and correct longitude');
    is($address->bag_id,    '0402200001630180', '.. and correct BAG ID');

    my $sbi = $company->main_activity;
    is($sbi->code, '1011', "Main activity has correct code");
    is($sbi->description, 'Slachterijen (geen pluimvee-)', '.. with the correct description');

    $sbi = $company->secondairy_activities;
    is(@$sbi, 2, "Got two other SBI's");


}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::External::KvKAPI - Tests for the KvKAPI model

=head1 DESCRIPTION

Glue layer between Zaaksysteem and KvKAPI.

Queries and transforms KvKAPI calls to ZS objects.

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::External::KvKAPI;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
