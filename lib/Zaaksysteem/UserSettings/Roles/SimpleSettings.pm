package Zaaksysteem::UserSettings::Roles::SimpleSettings;

use Moose::Role;

=head1 NAME

Zaaksysteem::UserSettings::Roles::SimpleSettings - A way to create a simple class which is an interface
to the usersettings hash.

=head1 SYNOPSIS

    package Zaaksysteem::UserSettings::SimpleInterface;

    use Moose;
    with qw/Zaaksysteem::UserSettings::Roles::SimpleSetting/;

    has '_setting_list' => (is => 'rw', default => sub { return ['version','test' ]; });
    has 'version'       => (is => 'rw', default => 2);
    has 'test'          => (is => 'rw', default => 1);

=head1 DESCRIPTION

Use this role for a simple way to inflate and deflate usersettings into an array

=head1 ATTRIBUTES

=head2 _setting_list

Override this attribute in your main object, exposing the attributes which you would like to
save and inflate from the settings object

=cut

has '_setting_list'     => (
    is          => 'rw',
    default     => sub { return [] },
    isa         => 'ArrayRef',
);

=head1 PRIVATE METHODS

=head2 _deflate_to_settings

Turns this object into a hash for user settings

=cut

sub _deflate_to_settings {
    my $self        = shift;

    return {
        map ( { $_ => $self->$_ } grep ({ my $pred = 'has_' . $_; $self->$pred } @{ $self->_setting_list }) ),
    };
}

=head2 _inflate_from_settings

Turns given settings into this object

=cut

sub _inflate_from_settings {
    my $self        = shift;
    my $settings    = shift;

    for my $key (@{ $self->_setting_list }) {
        next unless defined $settings->{$key};

        $self->$key($settings->{$key});
    }
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
