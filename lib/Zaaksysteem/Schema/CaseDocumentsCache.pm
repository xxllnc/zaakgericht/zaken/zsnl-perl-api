use utf8;
package Zaaksysteem::Schema::CaseDocumentsCache;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseDocumentsCache

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<case_documents_cache>

=cut

__PACKAGE__->table("case_documents_cache");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 0

=head2 value

  data_type: 'text[]'
  is_nullable: 1

=head2 value_v0

  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=head2 value_v1

  data_type: 'jsonb'
  default_value: '[]'
  is_nullable: 0

=head2 magic_string

  data_type: 'text'
  is_nullable: 0

=head2 library_id

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 0 },
  "value",
  { data_type => "text[]", is_nullable => 1 },
  "value_v0",
  { data_type => "jsonb", default_value => "[]", is_nullable => 0 },
  "value_v1",
  { data_type => "jsonb", default_value => "[]", is_nullable => 0 },
  "magic_string",
  { data_type => "text", is_nullable => 0 },
  "library_id",
  { data_type => "integer", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</case_id>

=item * L</library_id>

=back

=cut

__PACKAGE__->set_primary_key("case_id", "library_id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-02-18 12:28:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:I+mmJT9b+JV1fxY81+s6ig


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
