use utf8;
package Zaaksysteem::Schema::CaseAttributes;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CaseAttributes

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<case_attributes>

=cut

__PACKAGE__->table("case_attributes");
__PACKAGE__->result_source_instance->view_definition(" SELECT z.id AS case_id,\n    COALESCE(zk.value, '{}'::text[]) AS value,\n    bk.magic_string,\n    bk.id AS library_id,\n    bk.value_type,\n    bk.type_multiple AS mvp\n   FROM (((zaak z\n     JOIN zaaktype_kenmerken ztk ON ((z.zaaktype_node_id = ztk.zaaktype_node_id)))\n     JOIN bibliotheek_kenmerken bk ON (((bk.id = ztk.bibliotheek_kenmerken_id) AND (bk.value_type <> ALL (ARRAY['file'::text, 'appointment'::text])))))\n     LEFT JOIN zaak_kenmerk zk ON (((z.id = zk.zaak_id) AND (zk.bibliotheek_kenmerken_id = ztk.bibliotheek_kenmerken_id))))\n  GROUP BY z.id, COALESCE(zk.value, '{}'::text[]), bk.magic_string, bk.id, bk.value_type");

=head1 ACCESSORS

=head2 case_id

  data_type: 'integer'
  is_nullable: 1

=head2 value

  data_type: 'text[]'
  is_nullable: 1

=head2 magic_string

  data_type: 'text'
  is_nullable: 1

=head2 library_id

  data_type: 'integer'
  is_nullable: 1

=head2 value_type

  data_type: 'text'
  is_nullable: 1

=head2 mvp

  data_type: 'boolean'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "case_id",
  { data_type => "integer", is_nullable => 1 },
  "value",
  { data_type => "text[]", is_nullable => 1 },
  "magic_string",
  { data_type => "text", is_nullable => 1 },
  "library_id",
  { data_type => "integer", is_nullable => 1 },
  "value_type",
  { data_type => "text", is_nullable => 1 },
  "mvp",
  { data_type => "boolean", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-09-13 15:28:56
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:hXg36HXFjhfylSsty7+xmg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
