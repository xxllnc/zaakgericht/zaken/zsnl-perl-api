use utf8;
package Zaaksysteem::Schema::SearchQuery;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::SearchQuery

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<search_query>

=cut

__PACKAGE__->table("search_query");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'search_query_id_seq'

=head2 settings

  data_type: 'text'
  is_nullable: 1

=head2 ldap_id

  data_type: 'integer'
  is_nullable: 1

=head2 name

  data_type: 'varchar'
  is_nullable: 1
  size: 256

=head2 sort_index

  data_type: 'integer'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "search_query_id_seq",
  },
  "settings",
  { data_type => "text", is_nullable => 1 },
  "ldap_id",
  { data_type => "integer", is_nullable => 1 },
  "name",
  { data_type => "varchar", is_nullable => 1, size => 256 },
  "sort_index",
  { data_type => "integer", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 RELATIONS

=head2 search_query_delens

Type: has_many

Related object: L<Zaaksysteem::Schema::SearchQueryDelen>

=cut

__PACKAGE__->has_many(
  "search_query_delens",
  "Zaaksysteem::Schema::SearchQueryDelen",
  { "foreign.search_query_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07046 @ 2017-06-15 14:24:44
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:MtVQoRVhFqwCCiDnlhs94w





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

