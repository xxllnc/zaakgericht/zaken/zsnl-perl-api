package Zaaksysteem::Controller::Auth::Token;
use Moose;

BEGIN { extends 'Zaaksysteem::Controller' }

use BTTW::Tools;
use JSON::XS;
use LWP::UserAgent;
use Zaaksysteem::Constants qw(LOGGING_COMPONENT_USER);
use Zaaksysteem::Types qw(UUID NonEmptyStr);
use PerlX::Maybe::XS qw(provided maybe);
use Zaaksysteem::Constants::Users qw(REGULAR);

=head1 NAME

Zaaksysteem::Controller::Auth::Token - Token-based authentication

=head1 METHODS

=head2 base

Base controller for token authentication.

=cut

sub base : Chained('/') : PathPart('auth/token') : CaptureArgs(0) {}

=head2 login

Log in using a session invitation token

=cut

sub login : Chained('base') {
    my $self = shift;
    my $c = shift;

    if (!$c->model('DB::Config')->get('enable_bil')) {
        $c->detach('/forbidden');
        return;
    }

    my $token = $c->req->params->{auth_token};
    unless ($c->authenticate({ token => $token }, 'token')) {
        $self->request_unauthorized($c);
    }

    $c->user->log_login_attempt(
        ip      => $c->get_client_ip,
        success => 1,
        method  => 'invitation-token',
    );

    $c->model('DB::Logging')->trigger(
        'auth/login/token_use',
        {
            'component' => LOGGING_COMPONENT_USER,
            'data' => {
                invitation_id => $c->session->{auth}{used_invitation_id},
                subject_id    => $c->user->id,
            },
        },
    );

    $c->forward('/auth/page/login_success');
    $c->detach('/index');
}

=head2 generate

Generates a new session invitation for the admin user.

Can only be called using the platform key for authentication.

=cut

define_profile generate => (
    required => {
        remote_user => NonEmptyStr,
    },
    optional => {
        username => NonEmptyStr,
    },
);

sub generate : Chained('base') {
    my $self = shift;
    my $c = shift;

    $c->assert_post();
    $c->assert_platform_access();
    my $opts = assert_profile($c->req->params)->valid;

    if (exists $opts->{username} && ref $opts->{username}) {
        throw(
            'auth/token/generate/multiple_usernames',
            'Multiple usernames specified, cannot continue.'
        );
    }
    if (ref $opts->{remote_user}) {
        throw(
            'auth/token/generate/multiple_remote_users',
            'Multiple remote users specified, cannot continue.'
        );
    }

    my $username = $opts->{username} || [ 'admin', 'beheerder' ];

    my $subject = $c->model('DB::Subject')->search_active(
        {
            username => $username,
        },
        {
            # "admin" sorts before "beheerder" so it'll get used preferentially
            order_by => { -asc => 'username' },
            rows => 1,
        }
    )->first;

    throw(
        'auth/token/generate/no_user',
        'No user could be found to generate a login token for',
    ) unless $subject;

    my $invitation = $c->model('Session::Invitation')->create({
        subject      => $subject->as_object,
        date_expires => DateTime->now->add(minutes => 1),
    });

    $c->model('DB::Logging')->trigger(
        'auth/login/token_create',
        {
            'component' => LOGGING_COMPONENT_USER,
            'data' => {
                subject_id    => $c->user->id,
                remote_user   => $opts->{remote_user},
                invitation_id => $invitation->id,
            },
        },
    );

    my $uri = URI->new();
    $uri->scheme('https');
    $uri->host($c->req->uri->host);
    $uri->path('/auth/token/login');
    $uri->query_form({ auth_token => $invitation->token });

    $c->stash->{json} = { login_url => "$uri" };

    $c->detach('Zaaksysteem::View::JSON');
}

=head2 remote_login

Counterpart of L</generate>. Used on the "Mintlab" instance only, and calls
"/auth/token/generate" on the target instance.

Has one named argument: C<instance_id>, the UUID of the instance to log in to.

=cut

define_profile remote_login => (
    required => {
        instance_id => UUID
    },
    optional => {
        remote_user => NonEmptyStr,
        host_id     => UUID,
    },
);

sub remote_login : Local {
    my $self = shift;
    my $c = shift;

    unless ($c->check_user_mask(REGULAR)) {
        $c->log->error("Disallowing, no logged-in user.");
        $c->detach('/forbidden');
    }

    my $params = assert_profile($c->req->params)->valid;


    my $instance  = $c->model('Object')->retrieve(uuid => $params->{instance_id});
    my $host;

    if (exists $params->{host_id}) {
        $host  = $c->model('Object')->retrieve(uuid => $params->{host_id});
        my $relationships = $c->model('DB')->schema->resultset('ObjectRelationships')->search(
            {
                '-or'   => [
                    { object1_uuid => $instance->id, object2_type => 'host', object2_uuid => $host->id },
                    { object2_uuid => $instance->id, object1_type => 'host', object1_uuid => $host->id },
                ],
            },
        );

        my $rel = $relationships->next;
        if (!$rel) {
            throw("remote_login/host/not_exists", "Unable to login to host, it does not exist for this instance!");
        }
        if ($relationships->next) {
            throw("remote_login/host/multiple", "Unable to login to host, multiple relationships found!");
        }
    }

    my $login_url = $c->model('Auth::Remote')->request_login_url(
        instance => $instance,
        user     => $c->user,
        maybe host => $host,
    );

    $c->res->redirect($login_url);
    $c->detach();
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
