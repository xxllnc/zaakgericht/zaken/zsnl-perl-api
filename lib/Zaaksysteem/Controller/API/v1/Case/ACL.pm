package Zaaksysteem::Controller::API::v1::Case::ACL;

use Moose;
use namespace::autoclean;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::ACL - ACL calls for viewing and altering ACL's

=head1 DESCRIPTION

TODO

=cut

use BTTW::Tools;
use Zaaksysteem::Object::Types::Case::ACL;

sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/acl> routing namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('acl') : CaptureArgs(0) { }

=head2 list

=head3 URL Path

C</api/v1/case/[CASE_UUID]/acl>

=cut

sub list : Chained('base') : PathPart('') : Args(0) : RO {
    my ($self, $c)  = @_;

    $c->forward('_get_and_send_acls');
}

=head2 update

=head3 URL Path

C</api/v1/case/[CASE_UUID]/acl/update>

=cut

define_profile update => (
    optional => {
        values     => 'HashRef',
        cascade_to => 'HashRef',
    }
);

sub update : Chained('base') : PathPart('update') : Args(0) : RO {
    my ($self, $c)  = @_;

    $self->assert_post($c);
    my $params  = assert_profile($c->req->params)->valid;

    my @rawacls = ($params->{values} ? (@{ $params->{values} }) : ());
    my $cascade = $params->{cascade_to} // {};

    my $case = $c->stash->{zaak};

    $c->model('Zaak')->update_specific_acls(
        $case,
        \@rawacls,
        $cascade,
    );

    $c->forward('_get_and_send_acls');
}

=head1 PRIVATE ACTIONS

=head2 _get_and_send_acls

Method for retrieving and sending the ACLS

=cut

sub _get_and_send_acls : Private {
    my ($self, $c) = @_;

    my @acls = $self->_get_acls($c);

    $c->stash->{ result } = Zaaksysteem::API::v1::ArraySet->new(
        content => [ @acls ],
    )->init_paging($c->req);
}

=head1 PRIVATE METHODS

=head2 _get_acls

    my @object_types_case_acls = $self->_get_acls($c->model('ObjectData')->search({object_class => 'case'})->first);

Return a list of L<Zaaksysteem::Object::Types::Case::ACL> objects belonging to this case and type of this case.

=cut

sub _get_acls {
    my ($self, $c) = @_;

    my $case = $c->stash->{zaak};
    my $casetype_uuid = $case->zaaktype_id->get_column('uuid');

    ### Retrieve ACLs for the object and the type of the object (case (uuid) and casetype (class_uuid)).
    ### Make sure we only retrieve positions
    my $rs = $c->model('DB')->schema->resultset('ObjectAclEntry')->search(
        {
            '-or' => [
                {
                    entity_type => 'position',
                    scope       => 'instance',
                    object_uuid => $case->get_column('uuid'),
                },
                {
                    entity_type => 'position',
                    scope       => 'type',
                    object_uuid => $casetype_uuid,
                },
            ]
        }
    );

    ### Normalize data, so we can combine the capabilities in one array (['read','write',...])
    my %positions;
    while (my $acl = $rs->next) {
        if ($acl->scope eq 'type') {
            # Public:
            if (!$case->acl_groupname || $case->acl_groupname eq 'public') {
                next if $acl->groupname && $acl->groupname ne 'public';

            # Confidential or "other"
            } else {
                next if !$acl->groupname || $acl->groupname ne $case->acl_groupname;
            }
        }

        $positions{ $acl->scope . ':' . $acl->entity_id } //= [];
        push(@{ $positions{ $acl->scope . ':' . $acl->entity_id } }, $acl->capability);
    }

    ### Create ACL Objects from the combined acl rows.
    my @acls;
    for my $key (sort keys %positions) {
        my ($scope, $position) = split(':', $key);

        push(@acls,
            Zaaksysteem::Object::Types::Case::ACL->new(
                entity_type  => 'position',
                capabilities => [sort @{ $positions{$key} }],
                entity_id    => $position,
                scope        => $scope,
                read_only    => ($scope eq 'type' ? 1 : 0)
            )
        );
    }

    return @acls;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
