package Zaaksysteem::Controller::API::v1::Case::Relation;

use Moose;

BEGIN { extends 'Zaaksysteem::API::v1::Controller' }

=head1 NAME

Zaaksysteem::Controller::API::v1::Case::Relation - APIv1 controller for case relations

=head1 DESCRIPTION

This is a controller API class for C<api/v1/case>. Extensive documentation about this
API can be found in:

L<Zaaksysteem::Manual::API::V1::Case>

Tests showing the usage via the JSON API can be found in:

L<TestFor::Catalyst::Controller::API::V1::Case>

=cut

use BTTW::Tools;
use List::MoreUtils qw[any];
use Zaaksysteem::Types qw[UUID Boolean];
use Zaaksysteem::Object::Iterator;
use Zaaksysteem::API::v1::Set;


sub BUILD {
    my $self = shift;

    $self->add_api_context_permission('intern', 'extern');
}

=head1 ACTIONS

=head2 base

Reserves the C</api/v1/case/[CASE_UUID]/relation> routing namespace.

=cut

sub base : Chained('/api/v1/case/instance_base') : PathPart('relation') : CaptureArgs(0) { }

=head2 case_relation_base

Base for the relation modification endpoints. Retrieves the case object and
that of the "related case" being referenced, and puts both on the stash.

=cut

define_profile case_relation_base => (
    required => {
        related_id => UUID,
    },
);

sub case_relation_base : Chained('base') : PathPart('') : CaptureArgs(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    $c->stash->{related_case} = $self->_get_case($c, $opts->{related_id});
    return;
}


=head2 plain_case_relation

Return all the "normal" case relations of a case allowing a ZS caller to
retreive all relations without having to forge a huge ZQL query

=cut

sub plain_case_relation : Chained('base') : PathPart('') : Args(0) {
    my ($self, $c) = @_;

    my $case_id = $c->stash->{zaak}->id;

    my $relations = $c->model('DB::ViewCaseRelationship')->search_rs({
        case_id => $case_id,
        type    => 'plain',
    });

    $c->parse_search_query_db;

    my $cases = $c->stash->{cases}->search_rs(
        {
            'me.id' => { -in => $relations->get_column('relation_id')->as_query },
        },
        {
            columns => [qw(uuid id)]
        }
    );

    if (!$cases->{attrs}{order_by}) {
        $cases = $cases->search_rs(
            {
                'related_cases.type'    => 'plain',
                'related_cases.case_id' => $c->stash->{zaak}->id,
            },
            {
                join     => 'related_cases',
                order_by => { -asc => 'related_cases.order_seq' },
            }
        );
    }

    $c->stash->{result} = Zaaksysteem::API::v1::ResultSet->new(
        iterator => $cases
    )->init_paging($c->req);
    return;
}

=head2 add_object_relation

Builds a new case to object relationship, optionally copying attribute values
from the related object to the case.

=head3 URL Path

C</api/v1/case/[CASE_UUID]/relation/add_object>

=head3 Parameters

=over

=item * related_id

C<UUID> of the object to be related. Any type should work.

=item * blocks_deletion

Declares whether the relationship should prevent deletion of the object.

Defaults to false.

=item * copy_attribute_values

When true, will attempt to update the case with values from the newly related
object. This will overwrite exsting values, but log that it has happened.

=back

=cut

define_profile add_object_relation => (
    required => {
        related_id => UUID
    },
    optional => {
        blocks_deletion => Boolean,
        copy_attribute_values => Boolean
    }
);

sub add_object_relation : Chained('base') : PathPart('add_object') : Args(0) {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    $c->stash->{ related_object } = try {
        return $c->model('Object')->retrieve(
            uuid => $opts->{ related_id }
        );
    } catch {
        $c->log->error(sprintf(
            'Exception caught during object retrieval: %s',
            $_
        ));

        throw('api/v1/case/object_relation/retrieval_fault', sprintf(
            'Could not retrieve object "%s" to be related',
            $opts->{ related_id }
        ));
    };

    my $case_uuid = $c->stash->{zaak}->get_column('uuid');

    my %relation_create_args = (
        owner_object_uuid => $case_uuid,
        object1_uuid => $case_uuid,
        object1_type => 'case',
        type1 => 'related',
        title1 => $c->stash->{zaak}->case_title,

        object2_uuid => $c->stash->{ related_object }->id,
        object2_type => $c->stash->{ related_object }->type,
        type2 => 'related',
        title2 => $c->stash->{ related_object }->TO_STRING,

        blocks_deletion => $opts->{ blocks_deletion } ? 1 : 0
    );

    my $relation = try {
        return $c->stash->{case}->object_relationships_object1_uuids->create(
            \%relation_create_args);
    }
    catch {
        $c->log->error(
            sprintf('Exception caught while setting case relation: %s', $_));

        throw(
            'api/v1/case/object_relation/relation_fault',
            sprintf('Could not relate object "%s" to case',
                $c->stash->{related_object})
        );
    };

    if ($opts->{ copy_attribute_values }) {
        $c->forward('copy_attribute_values');
    }

    $c->detach('/api/v1/case/get');
}

=head2 add

Add a new relation with the case specified in the C<related_id> parameter.

=head3 URL Path

C</api/v1/case/[CASE_UUID]/relation/add>

=cut

sub add : Chained('case_relation_base') : PathPart('add') : Args(0) : RW {
    my ($self, $c) = @_;

    $c->model('DB::CaseRelation')->add_relation(
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id,
    );

    $c->stash->{zaak}->update_object_relationships;
    $c->stash->{case}->discard_changes();

    $c->detach('/api/v1/case/get');
}

=head2 delete

Remove the relation with the case specified in the C<related_id> parameter.

=head3 URL Path

C</api/v1/case/[CASE_UUID]/relation/remove>

=cut

sub delete : Chained('case_relation_base') : PathPart('remove') : Args(0) : RW {
    my ($self, $c) = @_;

    my $relation = $c->model('DB::CaseRelation')->get(
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id,
    );

    unless ($relation) {
        throw('api/v1/case/relation/retrieval_fault', sprintf(
            'Case relation to %s could not be retrieved. Unable to continue.',
            $c->stash->{related_case}->id,
        ));
    }

    $relation->delete();

    $c->model('DB::CaseRelation')->recalculate_order($c->stash->{zaak}->id);

    $c->stash->{zaak}->touch();
    $c->stash->{related_case}->touch();
    $c->stash->{case}->discard_changes();
    $c->stash->{zaak} = $c->stash->{case};
    $c->detach('/api/v1/case/get');
    return;
}

=head2 move

Change the order of relations with other cases, moving the relation with the
case specified in C<related_id> to after the relation with C<after> (or to the
beginning of C<after> is not specified).

=head3 URL Path

C</api/v1/case/[CASE_UUID]/relation/move>

=cut

define_profile move => (
    optional => {
        after => UUID,
    },
);

sub move : Chained('case_relation_base') : PathPart('move') : Args(0) : RW {
    my ($self, $c) = @_;

    my $opts = assert_profile($c->req->params)->valid;

    my $after_rel;
    if ($opts->{after}) {
        my $after_case = $self->_get_case($c, $opts->{after});

        $after_rel = $self->_get_relation(
            $c,
            $c->stash->{zaak}->id,
            $after_case->id
        );
    }

    my $relation = $self->_get_relation(
        $c,
        $c->stash->{zaak}->id,
        $c->stash->{related_case}->id
    );

    my @related_cases = $c->model('DB::CaseRelation')->get_sorted($c->stash->{zaak}->id);

    $c->log->debug(sprintf(
        "Order before move: %s",
        join(', ', map { $_->to_string } @related_cases)
    ));

    my ($hijacked) = grep { $_->id eq $relation->id } @related_cases;
    @related_cases = grep { $_->id ne $relation->id } @related_cases;

    my @sorted_cases;

    $c->log->debug(sprintf(
        'Moving relation %s after %s',
        $hijacked->to_string,
        $after_rel ? $after_rel->to_string : 'null'
    ));

    if($after_rel) {
        @sorted_cases = map {
            $_->id eq $after_rel->id ? ($_, $hijacked) : ($_)
        } @related_cases;
    } else {
        @sorted_cases = ($hijacked, @related_cases);
    }

    $c->log->debug(sprintf(
        'New order after move: %s',
        join(', ', map { $_->to_string } @sorted_cases)
    ));

    my $iter = 1;

    for my $rel (@sorted_cases) {
        $rel->order_seq($iter);
        $rel->update;

        $iter++;
    }

    $c->stash->{case}->touch();
    $c->stash->{case}->discard_changes();
    $c->stash->{zaak} = $c->stash->{case};

    $c->detach('/api/v1/case/get');
}

=head1 PRIVATE ACTIONS

=head2 copy_attribute_values

Copies attributes from the stashed C<related_object> to the stashed C<case>.

=cut

sub copy_attribute_values : Private {
    my ($self, $c) = @_;

    # Get all magic string mapable attribute names, filtering the date duo
    # since they are kinda magic. TODO fix scope-bloat for those two attributes
    my @attribute_names = grep {
        $_ ne 'date_created' && $_ ne 'date_modified'
    } $c->stash->{ related_object }->attribute_names;

    my $casetype = $c->model('Object')->inflate_from_relation($c->stash->{zaak}
            ->get_object_relation($c->stash->{zaak}->object_data, 'casetype'));

    # Seperate closure, grep-in-grep fuzzes $_ scope
    my $filter = sub {
        my $name = shift;

        return any { $_ eq $name } @attribute_names;
    };

    # Find all attribute values
    # 1 Iterate casetype phases
    # 2 Extract attribute definitions
    # 3 Find attributes that we're interested in
    # 4 Map the bibliotheek_kenmerk_id to the value on the object
    my %values = map {
        $_->{ catalogue_id } => $c->stash->{ related_object }->get_attribute_value(
            $_->{ magic_string }
        )
    } grep {
        $_->{ catalogue_id } && $filter->($_->{ magic_string })
    } map {
        @{ $_->attributes }
    } @{ $casetype->instance_phases };

    $c->stash->{ zaak }->zaak_kenmerken->update_fields({
        new_values => \%values,
        zaak => $c->stash->{ zaak }
    });

    $c->stash->{ case }->discard_changes;
    $c->stash->{ zaak }->touch;
}

=head1 PRIVATE METHODS

=head2 _get_case

=cut

sub _get_case {
    my ($self, $c, $uuid) = @_;

    my $case = $c->stash->{ cases }->search_rs({ uuid => $uuid })->first;
    return $case if $case;

    throw(
        'api/v1/case/not_found',
        "The case related object with UUID '$uuid' could not be found.",
        { http_code => 404 }
    );
}

=head2 _get_relation

=cut

sub _get_relation {
    my ($self, $c, $a, $b) = @_;

    my $relation = $c->model('DB::CaseRelation')->get($a, $b);

    unless ($relation) {
        throw('api/v1/case/relation/retrieval_fault', sprintf(
            "Case relation between '%s' and '%s' could not be found, unable to continue.",
            $a, $b
        ));
    }

    return $relation;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
