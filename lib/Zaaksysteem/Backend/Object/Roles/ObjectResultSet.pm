package Zaaksysteem::Backend::Object::Roles::ObjectResultSet;

use Moose::Role;
use Moose::Util qw/ensure_all_roles/;
use Scalar::Util qw/blessed/;

=head1 NAME

Z::B::Object::Roles::ObjectResultSet - Special ResultSet for ZQL Objects, blesses returned
rows with L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>

=head1 SYNOPSIS

    my $rs      = $object->from_type('case');

    ok($rs->count, 'Found objects from type "case"');

    my $case    = $rs->first;

    ## $case has role: Z::B::Object::Roles::ObjectResultSet

=head1 DESCRIPTION

This class will ensure every returned row from the database has the ZQL Object Component role
applied L<Zaaksysteem::Backend::Object::Roles::ObjectComponent>


=cut

=head1 METHODS

=head2 apply_zql_roles(@rows)

Return value: @rows

Applies role L<Zaaksysteem::Backend::Object::Roles::ObjectComponent> to every given
row in C<@rows>

=cut

sub apply_zql_roles {
    my $self                        = shift;

    for my $row (grep { blessed($_) } @_) {
        ensure_all_roles(
            $row,
            'Zaaksysteem::Backend::Object::Roles::ObjectComponent'
        );

        $row->_initialize_zql($self->{attrs}->{_zql_options} || {});
    }
}

with qw(Zaaksysteem::Search::ZQL::Role::ResultSet);

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
