package Zaaksysteem::Backend::Email::Transport;
use Zaaksysteem::Moose;

use Authen::SASL;
use Authen::SASL::Perl::XOAUTH2;
use Net::SMTP;

extends 'Email::Sender::Transport::SMTP';
with 'MooseX::Log::Log4perl';

has sasl => (
    isa => 'Authen::SASL',
    required => 0,
);

around _smtp_client => sub {
    my $orig = shift;
    my $self = shift;

    my $smtp = $self->$orig(@_);

    if ($self->sasl) {
        $self->_throw("sasl object AND sasl_username found")
            if defined $self->sasl_username;

        $self->log->debug("SASL object found - using it for auth");

        my $sasl = $self->sasl;

        unless ($smtp->auth($sasl)) {
            $self->_throw('SMTP failed AUTH', $smtp);
        }
    }

    return $smtp;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2022, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
