package Zaaksysteem::Backend::Sysin::Modules::Scanstraat;
use Moose;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Scanstraat - Document Intake module for Zaaksysteem

=head1 SYNOPISIS

=head1 METHODS

=cut

use Zaaksysteem::Constants qw(ZAAKSYSTEEM_BOFH);
use BTTW::Tools;
use Zaaksysteem::ZAPI::Form::Field;
use Zaaksysteem::ZAPI::Form;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
  Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
  Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
  /;

use constant INTERFACE_ID => 'scanstraat';

use constant INTERFACE_CONFIG_FIELDS => [
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_api_key',
        type        => 'text',
        label       => 'API key',
        required    => 1,
        description => 'Interface key voor scanstraat',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name     => 'interface_medewerker',
        type     => 'spot-enlighter',
        label    => 'Medewerker',
        required => 1,
        description =>
            'De standaardgebruiker welke aan de documenten wordt gekoppeld',
        data => {
            restrict    => 'contact/medewerker?include_admin=1',
            placeholder => 'Type uw zoekterm',
            label       => 'naam',
        }
        ),
];

use constant MODULE_SETTINGS => {
    name                          => INTERFACE_ID,
    label                         => 'Scanstraat',
    interface_config              => INTERFACE_CONFIG_FIELDS,
    direction                     => 'incoming',
    manual_type                   => ['text'],
    is_multiple                   => 0,
    is_manual                     => 0,
    retry_on_error                => 0,
    allow_multiple_configurations => 0,
    is_casetype_interface         => 0,
    has_attributes                => 0,
    trigger_definition => {
        upload_document => {
            method => 'upload_document',
            update => 1
        },
    },
    test_interface  => 0,
};

has interface => (is => 'rw');
has exception => (is => 'rw');

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig(%{MODULE_SETTINGS()});
};

=head2 upload_document

Document uploader

=head3 SYNOPISIS

=head3 ARGUMENTS

=head3 RETURNS

Ok or not

=cut

define_profile upload_document => (
    required => {
        api_key  => 'Str',
        filename => 'Str',
        filepath => 'Str',
    },
    optional => {
        document => 'Any',
    },
);

sub upload_document {
    my ($self, $params, $interface) = @_;
    $params = assert_profile($params)->valid;

    $self->interface($interface);
    my $transaction = $interface->process({
            external_transaction_id => 'unknown',
            input_data              => 'xml',
            processor_params        => {
                processor => '_process_document_upload',
                %$params,
            },
        },
    );

    if ($self->exception) {
        if (eval { $self->exception->isa('Throwable::Error') } ) {
            $self->exception->throw();
        }
        else {
            throw 'Scanstraat/upload_document', $self->exception;
        }
    }
    return $transaction;
}


sub _process_document_upload {
    my ($self, $record) = @_;

    my $transaction = $self->process_stash->{transaction};
    my $params      = $transaction->get_processor_params();
    my $interface   = $transaction->interface;

    try {
        my $config = $interface->get_interface_config;
        throw('process_document_upload', "Incorrect API key") if $params->{api_key} ne $config->{api_key};

        my $schema = $interface->result_source->schema;
        my $user;
        if ($config->{medewerker}) {
            my $rs = $schema->resultset('Subject')->search_rs({username => $config->{medewerker}{username}});
            my $c = $rs->count;
            if ($c > 1) {
                throw('scanstraat/multiple_users', "Multiple users found with username: $config->{medewerker}{username}");
            }
            elsif ($c == 0) {
                throw('scanstraat/no_user', "No user found with username: $config->{medewerker}{username}");
            }
            $user = 'betrokkene-medewerker-' . $rs->first->id;
        }
        # TODO: You should be able to remove this after 3.24.0rc1 or that
        # development cycle.
        # Fall back, handy for once we go live but forget to change the
        # scanstraat.
        if (!defined $user) {
            $self->log->warn("Scanstraat has no user defined, please correct this ASAP.");
            $user = ZAAKSYSTEEM_BOFH;
        }

        my $file = $interface->result_source->schema->resultset('File')->file_create({
            file_path => $params->{filepath},
            name      => $params->{filename},
            db_params => {
                created_by      => $user,
                reject_to_queue => 1,
            },
        });

        my $msg = sprintf("Document '%s' [%d] is geupload", $params->{filename}, $file->id);
        $record->preview_string($msg);
        $record->output($msg);
    }
    catch {
        if (eval {$_->isa('Throwable::Error')}) {
            my $err = $_->as_string;
            $record->output($err);
            $record->preview_string($err);
            $self->exception($_);
        }
        # ClamAv::Error::Client errors
        elsif (eval {$_->isa('Error::Simple')}) {
            my $err = $_->stringify;
            $record->output($err);
            $record->preview_string($err);
            $self->exception($err);
        }
        else {
            $record->output($_);
            $record->preview_string($_);
            $self->exception($_);
        }
        die $_;
    };
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

1;

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 INTERFACE_CONFIG_FIELDS

TODO: Fix the POD

=cut

=head2 INTERFACE_ID

TODO: Fix the POD

=cut

=head2 MODULE_SETTINGS

TODO: Fix the POD

=cut

=head2 ZAAKSYSTEEM_BOFH

TODO: Fix the POD

=cut

