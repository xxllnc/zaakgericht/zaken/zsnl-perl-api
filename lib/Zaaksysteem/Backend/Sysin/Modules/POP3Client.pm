package Zaaksysteem::Backend::Sysin::Modules::POP3Client;
use Moose;

extends 'Zaaksysteem::Backend::Sysin::Modules';

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::OAuth2Client
    Zaaksysteem::Backend::Sysin::Modules::Roles::ProcessorParams
    Zaaksysteem::Backend::Sysin::Modules::Roles::Tests
/;

use Zaaksysteem::Backend::Sysin::POP3Client::Model;
use Zaaksysteem::Backend::Sysin::Modules::Shared::OAuth2Client qw(
    build_oauth2_client_fields
    build_oauth2_form_object_values
);

my $INTERFACE_ID = 'pop3client';

my $INTERFACE_CONFIG_FIELDS = [
    build_oauth2_client_fields(),

    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_protocol',
        type        => 'select',
        label       => 'Protocol voor externe mailbox',
        required    => 1,
        when        => 'interface_kind === "custom"',
        description => 'Protocol dat gebruikt wordt om de externe mailbox te benaderen.<br>POP3S: Gebruikt het POP3-protocol over een reguliere TLS-beveiligde verbinding.<br>POP3 + STLS: Gebruikt het POP3-protocol in combinatie met STLS om de verbinding te beveiligen (RFC 2595).',
        default     => 'POP3S',
        data => {
            options => [
                { value => 'POP3S', label => 'POP3S (POP3 over TLS)' },
                { value => 'POP3',  label => 'POP3 + STLS (RFC2595)' },
                $ENV{ZS_INSECURE_POP}
                    ? { value => 'INSECURE', label => 'POP3 (no TLS/SSL, for testing purposes only)' }
                    : (),

            ],
        },
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_host',
        type        => 'text',
        label       => 'Hostname',
        required    => 1,
        when        => 'interface_kind === "custom"',
        description => 'Hostname van de mailserver waar de mailbox te vinden is.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_port',
        type        => 'text',
        label       => 'Poort',
        required    => 1,
        when        => 'interface_kind === "custom"',
        default     => '995',
        description => 'Poort waarop de mailserver te bereiken is. Standaard &quot;110&quot; voor POP3 en &quot;995&quot; voor POP3S.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_user',
        type        => 'text',
        label       => 'Gebruikersnaam',
        required    => 1,
        when        => 'interface_kind === "custom"',
        default     => 'username',
        description => 'Gebruikersnaam waarmee ingelogd wordt op de mailbox.',
    ),
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_password',
        type        => 'password',
        label       => 'Wachtwoord',
        when        => 'interface_kind === "custom"',
        required    => 1,
        default     => 'password',
        description => 'Wachtwoord waarmee ingelogd wordt op de mailbox.',
    ),
];

my $MODULE_SETTINGS = {
    name             => $INTERFACE_ID,
    interface_config => $INTERFACE_CONFIG_FIELDS,
    label            => 'POP3 Mailbox',
    direction        => 'incoming',
    manual_type      => [],
    is_multiple      => 1,
    is_manual        => 0,
    has_attributes   => 0,
    retry_on_error   => 0,
    is_casetype_interface         => 0,
    allow_multiple_configurations => 1,

    test_interface  => 1,
    test_definition => {
        description => qq{
            Om te controleren of de applicatie goed geconfigureerd is, kunt u
            hieronder een aantal tests uitvoeren. Hiermee controleert u de
            verbinding met uw mailserver.
        },
        tests => [
            {
                id => 1,
                label => "Test verbinding met server",
                name => "connection_test",
                method => "connection_test",
                description => "Test verbinding met de POP3-server en log in om te controleren of de gebruikersnaam en wachtwoord juist zijn.",
            }
        ],
    },
};


=head2 _load_values_into_form_object

=cut

around _load_values_into_form_object => sub {
    my $orig        = shift;
    my $self        = shift;
    my $opts        = $_[1]; # get options

    my $form        = $self->$orig(@_);
    my $interface   = $opts->{entry};

    $form->load_values({
        build_oauth2_form_object_values($opts, $interface)
    });

    return $form;
};

=head2 BUILDARGS

Wrapper to inject the module settings into newly-created instances.

=cut

around BUILDARGS => sub {
    my $orig  = shift;
    my $class = shift;

    return $class->$orig( %$MODULE_SETTINGS );
};

=head2 _get_model

Create a new instance of C<Zaaksysteem::Backend::Sysin::POP3Client::Model> and
return it.

=cut

sub _get_model {
    my ($self, $opts) = @_;

    my $interface = $opts->{interface};
    my $config = $interface->get_interface_config;
    my %pop3_client_params;

    if ($config->{kind} eq 'microsoft') {
        $self->log->trace("POP3 Client - Microsoft OAuth2");
        $self->refresh_access_token($interface);

        $pop3_client_params{auth} = {
            type => 'oauth2',
            user => $config->{ms_username},
            token => $interface->internal_config->{access_token},
        };
        $pop3_client_params{host} = 'outlook.office365.com';
        $pop3_client_params{port} = 995;
        $pop3_client_params{protocol} = 'POP3S';
    } else {
        $self->log->trace("POP3 Client - Basic auth");
        $pop3_client_params{auth} = {
            type => 'basic',
            user => $config->{user},
            password => $config->{password},
        };
        $pop3_client_params{host} = $config->{host};
        $pop3_client_params{port} = $config->{port};
        $pop3_client_params{protocol} = $config->{protocol};
    }

    return Zaaksysteem::Backend::Sysin::POP3Client::Model->new(
        %pop3_client_params
    );
}

=head2 connection_test

Connects to the POP3 server and logs in, using the configured settings.

Returns true on success.

=cut

sub connection_test {
    my ($self, $interface) = @_;

    my $model = $interface->model();

    $model->login();
    $model->close();

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
