package Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate;
use Moose::Role;

use Crypt::OpenSSL::X509;
use BTTW::Tools;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate - Role for interfaces that need to verify a client certificate

=head1 SYNOPSIS

    package Zaaksysteem::Backend::Sysin::Modules::MyAwesomeInterface;
    extends 'Zaaksysteem::Backend::Sysin::Modules';

    with qw/
        Zaaksysteem::Backend::Sysin::Modules::Roles::SOAPGeneric
        Zaaksysteem::Backend::Sysin::Modules::Roles::ClientCertificate
    /;

    # etc.
    # The /sysin/interface/XXX/soap API endpoint uses a configuration field
    # named "(interface_)client_certificate" to find the configured one.

=head1 METHODS

=head2 assert_client_certificate

Verify if the given certificate hash matches the supplied certificate.

=cut

define_profile assert_client_certificate => (
    optional => {
        client_cert        => 'Str',
        stored_cert        => 'Str',
        dn                 => 'Str',
        get_certificate_cb => 'CodeRef',
    },
    require_some =>
        { certificates => [1, qw(stored_cert get_certificate_cb)], },
);

sub assert_client_certificate {
    my $self = shift;
    my $params = assert_profile({@_})->valid;

    if ($ENV{ZS_NO_CLIENT_CERT_CHECK}) {
        $self->log->debug('Client certificate check skipped due to developer settings');
        return 1;
    }

    if (!$params->{client_cert}) {
        my $error = "Verification failed: No certificate sent by client.";
        $self->log->error($error);
        throw(
            'sysin/modules/client_certificate/none_supplied',
            $error,
            { http_code => 403 },
        );
    }

    my $stored_certificate_obj;
    if (defined $params->{stored_cert}) {
        $stored_certificate_obj = Crypt::OpenSSL::X509->new_from_string($params->{stored_cert});
    }
    else {
        $stored_certificate_obj = Crypt::OpenSSL::X509->new_from_file($params->{get_certificate_cb}->()->get_path);
    }

    if (!$stored_certificate_obj) {
        my $error = "Client certificate verification failed: No certificate configured to check against.";
        $self->log->error($error);
        throw(
            'sysin/modules/client_certificate/none_configured',
            $error,
            { http_code => 403 },
        );
    }

    my $given_certificate_obj = Crypt::OpenSSL::X509->new_from_string($params->{client_cert});

    if($given_certificate_obj->fingerprint_sha256() eq $stored_certificate_obj->fingerprint_sha256()) {
        $self->log->debug(
            sprintf(
                "Client certificate verification SUCCESS: SHA256 match: %s",
                $given_certificate_obj->fingerprint_sha256,
            ),
        );
        return 1;
    }

    # We check the certificate SHA256 fingerprints, but lots of programs
    # (Windows..) do not show those, we log the sha1 fingerprints + subject
    # string for easy matching.
    my $error = sprintf(
        "Client certificate verification FAILED.\nConfigured certificate = %s (subject: %s), client = %s (subject: %s)",
        $stored_certificate_obj->fingerprint_sha1,
        $stored_certificate_obj->subject,
        $given_certificate_obj->fingerprint_sha1,
        $given_certificate_obj->subject,
    );
    $self->log->info($error);

    throw(
        'sysin/modules/client_certificate/mismatch',
        $error,
        { http_code => 403 },
    );
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
