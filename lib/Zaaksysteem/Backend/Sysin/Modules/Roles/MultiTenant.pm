package Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant;
use Moose::Role;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::MultiTenant - Abstract role for MultiTenant interfaces

=head1 DESCRIPTION

A role which introduces configuration items for a multi-tenant interface.
You must implement the following functions:

=over

=item build_config_fields

=item log

Or just with L<MooseX::Log::Log4perl>

=back

=head1 SYNOPSIS

    package ZS::Backend::Sysin::Modules::Foo;
    with 'Zaaksysteem::Backend::Syin::Modules::Roles::MultiTenant';

    # You must implement the following function
    sub build_config_fields {
        my $self = shift;
        my @fields = (
            ...,
        );
    }

=cut

requires qw(
    build_config_fields
    log
);

use BTTW::Tools;
use List::Util qw(uniq);
use Zaaksysteem::ZAPI::Form::Field;

=head1 ATTRIBUTES

=head2 schema

The schema attribute.

=cut

has schema => (
    is        => 'rw',
    isa       => 'Zaaksysteem::Schema',
    predicate => 'has_schema',
    accessor  => '_schema',
    writer    => 'set_schema',
);

sub schema {
    my $self = shift;

    return $self->_schema if $self->has_schema;

    throw('multitenant/schema/missing',
        "You did not provide schema for the Multi Tenant role");
}

=head2 interface_config

Interface configuration attribute, requires the builder C<build_config_fields>

=cut

has interface_config => (
    is      => 'rw',
    isa     => 'ArrayRef',
    lazy    => 1,
    builder => 'build_config_fields',
);

=head2 generate_hostname_fields

Generate L<Zaaksysteem::ZAPI::Form::Field>s for multi tenant enabled hosts
Returns undef when the list of hostnames is empty

    $self->generate_hostname_fields(@array);

=cut

sub generate_hostname_fields {
    my ($self, @hostnames) = @_;

    return unless @hostnames;

    my @data = map { { value => $_, label => $_ } } @hostnames;

    return (
        Zaaksysteem::ZAPI::Form::Field->new(
            name    => 'interface_is_multi_tenant',
            type    => 'checkbox',
            label   => 'MultiTenant',
            default => 0,
            description =>
                'Geef aan of deze koppeling van toepassing is op een multitenant omgeving',
        ),
        Zaaksysteem::ZAPI::Form::Field->new(
            name     => 'interface_multi_tenant_host',
            type     => 'select',
            label    => 'Hostname',
            when     => 'interface_is_multi_tenant === true',
            default  => 'Geen',
            required => 1,
            description =>
                'Geef aan op welke hostname deze koppeling van toepassing is',
            data => {
                options => \@data,
            },
        ),
    );
}

=head2 build_config_fields

Adds multi tenant configuration fields

=cut

around 'build_config_fields' => sub {
    my $orig = shift;
    my $self = shift;
    my @args = @_;

    my $fields = $self->$orig(@_);

    if ($self->schema->is_multi_tenant) {
        my $hostnames = $self->schema->customer_hostnames;
        push(@$fields, $self->generate_hostname_fields(@$hostnames));
    }
    return $fields;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
