package Zaaksysteem::Search::Term;
use Moose;

=head1 NAME

Zaaksysteem::Search::Term - Interface-only role for search term classes

=head1 METHODS

This base class requires its subclasses to implement one method:

=over

=item * evaluate

A method that evaluates the term (if applicable) and returns the SQL
representation as:

    ($string_with_placeholders, @placeholder_values)

=back

=cut

sub evaluate {
    ...
}

__PACKAGE__->meta->make_immutable();



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

