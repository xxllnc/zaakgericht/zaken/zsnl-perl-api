package Zaaksysteem::Search::Constants;

our @INDEXED_FIELDS = (
    'case.status',
    'case.assignee.id',
    'case.casetype.id',
    'case.requestor.id',
    'case.route_ou',
    'case.route_role',
);

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
