=head1 NAME

Zaaksysteem::Manual::API::V1::Widget - Widget retrieval and creation

=head1 Description

This API-document describes the usage of our JSON Dashboard=>Widget API. Via the Widget API it is possible
to retrieve, create and edit widgets for a logged in user. Widgets are those panels on your dashboard

=head2 API

This document is based on the V1 API of Zaaksysteem, more information about the default format
of this API can be found in L<Zaaksysteem::Manual::API::V1>. Please make sure you read this
document before continuing.

=head2 URL

The base URL for this API is:

    /api/v1/dashboard/widget

Make sure you use the HTTP Method C<GET> for retrieving, and C<POST> for change requests.

=head2 list

   /api/v1/dashboard/widget

Retrieving multiple objects from our database is as simple as calling the URL C</api/v1/dashboard/widget> without arguments. The
C<result> property will contain an object of type B<set>, which allows us to page the data.

B<Example call>

 https://localhost/api/v1/dashboard/widget

B<Request JSON>

An empty request body suffices

B<Response JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-e1e02c-885968",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "column" : 2,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "9eb3d602-19ac-4d1d-a41b-a0b91b5cb08f",
                  "row" : 1,
                  "size_x" : 300,
                  "size_y" : 150,
                  "widget" : "searchquery"
               },
               "reference" : "9eb3d602-19ac-4d1d-a41b-a0b91b5cb08f",
               "type" : "widget"
            },
            {
               "instance" : {
                  "column" : 2,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "af70490b-6313-48ea-a080-194098aac2b9",
                  "row" : 1,
                  "size_x" : 300,
                  "size_y" : 150,
                  "widget" : "searchquery"
               },
               "reference" : "af70490b-6313-48ea-a080-194098aac2b9",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}


=end javascript

=head1 Mutate data

Mutations via our API will have to be send via the HTTP C<POST> method. The inputdata for these mutations
are one single JSON object containing the parameters. When no input is needed, make sure you send an empty
object (C< {} >)

Every mutation ends with a complete return of all widgets in a C<set> type.

=head2 create

   /api/v1/dashboard/widget/create

It is possible to create a widget in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item widget [required]

B<TYPE>: Str (enum: search_query, etc,etc)

The type of this widget, like favouritesearches, savedsearch, etc

=item column [required]

B<TYPE>: Integer

The column number for this widget, you can think of your dashboard as a table: this is the column number

=item row [required]

B<TYPE>: Integer

The row number for this widget, you can think of your dashboard as a table: this is the row number

=item size_x [required]

B<TYPE>: Integer

The width of this widget, for resizing purposes

=item size_y [required]

B<TYPE>: Integer

The sheight of this widget, for resizing purposes

=item data [required]

B<TYPE>: JSONObject

Every widget requires some parameters, like the SavedSearch uuid for showing results for a user

=back

B<Example call>

 https://localhost/api/v1/dashboard/widget/create

B<Request JSON JSON>

=begin javascript

{
   "column" : 2,
   "data" : {
      "more_data" : {
         "one" : "two"
      }
   },
   "row" : 1,
   "size_x" : 300,
   "size_y" : 150,
   "widget" : "second"
}


=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-e1e02c-b29488",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "column" : 2,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "4f329a11-f727-4104-9a00-3bb52e276781",
                  "row" : 1,
                  "size_x" : 300,
                  "size_y" : 150,
                  "widget" : "second"
               },
               "reference" : "4f329a11-f727-4104-9a00-3bb52e276781",
               "type" : "widget"
            },
            {
               "instance" : {
                  "column" : 2,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "4e7e2717-40d0-40e3-9347-f59da4e593c9",
                  "row" : 1,
                  "size_x" : 300,
                  "size_y" : 150,
                  "widget" : "first"
               },
               "reference" : "4e7e2717-40d0-40e3-9347-f59da4e593c9",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 update

   /api/v1/dashboard/widget/UUID/update

It is possible to update a single widget in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing the following properties

B<Properties>

=over 4

=item widget [required]

B<TYPE>: Str (enum: search_query, etc,etc)

The type of this widget, like favouritesearches, savedsearch, etc

=item column [required]

B<TYPE>: Integer

The column number for this widget, you can think of your dashboard as a table: this is the column number

=item row [required]

B<TYPE>: Integer

The row number for this widget, you can think of your dashboard as a table: this is the row number

=item size_x [required]

B<TYPE>: Integer

The width of this widget, for resizing purposes

=item size_y [required]

B<TYPE>: Integer

The sheight of this widget, for resizing purposes

=item data [required]

B<TYPE>: JSONObject

Every widget requires some parameters, like the SavedSearch uuid for showing results for a user

=back

B<Example call>

 https://localhost/api/v1/dashboard/widget/c25d2daf-0e00-4fa5-8bc7-b61fff072234/update

B<Request JSON JSON>

=begin javascript

{
   "column" : 3,
   "data" : {
      "more_data" : {
         "one" : "two"
      }
   },
   "row" : 44,
   "size_x" : 340,
   "size_y" : 120,
   "widget" : "searchquery2"
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-e1e02c-93338f",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 1,
            "total_rows" : 1
         },
         "rows" : [
            {
               "instance" : {
                  "column" : 3,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "975638c2-d298-4340-9c6b-fd6ab59f9d7d",
                  "row" : 44,
                  "size_x" : 340,
                  "size_y" : 120,
                  "widget" : "searchquery2"
               },
               "reference" : "975638c2-d298-4340-9c6b-fd6ab59f9d7d",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 bulk_update

   /api/v1/dashboard/widget/bulk_update

It is possible to update multiple widgets in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing a property C<updates>, containing
an array of hashes containing the following properties.

B<Properties>

=over 4

=item widget [required]

B<TYPE>: Str (enum: search_query, etc,etc)

The type of this widget, like favouritesearches, savedsearch, etc

=item column [required]

B<TYPE>: Integer

The column number for this widget, you can think of your dashboard as a table: this is the column number

=item row [required]

B<TYPE>: Integer

The row number for this widget, you can think of your dashboard as a table: this is the row number

=item size_x [required]

B<TYPE>: Integer

The width of this widget, for resizing purposes

=item size_y [required]

B<TYPE>: Integer

The sheight of this widget, for resizing purposes

=item data [required]

B<TYPE>: JSONObject

Every widget requires some parameters, like the SavedSearch uuid for showing results for a user

=back

B<Example call>

 https://localhost/api/v1/dashboard/widget/bulk_update

B<Request JSON JSON>

=begin javascript

{
   "updates" : [
      {
         "column" : 3,
         "data" : {
            "more_data" : {
               "one" : "two"
            }
         },
         "row" : 44,
         "size_x" : 340,
         "size_y" : 120,
         "uuid" : "af70490b-6313-48ea-a080-194098aac2b9",
         "widget" : "searchquery2"
      },
      {
         "column" : 4,
         "data" : {
            "more_data" : {
               "one" : "three"
            }
         },
         "row" : 55,
         "size_x" : 350,
         "size_y" : 160,
         "uuid" : "9eb3d602-19ac-4d1d-a41b-a0b91b5cb08f",
         "widget" : "searchquery2"
      }
   ]
}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-e1e02c-78d37e",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 2,
            "total_rows" : 2
         },
         "rows" : [
            {
               "instance" : {
                  "column" : 4,
                  "data" : {
                     "more_data" : {
                        "one" : "three"
                     }
                  },
                  "id" : "9eb3d602-19ac-4d1d-a41b-a0b91b5cb08f",
                  "row" : 55,
                  "size_x" : 350,
                  "size_y" : 160,
                  "widget" : "searchquery2"
               },
               "reference" : "9eb3d602-19ac-4d1d-a41b-a0b91b5cb08f",
               "type" : "widget"
            },
            {
               "instance" : {
                  "column" : 3,
                  "data" : {
                     "more_data" : {
                        "one" : "two"
                     }
                  },
                  "id" : "af70490b-6313-48ea-a080-194098aac2b9",
                  "row" : 44,
                  "size_x" : 340,
                  "size_y" : 120,
                  "widget" : "searchquery2"
               },
               "reference" : "af70490b-6313-48ea-a080-194098aac2b9",
               "type" : "widget"
            }
         ]
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head2 delete

   /api/v1/dashboard/widget/UUID/delete

It is possible to update multiple widgets in zaaksysteem.nl.

The input (request) data for this call is a JSON object containing a property C<updates>, containing
an array of hashes containing the following properties.

B<Properties>

None required

B<Example call>

 https://localhost/api/v1/dashboard/widget/c25d2daf-0e00-4fa5-8bc7-b61fff072234/delete

B<Request JSON JSON>

=begin javascript

{}

=end javascript

B<Response JSON JSON>

=begin javascript

{
   "api_version" : 1,
   "request_id" : "mintlab-e1e02c-8b682f",
   "development" : false,
   "result" : {
      "instance" : {
         "pager" : {
            "next" : null,
            "page" : 1,
            "pages" : 1,
            "prev" : null,
            "rows" : 0,
            "total_rows" : 0
         },
         "rows" : []
      },
      "reference" : null,
      "type" : "set"
   },
   "status_code" : 200
}

=end javascript

=head1 Objects

=head2 Widget

Most of the calls in this document return an instance of type C<widget>. Below we provide more information about
the contents of this object.

B<Properties>

=over 4

=item widget

B<TYPE>: Str (enum: search_query, etc,etc)

The type of this widget, like favouritesearches, savedsearch, etc

=item column

B<TYPE>: Integer

The column number for this widget, you can think of your dashboard as a table: this is the column number

=item row

B<TYPE>: Integer

The row number for this widget, you can think of your dashboard as a table: this is the row number

=item size_x

B<TYPE>: Integer

The width of this widget, for resizing purposes

=item size_y

B<TYPE>: Integer

The sheight of this widget, for resizing purposes

=item data

B<TYPE>: JSONObject

Every widget requires some parameters, like the SavedSearch uuid for showing results for a user

=back

=head1 Support

The data in this document is supported by the following test. Please make sure you use the API as described
in this test. Any use of this API outside the scope of this test is B<unsupported>

L<TestFor::Catalyst::API::V1::Dashboard>

=head1 PROJECT FOUNDER

Mintlab B.V. <info@mintlab.nl>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
