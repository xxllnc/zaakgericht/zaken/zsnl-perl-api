package Zaaksysteem::Model::BR::Subject;
use Zaaksysteem::Moose;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::BR::Subject - Catalyst glue for
L<Zaaksysteem::BR::Subject> instances.

=head1 DESCRIPTION

    my $brige = $c->model('BR::Subject');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::BR::Subject',
    constructor => 'new',
);

use Zaaksysteem::Constants::Users qw(REGULAR);

=head2 prepare_arguments

This method implements the interface required by
L<Catalyst::Model::Factory::PerRequest>.

=cut

sub prepare_arguments {
    my $self = shift;
    my $c = shift;
    my @args = @_;

    my %args = (
        schema => $c->model('DB')->schema,
        @args ? %{$args[0]} : (),
    );

    if (($args{remote_search} //'') eq 'stufconfig' && !$args{config_interface_id}) {
        my $rs;
        if ($c->user_exists && $c->check_user_mask(REGULAR)) {
            $rs = $c->model("DB::Interface")->search_active({module => 'stufconfig'});
        }
        else {
            $rs = $c->model("DB::Interface")->search_active_restricted({module => 'stufconfig'});
        }
        my @interfaces = $rs->all;
        if (@interfaces != 1) {
            throw("model/br/subject/stuf/config_interface_id",
                "Unable to determine stufconfig interface for request");
        }

        my $config = $interfaces[0]->get_interface_config;
        if ($config->{gbav_search} || $config->{local_search}) {
            $args{config_interface_id} = $interfaces[0]->id;
        }
        else {
            throw("model/br/subject/stuf/remote_search",
                "No remote search defined in config interface");
        }

    }

    return \%args;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
