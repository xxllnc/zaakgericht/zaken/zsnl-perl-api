package Zaaksysteem::Zaken::RelatedObjects;
use Zaaksysteem::Moose;

with qw(
    Zaaksysteem::Moose::Role::Schema
);
use List::Util qw(uniq);

=head1 NAME

Zaaksysteem::Zaken::RelatedObjects - A model for planned cases

=head1 DESCRIPTION

A model that deals with planned cases

=head1 SYNOPSIS

    package Zaaksysteem::Example;
    use Zaaksysteem::Zaken::RelatedObjects;

    my $model = Zaaksysteem::Zaken::RelatedObjects->new(
        schema => $schema,
        case   => $case,
    );

    my $objects = $model->get_related_objects;
    #
    # [
    #     {
    #         object_class_name => $class_name,
    #         object_class_uuid => $class_uuid,
    #         uuid              => $uuid,
    #         name              => $name,
    #     }
    # ]
    #

=head1 ATTRIBUTES

=head2 case

A Zaaksysteem case

=cut

has case => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Zaken::ComponentZaak',
    required => 1,
);

has _relation_rs => (
    is       => 'ro',
    isa      => 'DBIx::Class::ResultSet',
    lazy     => 1,
    builder  => '_build_relation_rs',
    init_arg => undef,
);

sub _build_relation_rs {
    my $self = shift;

    return $self->build_resultset(
        'ObjectRelationships',
        {
            type1        => 'related',
            type2        => 'related',
        }
    );
}

has _related_objects => (
    is       => 'ro',
    isa      => 'ArrayRef',
    lazy     => 1,
    builder  => '_build_related_objects',
    init_arg => undef,
);

sub _build_related_objects {
    my $self = shift;

    my $uuid = $self->case->get_column('uuid');
    my $object = $self->_relation_rs->search_rs(
        {
            -and => {
                -or => [
                    {
                        object2_type => 'case',
                        -and => {
                            -or => [
                                { object2_uuid      => $uuid },
                                { owner_object_uuid => $uuid },
                            ],
                        },
                    },
                    # This is when someone creates a link via the "create link"
                    # option in the relationship tab. The way it is linked is a
                    # tad bit different
                    {
                        object1_type => 'case',
                        -and => {
                            -or => [
                                { object1_uuid      => $uuid },
                                { owner_object_uuid => $uuid },
                            ],
                        },
                    },
                ],
            }
        }
    );

    my @objects = $object->all;
    return \@objects;

}

has _object_objectdata_rs => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Backend::Object::Data::ResultSet',
    lazy     => 1,
    builder  => '_build_object_objectdata_rs',
    init_arg => undef,
);

sub _build_object_objectdata_rs {
    my $self = shift;

    return $self->build_resultset(
        'ObjectData',
        {
            object_class => {
                -not_in => [qw(
                    casetype
                    case
                    saved_search
                    host
                    instance
                    controlpanel
                    widget
                )]
            }
        }
    );
}

=head2 get_related_objects

Get the related objects for the case

    {
        object_class_name => $class_name,
        object_class_uuid => $class_uuid,
        uuid => $uuid,
        name => $name,
    }

=cut

sub _get_type_from_object_relationship {
    my $object = shift;
    return $object->object1_type eq 'case'
        ? $object->object2_type
        : $object->object1_type;
}

sub _get_uuid_from_object_relationship {
    my $object = shift;
    return $object->object2_type eq 'case'
        ? $object->get_column('object1_uuid')
        : $object->get_column('object2_uuid');
}

sub _get_title_from_object_relationship {
    my $object = shift;
    return $object->object2_type eq 'case'
        ? $object->get_column('title1')
        : $object->get_column('title2');
}

sub get_related_objects {
    my $self = shift;

    my $objects = $self->_related_objects;

    my @uuids = map {
        _get_uuid_from_object_relationship($_);
    } @{$objects};

    my $od = $self->_object_objectdata_rs->search_rs(
        {
            uuid         => { -in  => \@uuids },
            object_class => { '!=' => 'type' },
        },
        { columns => [qw(uuid class_uuid)] }
    );

    my %object_to_type;
    my @class_uuids;
    while (my $o = $od->next) {
        $object_to_type{$o->uuid} = $o->get_column('class_uuid');
        push(@class_uuids, $o->get_column('class_uuid'));
    }

    @class_uuids = uniq @class_uuids;
    my $types = $self->_object_objectdata_rs->search_rs(
        {
            uuid         => { -in => \@class_uuids },
            object_class => 'type',
        },
        { columns => [qw(uuid properties)] }
    );

    my %type_mapping;
    while (my $type = $types->next) {
        $type_mapping{$type->uuid} = $type->properties->{values}{name}{value};
    }

    my @object_to_show;
    foreach my $object (@{$objects}) {

        my $uuid = _get_uuid_from_object_relationship($object);
        my $class_uuid = $object_to_type{$uuid};

        my $name       = _get_title_from_object_relationship($object);
        my $class_name = $type_mapping{$class_uuid};

        my $json = {
            object_class_name => $class_name,
            object_class_uuid => $class_uuid,
            uuid => $uuid,
            name => $name,
            type => _get_type_from_object_relationship($object),
            blocks_deletion => $object->blocks_deletion,
        };
        push(@object_to_show, $json);
    }

    return \@object_to_show;
}

sub all_relations {
    my $self = shift;
    my $objects = $self->get_related_objects();

    my $uuid = $self->case->get_column('uuid');
    foreach (@$objects) {
        my $object = Zaaksysteem::Object::Relation->new(
            related_object_id => $uuid,
            related_object_type => $_->{type},
            blocks_deletion => 1,
            relationship_name_a => 'related',
            relationship_name_b => 'related',
            related_object => $_->{uuid},
        );
        $_ = $object;
    }
    return $objects;
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
