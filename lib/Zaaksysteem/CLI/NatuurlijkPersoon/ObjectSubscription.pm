package Zaaksysteem::CLI::NatuurlijkPersoon::ObjectSubscription;
use Moose;

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::OjectSubscription - Add object subscriptions to authentic persons

=head1 DESCRIPTION

CLI script to add object subscriptions to natuurlijk personen for migration purposes

=cut

extends 'Zaaksysteem::CLI';

with 'Zaaksysteem::CLI::Roles::Auditlog';

use BTTW::Tools;
use DateTime;
use JSON::XS;

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            $self->log->info("Updating natuurlijk personenen");
            #$self->delete_duplicate_subscription_ids;
            $self->create_object_subscriptions_for_natuurlijk_personen;
        }
    );


    return 1;

};

sub _get_stuf_interface {
    my $self = shift;
    my $interface;
    my $rs = $self->schema->resultset('Interface');
    foreach (qw(stufnps stufprs)) {
        $interface = $rs->search_active({ module => $_ })->first;
        return $interface if $interface;
    }
    throw("cli/np/interface/stuf/missing",
        "Unable to find stuf interface on this instance");
}

=head2 delete_duplicate_subscription_ids

Deletes duplicate subscriptions

=cut

sub delete_duplicate_subscription_ids {
    my ($self) = @_;

    my $rs = $self->get_duplicate_object_subscriptions;
    while (my $os = $rs->next) {
        if ($os->date_deleted) {
            $self->log->info(
                sprintf(
                    "Deleting duplicate object subscription %s",
                    $os->id
                )
            );
            $os->delete;
        }
    }
}

=head2 create_object_subscriptions_for_natuurlijk_personen

Deduplicates authentic natural persons found in the database.

=cut

sub create_object_subscriptions_for_natuurlijk_personen {
    my $self = shift;

    my $os_rs = $self->schema->resultset('ObjectSubscription');
    my $os    = $os_rs->search_rs(
        {
            local_table  => 'NatuurlijkPersoon',
            date_deleted => undef,
        }
    )->get_column('local_id::integer')->as_query;

    my $rs = $self->schema->resultset('NatuurlijkPersoon')->search_rs(
        {
            authenticated => 1,
            active        => 1,
            deleted_on    => undef,
            'me.id'       => { 'not in' => $os },
        },
    );

    my $interface_id = $self->_get_stuf_interface->id;

    my $config_interface_id = $self->schema->resultset('Interface')
        ->search_active({ module => 'stufconfig' })->first->id;

    my $mapping;
    while (my $np = $rs->next) {
        my $bsn = int($np->burgerservicenummer);
        if (!$bsn) {
            $self->log->debug("Not updating natuurlijk personen with BSN 0");
            next;
        }
        if (my $os = $np->subscription_id) {
            if ($os->date_deleted) {
                $self->log->info(
                    sprintf(
                        "Updating object subscription %s for BSN %09d, removing date_deleted",
                        $os->id, $np->burgerservicenummer
                    )
                );
                $os->update({ date_deleted => undef });
            }
            else {
                $self->log->trace(
                    "Skipping object subscription creation, already present");
            }
        }
        else {
            my $os = $os_rs->create(
                {
                    interface_id        => $interface_id,
                    local_table         => 'NatuurlijkPersoon',
                    local_id            => $np->id,
                    external_id         => 'IN_PROGRESS',
                    config_interface_id => $config_interface_id,
                    date_created        => DateTime->now()->subtract({days => 1})
                }
            );

            $self->log->info(
                sprintf(
                    "Creating object subscription %s for BSN %09d, did not exist before",
                    $os->id, $np->burgerservicenummer
                )
            );
        }
    }

}

=head2 get_duplicate_object_subscriptions

Get duplicate object subscriptions for one local_id

=cut

sub get_duplicate_object_subscriptions {
    my $self = shift;

    my $sql = "select local_id from object_subscription where local_table='NatuurlijkPersoon' group by local_id having count(local_id) > 1";
    return $self->schema->resultset('ObjectSubscription')->search_rs(
        { local_id => { in   => \"($sql)" } },
        { order_by    => { -asc => 'me.local_id' } }
    );
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
