package Zaaksysteem::CLI::NatuurlijkPersoon::Deduplicate;
use Moose;

=head1 NAME

Zaaksysteem::CLI::NatuurlijkPersoon::Deduplicate - Deduplicate Natuurlijk Personen.

=cut

extends 'Zaaksysteem::CLI';

with 'Zaaksysteem::CLI::Roles::Auditlog';

use BTTW::Tools;
use DateTime;
use JSON::XS;
use Zaaksysteem::Types qw(BSN);

around run => sub {
    my $orig = shift;
    my $self = shift;

    $self->$orig(@_);

    # Deduplicate object subscriptions
    # This will make sure all duplicate natuurlijk personen with the same
    # subscription will be removed. Later on we will deduplicate natuurlijk
    # personen and unsubscribe ourselves from the broker. If duplicate
    # objectsubscriptions are present we cancel our "live" data.

    $self->do_transaction(
        sub {
            my ($self, $schema) = @_;

            $self->log->info("Updating object subscriptions");
            $self->deduplicate_object_subscriptions;

            $self->log->info("Updating natuurlijk personenen");
            $self->deduplicate_natuurlijk_personen;

            $self->list_duplicate_personen;
        }
    );


    return 1;

};

=head2 list_duplicate_personen

=cut

sub list_duplicate_personen {
    my $self = shift;

    my $rs_np = $self->get_duplicate_natuurlijk_personen;
    while (my $np = $rs_np->next) {
        $self->log->info(
            sprintf(
                "[%d] BSN '%s' [authentic: %s] is still duplicated in the database",
                $np->id, $np->burgerservicenummer, $np->authenticated
            )
        );
    }

}

=head2 deduplicate_natuurlijk_personen

Deduplicates authentic natural persons found in the database.

=cut

sub _deduplicate_natuurlijk_persoon {
    my ($self, $bsn, @list) = @_;

    my @progress = map {
        $self->schema->resultset('ObjectSubscription')->search_rs(
            {
                local_id    => $_->id,
                local_table => 'NatuurlijkPersoon',
                external_id => 'IN_PROGRESS',
            }
            )->all
    } @list;

    my @active = map {
        $self->schema->resultset('ObjectSubscription')->search_rs(
            {
                local_id    => $_->id,
                local_table => 'NatuurlijkPersoon',
                external_id => { '!=' => 'IN_PROGRESS' }
            }
            )->all
    } @list;

    if (!@active && !@progress) {
        my ($keep) = grep { $_->get_column('adres_id') && $_->authenticated } @list;
        if (!defined $keep) {
            # Keep only the one with cases, delete the other
            my @cases;
            foreach (@list) {
                my $involved = $_->involved_with_cases;
                if ($involved) {
                    push(@cases, $_);
                }
                else {
                    $self->log->info("Non authentic NP with BSN '$bsn' disabled");
                    $_->disable_natuurlijk_persoon;
                }
            }
            # with more than one case: consider failure
            if (@cases > 1) {
                $self->log->error(
                    "No authenticated NP with $bsn (or address is missing)");
                return;
            }
            return 1;
        }
        foreach my $rm (@list) {
            next if $rm->id eq $keep->id;
            $self->_merge_natuurlijk_persoon(
                to   => $keep,
                from => $rm,
            );
        }
        return 1;
    }
    elsif (!@active && @progress == 1) {
        my $keep = pop @progress;
        my $np   = $self->schema->resultset('NatuurlijkPersoon')
            ->find($keep->local_id);

        foreach my $rm (@list) {
            next if $rm->id eq $keep->id;
            $self->_merge_natuurlijk_persoon(
                to   => $np,
                from => $rm,
            );
        }
        return 1;

    }

    my $keep = pop @active;
    if (!$keep) {
        $self->log->error("No active burgers found with BSN $bsn");
        return;
    }
    my $np = $self->schema->resultset('NatuurlijkPersoon')
        ->find($keep->local_id);

    foreach my $rm (@active, @progress) {
        $self->close_os_and_update_np(
            object_subscription => $rm,
            natuurlijk_persoon  => $np,
            keep_os             => $keep,
        );
    }

    foreach my $rm (@list) {

        next if $rm->id eq $np->id;
        $self->_merge_natuurlijk_persoon(
            to   => $np,
            from => $rm,
        );
    }
    return 1;
}

sub deduplicate_natuurlijk_personen {
    my $self = shift;

    my $rs_np = $self->get_duplicate_natuurlijk_personen;
    my $mapping;
    while (my $np = $rs_np->next) {
        my $bsn = $np->bsn;
        if (!BSN->check($bsn)) {
            $np->update({ burgerservicenummer => undef });
            $self->log->info("Invalid BSN '$bsn', removing BSN from person");
            next;
        }
        push(@{ $mapping->{int($bsn)} }, $np);
    }

    foreach my $bsn (sort keys %{$mapping}) {
        $self->log->info("Going to merge BSN $bsn");
        my $ok = $self->_deduplicate_natuurlijk_persoon($bsn,
            @{ $mapping->{$bsn} });

        if ($ok) {
            $self->log->info(
                "Merged duplicate natuurlijk persoon with BSN $bsn");
        }
        else {
            $self->log->error(
                "Merge of duplicate natuurlijk persoon with BSN $bsn failed");
        }
    }
}

=head2 deduplicate_object_subscriptions

Deduplicates object subscriptions and the related natural person(s).

=cut

sub deduplicate_object_subscriptions {
    my $self = shift;

    my $rs = $self->get_duplicate_object_subscriptions;
    my $mapping;
    while (my $o = $rs->next) {
        my $external_id = int($o->external_id);
        push(@{ $mapping->{$external_id} }, $o);
    }

    foreach my $id (keys %{$mapping}) {
        $self->log->info(
            "Processing duplicate object subscription with external id $id");

        my @list = @{ $mapping->{$id} };

        my $keep = shift @list;
        my $np   = $self->schema->resultset('NatuurlijkPersoon')
            ->find($keep->local_id);

        if (!$np) {
            $self->log->error(
                sprintf
                    'Object subscription with external ID %s links to non-existing Natuurlijk Persoon %d',
                $keep->external_id, $keep->local_id,
            );
            return;
        }

        foreach my $os (@list) {
            $self->close_os_and_update_np(
                object_subscription => $os,
                natuurlijk_persoon  => $np,
                keep_os             => $keep,
            );
        }
    }
}

=head2 close_os_and_update_np

Close an object subscription and update the natural person.

=head3 ARGUMENTS

=over

=item * keep_os

Object subscription which you want to keep (used for logging purposes), required.

=item * object_subscription

Object subscription which you want to delete/close, required.

=item * natuurlijk_persoon

The natuurlijk persoon you want to keep, required.

=back

=cut

sub close_os_and_update_np {
    my $self    = shift;
    my %options = @_;

    my $os = $options{object_subscription};
    my $np
        = $self->schema->resultset('NatuurlijkPersoon')->find($os->local_id);

    if (!$np) {
        $self->log->error(
            sprintf
                'Object subscription with external ID %s links to non-existing Natuurlijk Persoon %d: to delete',
            $os->external_id, $os->local_id,
        );
        return;
    }

    $self->log->info(
        sprintf "Closing object subscription for BSN %d with external ID %s",
        $np->bsn, $os->external_id,
    );

    try {
        $self->_merge_natuurlijk_persoon(
            to   => $options{natuurlijk_persoon},
            from => $np
        );
        $self->_log_changes(
            'Object subscription',
            $os->get_column('id'),
            $os->get_column('external_id'),
            $options{keep_os}->get_column('id'),
            $options{keep_os}->get_column('external_id')
        );
        $os->delete;
    }
    catch {
        $self->log->error(
            sprintf
                'Unable to close object subscription [%s] for BSN %s to BSN %s due to error: %s',
            $os->external_id, $np->bsn,
            $options{natuurlijk_persoon}->bsn, $_,);
    };
}

=head2 _assert_same_person

Asserts if the natural person is the same (based on the BSN).

=cut

sub _assert_same_person {
    my ($self, %options) = @_;

    if ($options{from}->bsn ne $options{to}->bsn) {
        $self->log->debug(
            sprintf 'Natuurlijk Persoon mismatch, expected BSN %s got BSN %s',
            $options{to}->bsn, $options{from}->bsn,
        );
        throw('natuurlijk_persoon/bsn/mismatch', "Not the same person");
    }
    return 1

}

=head2 _merge_natuurlijk_persoon

Merges a natural person. It moves several components from natural person A to B.

=head3 ARGUMENTS

=over

=item * from

The natural person you are going to "delete"

=item * to

The natural person you are going to keep and get all the cases, logging, and such.

=back

=cut

sub _merge_natuurlijk_persoon {
    my ($self, %options) = @_;

    $self->_assert_same_person(%options);

    if (!$options{from}->involved_with_cases) {
        $options{from}->disable_natuurlijk_persoon;
        return;
    }

    my $now = DateTime->now();

    my @objects_to_move = qw(logging contactmoment concept_case cases);
    foreach (@objects_to_move) {
        my $method = "_move_${_}_to_other_np";
        if ($self->can($method)) {
            $self->$method(%options);
        }
        else {
            die "Unable to execute $method";
        }
    }

    $options{from}->update({ deleted_on => $now });
}

=head2 _bid

Helper method to get the betrokkene id.

=cut

sub _bid {
    my ($self, $np) = @_;
    return $np->bid;
    return sprintf("betrokkene-natuurlijk_persoon-%d", $np->id);
}

=head2 _move_contactmoment_to_other_np

Move contactmomenten from one natural person to another.

=cut

sub _move_contactmoment_to_other_np {
    my ($self, %options) = @_;

    my $to   = $options{to};
    my $from = $options{from};

    my $old = $self->_bid($from);

    my $rs
        = $self->schema->resultset('Contactmoment')
        ->search_rs({ '-or' => [subject_id => $old, created_by => $old] },);

    if ($self->log->is_trace) {
        $self->log->trace(sprintf "Found %d contactmomenten", $rs->count);
    }

    my $new = $self->_bid($to);
    while (my $item = $rs->next) {
        my $subject_id = $item->subject_id;
        my $created_by = $item->created_by;

        my %update = (
            $created_by eq $old ? (created_by => $new) : (),
            ($subject_id // '') eq $old ? (subject_id => $new) : (),
        );

        $self->_log_changes('Contactmoment', $item->get_column('id'),
            $old, $new);
        $item->update(\%update);
    }
}

sub _move_concept_case_to_other_np {
    my ($self, %options) = @_;

    my $to   = $options{to};
    my $from = $options{from};

    my $old = $self->_bid($from);

    my $rs = $self->schema->resultset('ZaakOnafgerond')
        ->search_rs({ betrokkene => $old });

    if ($self->log->is_trace) {
        $self->log->trace(sprintf "Found %d concept cases", $rs->count);
    }

    my $new = $self->_bid($to);
    while (my $item = $rs->next) {
        my %update = (betrokkene => $new);

        my $dupe = $self->schema->resultset('ZaakOnafgerond')->
            search_rs(\%update)->first;

        if (!$dupe) {
            $self->_log_changes('ZaakOnafgerond', $item->get_column('create_unixtime'),
                $old, $new);
            $item->update(\%update);
        }
        else {
            # This requires user interaction to solve, we don't move
            # concept cases. They are removed after 30 days, so no biggy
            $self->log->info("Skipping concept case merging, one casetype per subject is allowed");
        }
    }
}

=head2 _log_changes

Logs changes to log4perl and an audit log

=cut

sub _log_changes {
    my ($self, $component, @items) = @_;

    $self->audit_log($component, @items);

    my $f = lc($component);

    if ($f eq 'case') {
        $f = 'requestor';
    }

    my $msg = "$component %d: Updating $f for %s to %s";
    if ($f eq 'object subscription') {
        $msg
            = "Object subscription %d: Closing external id %s, keeping external id [%d] %s";
    }

    $self->log->info(sprintf($msg, @items));
}

=head2 _move_logging_to_other_np

Move Logging events from one natural person to another.

=cut

sub _move_logging_to_other_np {
    my ($self, %options) = @_;

    my $to   = $options{to};
    my $from = $options{from};

    my $old = $self->_bid($from);

    my $rs = $self->schema->resultset('Logging')->search_rs(
        {
            '-or' => [
                betrokkene_id => $old,
                created_by    => $old,
                modified_by   => $old,
                deleted_by    => $old,
                created_for   => $old,
            ]
        },
    );

    if ($self->log->is_trace) {
        $self->log->trace(sprintf "Found %d logging entries", $rs->count);
    }

    my $new = $self->_bid($to);
    while (my $item = $rs->next) {
        my $subject_id = $item->betrokkene_id;
        my $created_by = $item->created_by;

        my %update = map { $_ => $new }
            grep { defined $item->$_ && $item->$_ eq $old }
            qw(
            betrokkene_id
            created_by
            modified_by
            deleted_by
            created_for
        );

        my $data = $item->data;
        if (($data->{subject_id} // '') eq $old) {
            $data->{subject_id} = $new;
            $update{event_data} = JSON::XS->new->encode($data);
        }
        if ($self->log->is_trace) {
            $self->log->trace(dump_terse(\%update));
        }

        $self->_log_changes('Logging', $item->get_column('id'), $old, $new);
        $item->update(\%update);
    }
}

=head2 move_cases_to_other_np

Move cases from one natural person to another. Also updates the zaakbetrokkene table.

=cut

sub _move_cases_to_other_np {
    my ($self, %options) = @_;

    my $to   = $options{to};
    my $from = $options{from};

    my $betrokkenen_rs
        = $self->schema->resultset('ZaakBetrokkenen')->search_rs(
        {
            'me.betrokkene_type'      => 'natuurlijk_persoon',
            'me.gegevens_magazijn_id' => $from->get_column('id'),
        }
        );

    my $cases = $self->schema->resultset('Zaak')->search_extended(
        {
            aanvrager =>
                { -in => $betrokkenen_rs->get_column('id')->as_query }
        }
    );

    if ($self->log->is_trace) {
        $self->log->trace(sprintf "Found %d cases", $cases->count);
    }

    my $new = $self->_bid($to);
    my $old = $self->_bid($from);
    my $id  = $to->id;

    while (my $case = $cases->next) {
        $self->_log_changes('Case', $case->get_column('id'), $old, $new);
        $case->set_aanvrager($new);
        $case->discard_changes();
        $case->touch;
    }

    $betrokkenen_rs = $self->schema->resultset('ZaakBetrokkenen')->search_rs(
        {
            'me.betrokkene_type'      => 'natuurlijk_persoon',
            'me.gegevens_magazijn_id' => $from->get_column('id'),
        }
    );

    while (my $zb = $betrokkenen_rs->next) {
        $self->_log_changes('Zaakbetrokkene', $zb->get_column('id'),
            $old, $new);
        $zb->update({ gegevens_magazijn_id => $id });
    }
}

=head2 get_duplicate_natuurlijk_personen

Get duplicate natural persons from the database.

=cut

sub get_duplicate_natuurlijk_personen {
    my $self = shift;

    my $sql
        = "select burgerservicenummer::int from natuurlijk_persoon where deleted_on is NULL and NULLIF(burgerservicenummer, '')::int != 0 group by burgerservicenummer::int having count(burgerservicenummer::int) > 1";
    return $self->schema->resultset('NatuurlijkPersoon')->search(
        { 'NULLIF(burgerservicenummer, \'\')::int' => { in   => \"($sql)" } },
        { order_by            => { -asc => 'me.burgerservicenummer' }, }
    );
}

=head2 get_duplicate_object_subscriptions

Get duplicate object_subscriptions from the database.

=cut

sub get_duplicate_object_subscriptions {
    my $self = shift;

    my $sql
        = "select external_id from object_subscription where local_table='NatuurlijkPersoon' and external_id != 'IN_PROGRESS' and date_deleted is NULL group by external_id having count(external_id) > 1";
    return $self->schema->resultset('ObjectSubscription')->search(
        { external_id => { in   => \"($sql)" } },
        { order_by    => { -asc => 'me.local_id' } }
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
