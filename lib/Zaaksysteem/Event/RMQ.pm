package Zaaksysteem::Event::RMQ;
use Zaaksysteem::Moose;

use DateTime;
use DateTime::Format::Pg;
use Time::HiRes;
use UUID4::Tiny qw(create_uuid_string);

with 'Zaaksysteem::Moose::Role::Schema';

has '+schema' => (
    required => 0,
    predicate => 'has_schema',
);


=head1 NAME

Zaaksysteem::Event::RMQ - Create RabbitMQ events

=head1 DESCRIPTION

Create event (datastructures) that can be broadcasted to RabbitMQ

=head1 SYNOPSIS

    use Zaaksysteem::Event::RMQ;

    my $builder = Zaaksysteem::Event::RMQ->new();

    $builder->create_incoming_email_event();
    $builder->create_case_custom_fields_updated_event();
    $builder->create_updated_company_event();
    $builder->create_updated_person_event();

=cut

sub enqueue_publish_event {
    my ($self, @events) = @_;

    if ($self->has_schema) {
        push(
            @{
                $self->schema->default_resultset_attributes
                    ->{events_to_publish}
            },
            @events
        );
        return 1;
    }
    $self->log->trace("Not publishing event(s), no schema object found");
    return;
}

sub _build_user_info {
    my $current_user = shift;
    if (not defined $current_user) {
        return (
            user_uuid => undef,
            user_info => undef,
        );
    }

    my %user_permissions = %{ $current_user->legacy_permissions };

    my %permissions;
    for my $permission (keys %permissions) {
        $permissions{$permission} = $user_permissions{$permission} ? \1 : \0;
    }

    return (
        user_uuid => $current_user->uuid,
        user_info => {
            type => "UserInfo",
            user_uuid => $current_user->uuid,
            permissions => \%permissions,
        }
    );
}

sub build_event {
    my ($self, %params) = @_;

    my $routing_key = join(
        ".",
        qw(zsnl v2),
        $params{domain},
        $params{entity_type},
        $params{event_name},
    );

    my $parameters = {
        id             => create_uuid_string(),
        # DateTime::Format::Pg is actually ISO8601, and includes milliseconds
        # We need sub-second resolution so event recipients know what was done
        # first.
        created_date   => DateTime::Format::Pg->format_datetime(
            DateTime->from_epoch(epoch => Time::HiRes::time)->set_time_zone('UTC')
        ),
        correlation_id => create_uuid_string(),
        entity_data    => {},
        context        => $self->schema->customer_config->{instance_hostname},
        %params,
    };

    if (not exists $parameters->{user_uuid}) {
        $parameters = {
            %$parameters,
            _build_user_info($self->schema->current_user)
        }
    }

    return { routing_key => $routing_key, parameters => $parameters };
}

sub create_object_action_phase_action_event {
    my $self = shift;
    my (
        $case_uuid,
        $attribute_uuid,
        $attribute_magic_string,
        $change_type,
        $attribute_changes,
        $system_attribute_changes,
    ) = @_;

    return $self->build_event(
        domain => 'legacy',
        entity_type => 'Case',
        event_name => 'ObjectActionTriggered',
        entity_id => $case_uuid,
        changes => [
            {
                "key" => "action_type",
                "old_value" => undef,
                "new_value" => $change_type,
            },
            {
                "key" => "attribute_uuid",
                "old_value" => undef,
                "new_value" => $attribute_uuid,
            },
            {
                "key" => "attribute_magic_string",
                "old_value" => undef,
                "new_value" => $attribute_magic_string,
            },
            {
                "key" => "system_attribute_changes",
                "old_value" => undef,
                "new_value" => $system_attribute_changes,
            },
            {
                "key" => "attribute_changes",
                "old_value" => undef,
                "new_value" => $attribute_changes,
            },
        ]
    );
}

sub create_incoming_email_event {
    my ($self, $filestore_uuid) = @_;

    return $self->build_event(
        domain      => 'communication',
        entity_type => 'IncomingEmail',
        event_name  => 'EmailReceived',
        changes     => [
            {
                "key"       => "file",
                "old_value" => undef,
                "new_value" => "$filestore_uuid"
            }
        ],
        entity_id => create_uuid_string(),
    );
}

sub create_case_custom_fields_updated_event {
    my ($self, $case_uuid, $field_values) = @_;

    return $self->build_event(
        domain      => 'legacy',
        entity_type => 'Case',
        event_name  => 'CaseCustomFieldsUpdated',
        entity_id   => $case_uuid,
        changes     => [
            {
                "key"       => "custom_fields",
                "old_value" => undef,
                "new_value" => $field_values
            }
        ],
    );
}

sub create_case_destroyed_event {
    my ($self, $case_uuid, $date_destroyed) = @_;

    return $self->build_event(
        domain      => 'legacy',
        entity_type => 'Case',
        event_name  => 'CaseDestroyed',
        entity_id   => $case_uuid,
        changes     => [
            {
                "key"       => "date_destroyed",
                "old_value" => undef,
                "new_value" => DateTime::Format::Pg->format_datetime($date_destroyed),
            }
        ],
    );
}

sub create_updated_company_event {
    my ($self, $uuid, @changes) = @_;

    return $self->build_event(
        domain      => 'legacy',
        entity_type => 'Organization',
        event_name  => 'Updated',
        entity_id   => $uuid,
        changes     => \@changes,
    );
}

sub create_updated_person_event {
    my ($self, $uuid, @changes) = @_;

    return $self->build_event(
        domain      => 'legacy',
        entity_type => 'Person',
        event_name  => 'Updated',
        entity_id   => $uuid,
        changes     => \@changes,
    );
}

sub create_case_event {
    my ($self, $event_name, $uuid, @changes) = @_;

    return $self->build_event(
        domain      => 'legacy',
        entity_type => 'Case',
        event_name  => $event_name,
        entity_id   => $uuid,
        changes     => \@changes,
    );
}


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
