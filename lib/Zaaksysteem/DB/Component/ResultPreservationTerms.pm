package Zaaksysteem::DB::Component::ResultPreservationTerms;
use Zaaksysteem::Moose;
extends 'Zaaksysteem::Backend::Component';

=head1 NAME

Zaaksysteem::DB::Component::ResultPreservationTerms - Database component for "ResultPreservationTerms" rows

=head1 METHODS

=head2 calculate_destruction_date

=cut

sub calculate_destruction_date {
    my ($self, $date) = @_;
    my $dt = $date->clone;
    my $unit = $self->unit . 's';
    $dt->add($unit => $self->unit_amount);
    return $dt;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
