package Zaaksysteem::DB::Component::CustomObjectRelationship;
use base qw/DBIx::Class::Row/;
use Zaaksysteem::Moose;

sub insert { shift->next::method(@_) }
sub update { shift->next::method(@_) }

sub as_object {
    my $self = shift;

    my %type = (
        related_person_id => 'person',
        related_organization_id => 'company',
        related_employee_id => 'employee',
    );
    my ($object_type, $id);
    foreach (keys %type) {
        if ($id = $self->get_column($_)) {
            $object_type = $type{$_};
            last;
        }
    }

    return {
        type     => 'contact/link',
        instance => {
            contact => {
                reference => $self->related_uuid,
                type      => $object_type,
            },
            relation => {
                reference => $self->custom_object_id->uuid,
                type      => 'custom_object'
            },
        },
    };
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
