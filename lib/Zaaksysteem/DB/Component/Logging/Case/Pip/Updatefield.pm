package Zaaksysteem::DB::Component::Logging::Case::Pip::Updatefield;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    my $new_values = $self->data->{ new_values };

    my $subject_text = 'Aanvrager';

    if (exists $self->data->{ subject_name }) {
        $subject_text = sprintf(
            'Betrokkene "%s"',
            $self->data->{ subject_name }
        )
    }

    my $value_text;

    if (ref $new_values eq 'ARRAY') {
        $value_text = sprintf(
            'met de volgende waarden: %s',
            join(', ', map { sprintf('"%s"', $_) } @{ $new_values })
        );
    } else {
        $value_text = sprintf('met de volgende waarde: "%s"', $new_values);
    }

    my $reason = $self->data->{ toelichting } || $self->data->{ reason };
    my $reason_text = '';

    if ($reason) {
        $reason_text = sprintf(
            ' Opgegeven toelichting: "%s".',
            $reason
        );
    }

    return sprintf(
        '%s heeft een wijziging voorgesteld voor kenmerk "%s", %s.%s',
        $subject_text,
        $self->data->{ kenmerk },
        $value_text,
        $reason_text
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 onderwerp

TODO: Fix the POD

=cut

