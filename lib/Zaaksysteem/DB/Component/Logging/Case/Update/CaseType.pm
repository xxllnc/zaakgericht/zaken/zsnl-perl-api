package Zaaksysteem::DB::Component::Logging::Case::Update::CaseType;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


sub onderwerp {
    my $self = shift;

    if ($self->data->{old_case_type}) {
        sprintf(
            'Zaaktype "%s" van zaak %d is aangepast naar "%s"',
            $self->_zaaktype_id_to_name($self->data->{old_case_type}),
            $self->data->{case_id},
            $self->_zaaktype_id_to_name($self->data->{casetype_id}),
        );
    }
}

sub _zaaktype_id_to_name {
    my $self = shift;
    my $id = shift;

    my $rs = $self->result_source->schema->resultset("Zaaktype");
    my $ct = $rs->find($id);

    if ($ct) {
        return $ct->zaaktype_node_id->titel;
    }
    else {
        return "Verwijderd zaaktype: $id";
    }
}

sub event_category { 'case-mutation'; }

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

=head2 onderwerp

TODO: Fix the POD

=cut

