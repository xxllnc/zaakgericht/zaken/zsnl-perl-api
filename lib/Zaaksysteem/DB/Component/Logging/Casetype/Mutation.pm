package Zaaksysteem::DB::Component::Logging::Casetype::Mutation;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


=head2 onderwerp

Returns the log data in a human readable form.

=cut

sub onderwerp {
    my $self = shift;
    my $data = $self->data;

    my @msg = (sprintf("Zaaktype %s (%d) is opgeslagen", $data->{title}, $data->{case_type}));

    if ($data->{components}) {
        my $ref = ref $data->{components};
        if (!$ref) {
            push(@msg, "Aangepast component is: $data->{components}");
        }
        elsif ($ref eq 'ARRAY' && @{$data->{components}} > 1) {
            push(@msg, "Aangepaste componenten zijn: " . join(", ", @{$data->{components}}));
        }
        elsif ($ref eq 'ARRAY' && @{$data->{components}} == 1) {
            push(@msg, "Aangepast component is: $data->{components}[-1]");
        }
    }

    if ($data->{commit_message}) {
        push(@msg, "Opmerkingen: $data->{commit_message}");
    }

    return join(". ", @msg);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
