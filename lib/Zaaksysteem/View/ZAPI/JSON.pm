package Zaaksysteem::View::ZAPI::JSON;
use Zaaksysteem::Moose;

BEGIN { extends 'Catalyst::View::JSON'; }

=head1 NAME

Zaaksysteem::View::ZAPI::JSON - Return objects in a ZAPI JSON response

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

use JSON::XS (); # Empty list to prevent import of JSON::XS::encode_json
use Scalar::Util qw(blessed);
use Zaaksysteem::ZAPI::Error;
use Zaaksysteem::ZAPI::Response;

__PACKAGE__->config(
    expose_stash    => 'zapi',
);

has encoder => (
    is      => 'ro',
    isa     => 'JSON::XS',
    default => sub {
        return JSON::XS->new->utf8->canonical->pretty->allow_nonref
            ->allow_blessed->convert_blessed;
    },
    handles => { encode => 'encode' },
);

# after http://www.gossamer-threads.com/lists/catalyst/users/29706
after 'process' => sub {
    my ($self, $c) = @_;

    if (my $content_type = $c->stash->{ json_content_type }) {
        $c->res->content_type($content_type);
    }
};

=head2 encode_json

Encode the data to JSON

=cut


sub encode_json {
    my($self, $c, $data) = @_;

    $data->{result} = $self->_prepare_results($c, $data);

    no warnings 'redefine';
    local *DateTime::TO_JSON = sub { shift->iso8601 . 'Z' };

    return $self->encode($data);
}

sub _prepare_results {
    my ($self, $c, $data) = @_;

    if (ref $data eq 'HASH' && exists $data->{result}) {
        my $result = $data->{result};

        if (!blessed($result)) {
            return $result if ref $result ne 'ARRAY';

            for my $row (@$result) {
                next if ref $row ne 'HASH';
                next if !exists $row->{root} && !exists $row->{files};

                my @files;
                for my $file (@{$row->{files}}) {
                    push(@files, $self->_strip_sensitive_data_file($c, $file->TO_JSON));
                }
                $row->{files} = \@files;
            }
        }

        return $result if !blessed($result);

        if ($result->can('TO_JSON')) {
            my $data = $result->TO_JSON;

            my @rows;
            if (ref($data) eq 'ARRAY') {
                for my $row (@$data) {
                    push(@rows, $self->_strip_sensitive_data($c, $row));
                }

                return \@rows;
            }

            return $self->_strip_sensitive_data($c, $data);
        }
        elsif ($result->isa('DBIx::Class::ResultSet') || $result->isa('Zaaksysteem::Object::Iterator')) {
            $result->reset;

            my @rows;
            while (defined(my $entry = $result->next)) {
                push(@rows, $self->_strip_sensitive_data($c, $entry->TO_JSON));
            }
            return \@rows;
        }
        else {
            return $result;
        }
    }
    return $data;
}

my @file_keys = qw(
    accepted
    active_version
    case_documents
    confidential
    date_deleted
    deleted_by
    destroyed
    directory_id
    filestore_id
    id
    name
    uuid
);

sub _strip_sensitive_data_file {
    my ($self, $c, $json) = @_;

    delete $json->{search_index};
    delete $json->{search_order};
    delete $json->{search_term};
    return $json if ($c->user_exists && $c->user->is_admin);

    return $json if !$c->stash->{zapi_hide_mappings} || !$json->{confidential};

    my %json = map { $_ => $json->{$_} } @file_keys;
    $json{locked_timestamp} = DateTime->now();
    $json{locally_editable} = \0;
    return \%json;
}

sub _strip_sensitive_data {
    my $self  = shift;
    my $c     = shift;
    my $json  = shift;

    return $json if ($json->{object_type} // '') ne 'case';
    return $json if ($c->user_exists && $c->user->has_legacy_permission('view_sensitive_data'));

    my $mapping = $c->stash->{zapi_hide_mappings} // {};
    return $json if !%$mapping;

    my %values = %{$json->{values}};
    foreach my $attr (keys %values) {
        if (exists $mapping->{$attr}) {
            $values{$attr} = "Geen rechten";
        }
    }
    $json->{values} = \%values;
    return $json;
}

__PACKAGE__->meta->make_immutable(inline_constructor => 0);

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
