=head1 NAME

Zaaksysteem::Manual - Finding your way through our documentation

=head1 Description

The complexity of zaaksysteem makes it hard to understand it. This is why every code is accompanied
with documentation.

Our documentation is structured in three ways: API documentation, Module documentation and documentation
sorted by subject.

For now publicly availabled: B<API Documentation>

=head1 API Documentation

The information below will bring together all the API documentation. Before you jump
to your favourite subject, make sure that you have read the general API manual.

=head2 L<Zaaksysteem::Manual::API>

Here you will find all the information you need to succesfully "speak" to your Zaaksysteem.
You will learn how to introduce yourself (Authenticating) and how you should interpret the
output and errors from our ZAPI (Zaaksysteem API) system.

=head1 See also

L<Zaaksysteem>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
