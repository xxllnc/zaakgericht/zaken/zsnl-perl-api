package Zaaksysteem::Object::Types::Casetype::Authorisation;

use Moose;
use namespace::autoclean;

use Moose::Meta::Attribute;

use List::Util qw[first];

use BTTW::Tools;
use Zaaksysteem::Types qw[JSONBoolean];

extends 'Zaaksysteem::Object';

=head1 NAME

Zaaksysteem::Object::Types::Casetype::Authorisation - Authorisation object

=head1 DESCRIPTION

Casetype authorisation object

=head1 ATTRIBUTES

=head2 right

Description of a right

=cut

has right => (
    is => 'rw',
    isa => 'Str',
    label => 'Right',
    traits => [qw[OA]],
    required => 1,
);

=head2 role

A L<Zaaksysteem::Object::Types::Role>

=cut

has role => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Types::Role',
    label => 'Role',
    traits => [qw[OR]],
    required => 1,
);

=head2 group

A L<Zaaksysteem::Object::Types::Group>

=cut

has group => (
    is => 'rw',
    isa => 'Zaaksysteem::Object::Types::Group',
    label => 'Group',
    traits => [qw[OR]],
    required => 1,
);

=head2 confidential

Boolean indicating that the authorisation is meant for confidentiality

=cut

has confidential => (
    is => 'rw',
    isa => JSONBoolean,
    label => 'Confidential',
    traits => [qw[OA]],
    coerce => 1,
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
