package Zaaksysteem::Object::AttributeRole::DaysLeadTime;

use Moose::Role;

with 'Zaaksysteem::Object::AttributeRole';

=head1 NAME

For an open case: Empty
For a closed case: Number of days the case has ran.

=cut

sub _calculate_value {
    my $self = shift;

    my $startdate  = $self->_get_attribute('case.date_of_registration')->value;
    my $completion = $self->_get_attribute('case.date_of_completion')->value;

    return blessed($completion) ?
        abs $completion->delta_days($startdate)->in_units('days') :
        '';
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

