package Zaaksysteem::Object::Attribute::Datestamp;

use Moose::Role;

use DateTime::Format::ISO8601;

=head2 _build_human_value

On our Text role, the value is identical to human_value, unless overridden

=cut

sub _build_human_value {
    my $self = shift;

    if (ref $self->value eq 'ARRAY') {
        return [map { $self->_apply_format($_) } @{ $self->value }];
    }
    else {
        return $self->_apply_format($self->value);
    }
}

sub _apply_format {
    my $self = shift;
    my ($value) = @_;

    # Explicitly return undef, so there's always a value.
    return unless $value;
    return $self->_convert_value($value)->strftime('%d-%m-%Y');
}

sub _convert_value {
    my ($self, $value) = @_;

    return undef unless $value;
    unless (blessed $value) {
        $value = $self->_parse_datetime($value);
    }
    my $dt = $value->clone->set_time_zone('floating');
    $dt->truncate(to => 'day');
}

sub _build_index_value {
    my $self = shift;
    my $value = shift;

    return unless $value;
    return $self->_convert_value($value);
}

sub _parse_datetime {
    my $self = shift;

    return DateTime::Format::ISO8601->parse_datetime(shift);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

