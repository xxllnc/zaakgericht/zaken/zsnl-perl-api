package Zaaksysteem::Object::Queue::Model::Admin;

use Moose::Role;

use BTTW::Tools;
use Zaaksysteem::EventLog::Model;
use Zaaksysteem::Export::Model;
use Zaaksysteem::Object::Model;
use Zaaksysteem::Admin::PermissionMatrix;
use Zaaksysteem::Export::CSV;

=head1 NAME

Zaaksysteem::Object::Queue::Model::Admin - Admin queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 export

=cut

sig logbook_export => 'Zaaksysteem::Backend::Object::Queue::Component';

sub logbook_export {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;
    my $user = $self->find_subject($data->{user_id});

    my $om = Zaaksysteem::Object::Model->new(
        table  => 'ObjectData',
        schema => $self->schema,
        user   => $user,
    );

    my $model = Zaaksysteem::EventLog::Model->new(
        schema           => $self->schema,
        object_model     => $om,
        subject_model    => $self->subject_model,
        betrokkene_model => $self->schema->resultset('ZaakBetrokkenen'),
        user             => $user,
        zs_version       => $self->zs_version,
        environment      => $self->environment,
    );

    my $results = $model->search($data->{export_params});

    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        objects           => $results,
        format            => $data->{format} // 'csv',
        direct            => 1,
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    my $filename = $export->generate_filename('logboek', 'csv');
    $export->add_as_export($fh, $filename);

    return;
}

sub user_mgt_export {
    my $self = shift;
    my $item = shift;

    my $data = $item->data;
    my $user = $self->find_subject($data->{user_id});

    my $model = Zaaksysteem::Admin::PermissionMatrix->new(
        schema           => $self->schema,
        subject_model    => $self->subject_model,
        user             => $user,
        zs_version       => $self->zs_version,
        environment      => $self->environment,
        type             => $data->{type},
    );

    my $results = $model->search();

    # TODO: Rename to Export::Format
    my $formatting = Zaaksysteem::Export::CSV->new(
        user              => $user,
        objects           => $results,
        format            => $data->{format},
        no_header         => 1,
    );

    $formatting->process;

    my $fh = $formatting->to_filehandle();

    my $export = Zaaksysteem::Export::Model->new(
        schema     => $self->schema,
        user       => $user,
        request_id => $item->metadata->{request_id},
        uri        => URI->new_abs('/exportqueue', $self->base_uri),
    );

    my %mapping = (
        user_role           => "gebruikers-rollen",
        user_permission     => "gebruikers-rechten",
        casetype_role       => "zaaktype-rollen",
        casetype_permission => "zaaktype-rechten",
    );

    my $filename = $export->generate_filename(
        $mapping{$model->type},
        $data->{format} eq 'calc' ? 'ods' : $data->{format},
    );
    $export->add_as_export($fh, $filename);
    return;

}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
