package Zaaksysteem::Object::Queue::Model::Bedrijf;
use Moose::Role;

requires qw(build_resultset);

use BTTW::Tools;
use Zaaksysteem::BR::Subject;
use Zaaksysteem::BR::Subject::Constants qw(
    REMOTE_SEARCH_MODULE_NAME_KVKAPI
    REMOTE_SEARCH_MODULE_NAME_OVERHEIDIO
);


with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Queue::Model::Bedrijf - Bedrijf queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 disable_bedrijf

Disable natuurlijk personen

=head2 enable_bedrijf

Enable natuurlijk personen

=cut

sub _get_bedrijf {
    my ($self, $id) = @_;

    my $rs = $self->build_resultset('Bedrijf');

    my $company = $rs->find($id);
    return $company if $company;

    throw(
        "queue/model/bedrijf/company/not_found",
        "Unable to find company with ID $id"
    );
}

sig disable_bedrijf => 'Zaaksysteem::Backend::Object::Queue::Component';

sub disable_bedrijf {
    my ($self, $item) = @_;

    my $company = $self->_get_bedrijf($item->data->{bedrijf_id});
    $company->disable_bedrijf;

    return 1;
}

sig enable_bedrijf => 'Zaaksysteem::Backend::Object::Queue::Component';

sub enable_bedrijf {
    my ($self, $item) = @_;

    my $company = $self->_get_bedrijf($item->data->{bedrijf_id});
    $company->enable_bedrijf;
    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
