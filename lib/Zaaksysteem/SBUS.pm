package Zaaksysteem::SBUS;

use strict;
use warnings;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;

use Data::Serializer;

use Params::Profile;
use Data::Dumper;

use Moose;
use namespace::autoclean;

with
    'Zaaksysteem::SBUS::StoreRole',
    'Zaaksysteem::SBUS::Response',
    'Zaaksysteem::SBUS::Request',
    'Zaaksysteem::SBUS::Dispatcher::Soap',
    'Zaaksysteem::SBUS::Convenient';


=head1 NAME

Zaaksysteem::SBUS - Zaaksysteem ServiceBus, for importing and querying
different koppelingen.

=head1 SYNOPSIS

    my $sbus            = $c->model('SBUS');

    my $xml_response    = $sbus->response(
        $c
        {
            operation   => 'kennisgeving',
            sbus_type   => 'StUF',
            object      => 'PRS',
            input       => $XmlCompileReader_object,
            input_raw   => $c->stash->{soap}->envelope(),
        }
    )

    $c->stash->{soap}->compile_return(
        $xml_response
    )

=head1 DESCRIPTION

The ServiceBus is able to parse external requests and dispatch them to the
necessary 'gegevensmagazijn' tables.

It is able to retrieve information from other companies by requesting
information with soap calls, or it can parse incoming information.

See L<Zaaksysteem::SBUS::Response> for information about processing INCOMING data
See L<Zaaksysteem::SBUS::Request> for requesting data from other companies

=cut

has [qw/config customer schema log die_on_error fake_transport/] => (
    'is'        => 'rw',
);

=head2 dispatch

Arguments: $dispatcher_name, \%options

Dispatch Service Bus call to dispatcher, e.g. Soap, see
L<Zaaksysteem::SBUS::Dispatcher::Soap>

=cut

sub dispatch {
    my $self        = shift;
    my $dispatcher  = shift;

    die('Dispatcher: ' . $dispatcher . ' not found, make sure '
        . __PACKAGE__ . '::Dispatcher::'
        . ucfirst($dispatcher) . ' exists'
    ) unless $self->can('_dispatch_' . $dispatcher);

    my $sub         = '_dispatch_' . $dispatcher;

    $self->$sub(@_);
}

=head1 PRIVATE METHODS

=head2 _serialize

Arguments: $value

Return value: $serialized_value

Serializes a value (like a hashref) with Storable and returns serialized
object

=cut

sub _serialize {
    my ($self, $value) = @_;

    my $obj = Data::Serializer->new(
        serializer  => 'Storable',
    );

    return $obj->serialize($value);
}

=head2 _deserialize

Arguments: $serialized_value

Return value: $value

Returns the raw value which is serialized by Storable

=cut

sub _deserialize {
    my ($self, $value) = @_;

    my $obj = Data::Serializer->new(
        serializer  => 'Storable',
    );

    return $obj->deserialize($value);
}

=head2 _register_traffic

Arguments: \%create_parameters

Return value: $new_row

Returns the traffic_row from table SbusTraffic, serializes any params which
are a reference to like a HASH or ARRAY

=cut


sub _register_traffic {
    my ($self, $params) = @_;

    my $SBUS_TRAFFIC_PARAMS = SBUS_TRAFFIC_PARAMS;

    my $create  = {};
    for my $param (@{ $SBUS_TRAFFIC_PARAMS }) {
        if (ref($params->{$param})) {
            $create->{$param} = $self->_serialize($params->{$param});
            next;
        }
        $create->{$param} = $params->{$param};
    }

    return $self->schema->resultset('SbusTraffic')->create($create);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 SBUS_TRAFFIC_PARAMS

TODO: Fix the POD

=cut

