package Zaaksysteem::Moose::Role::SubjectModel;
use Moose::Role;

=head1 NAME

Zaaksysteem::Moose::Role::SubjectModel - A Subject Model role for models

=head1 DESCRIPTION

=head1 SYNOPSIS

    package Foo;
    with 'Zaaksysteem::Moose::Role::SubjectModel';

=cut

has subject_model => (
    is        => 'ro',
    isa       => 'Zaaksysteem::BR::Subject',
    predicate => 'has_subject_model',
);


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
