# Please see dev-bin/docker-perl for building and pushing this base
# layer
FROM perl:slim-stretch as build

ENV DEBIAN_FRONTEND=noninteractive \
    NO_NETWORK_TESTING=1

COPY ./dev-bin/cpanm /usr/local/bin/docker-cpanm

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get install -y --no-install-recommends \
      desktop-file-utils \
      gcc \
      git \
      ca-certificates \
      gpg \
      libc6-dev \
      libexpat1-dev \
      libmagic-dev \
      libpq-dev \
      #libssl-dev \
      openssl \
      zlib1g-dev \
# libssl1.0-dev is required, because Crypt::OpenSSL::{X509,RSA,VerifyX509} do not
# support libssl1.1 yet.
# It can be removed once
# https://github.com/dsully/perl-crypt-openssl-x509/issues/53 and related bugs
# for the other projects are fixed.
      libssl1.0-dev \
      libxml2-dev \
      locales \
      poppler-utils \
      shared-mime-info \
      unzip \
      uuid-dev \
      xmlsec1 \
      curl \
      pdftk \
  && localedef -i nl_NL -c -f UTF-8 \
    -A /usr/share/locale/locale.alias nl_NL.UTF-8
# Zaaksysteem's own modules and deps related to those
RUN docker-cpanm --notest IPC::System::Simple
RUN docker-cpanm File::ShareDir::Install
# IO::Socket::SSL borks when Net::SSLeay isn't installed
RUN docker-cpanm Net::SSLeay~'>=1.46'
RUN docker-cpanm LWP::Protocol::https
RUN docker-cpanm DBIx::Class
RUN docker-cpanm https://gitlab.com/waterkip/file-archivableformats.git@v1.5.3
RUN docker-cpanm https://gitlab.com/zaaksysteem/bttw-tools.git@v0.011
RUN docker-cpanm https://gitlab.com/zaaksysteem/syzygy.git@v0.005
RUN docker-cpanm https://gitlab.com/zaaksysteem/zaaksysteem-instance_config.git
# CPAN modules with issues
RUN docker-cpanm Catalyst::Runtime
# Systems under load may not have accurate timing differences, and thus test
# fail. It has no dependencies, but just in case.
RUN docker-cpanm --installdeps Time::Warp
RUN docker-cpanm --notest      Time::Warp
# Some builds failed due to xs errors and the build after it succeeds
# but consecutive builds after that broke due to timing out the build by
# taking longer than half an hour for it to be tested. Unsure what is
# hapenning, running it with --notest seems to fix this timeout issue
RUN docker-cpanm --installdeps Net::AMQP::RabbitMQ
RUN docker-cpanm --notest      Net::AMQP::RabbitMQ
# Class::C3 installed because there is some dependency resolution thing
# with Catalyst::Plugin::Params::Profile in the cpanfile.
RUN docker-cpanm Class::C3
# GnuPG::Interface requires patches which are found in the bug report,
# but haven't been merged upstream, so we don't require the testsuite
# right now: https://rt.cpan.org/Public/Bug/Display.html?id=102651
RUN docker-cpanm --installdeps GnuPG::Interface
RUN docker-cpanm --notest      GnuPG::Interface
# 2018-01-11: Compile::WSDL11 is a dependency of Catalyst::Controller::SOAP
# but it has not been declared in the META.json. we potentionally could end up
# that the dependecy tree does not notice it and starts building C::C::SOAP
# before Compile::WSDL11
# https://rt.cpan.org/Public/Bug/Display.html?id=95327
RUN docker-cpanm XML::Compile::WSDL11
# Make sure CGI::Simple is installed at a version which deals with it
# dependencies
RUN docker-cpanm CGI::Simple~'>=1.14'
# 2018-03-08: Catalyst::Plugin::Static::Simple has a testsuite bug. See
# https://rt.cpan.org/Public/Bug/Display.html?id=124211 for more info on the fault
RUN docker-cpanm --installdeps Catalyst::Plugin::Static::Simple
RUN docker-cpanm --notest      Catalyst::Plugin::Static::Simple
RUN docker-cpanm Class::Accessor::Fast
# Manual addition of a module that isn't indexed on CPAN and creates
# issues when you build via a local mirror, dep of Net::OpenStack::Swift
RUN curl -q \
        http://mirror.nl.leaseweb.net/CPAN/authors/id/M/MK/MKODERER/Sys-CPU-0.52.tar.gz \
        --output Sys-CPU-0.52.tar.gz \
    && echo "34305423e86cfca9a631b6f91217f90f  Sys-CPU-0.52.tar.gz" \
        | md5sum -c - \
    && docker-cpanm Sys-CPU-0.52.tar.gz
# FluentD logger
RUN docker-cpanm Data::MessagePack::Stream@1.04
# Datetime easy fix
RUN docker-cpanm DateTime::Format::Flexible@0.33
RUN docker-cpanm Catalyst::Plugin::Cache::HTTP::Preempt

# CPAN modules via cpanm
COPY cpanfile .
RUN docker-cpanm --installdeps .

FROM perl:slim-stretch as release
ENV DEBIAN_FRONTEND=noninteractive \
    NO_NETWORK_TESTING=1

RUN apt-get update \
  && apt-get upgrade -y \
  && apt-get --no-install-recommends -y install \
     ca-certificates \
     curl \
     desktop-file-utils \
     gpg \
     libexpat1 \
     libmagic1 \
     libpq5 \
     libssl1.1 \
     locales \
     openssl \
     pdftk \
     poppler-utils \
     shared-mime-info \
     unzip \
     uuid-runtime \
     xmlsec1 \
  && localedef -i nl_NL -c -f UTF-8 \
    -A /usr/share/locale/locale.alias nl_NL.UTF-8 \
  && apt-get autoremove --purge -yqq\
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/* ~/.cpanm \
  && groupadd -g 1001 zaaksysteem \
    && useradd -ms /bin/bash -u 1001 -g 1001 zaaksysteem -d /opt/zaaksysteem \
    && chown -R zaaksysteem /opt/zaaksysteem

COPY --from=build /usr/local/lib/perl5/ /usr/local/lib/perl5/

RUN perl -MPandoc::Release -E 'get(q{2.2.1}, verbose => 1)->download(bin => q{/opt/local/pandoc}, verbose => 1)->symlink(q{/usr/bin}, verbose => 1)'

WORKDIR /opt/zaaksysteem
