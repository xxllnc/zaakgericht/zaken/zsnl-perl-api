#!/usr/bin/perl

use FindBin;
use lib "$FindBin::Bin/../t/inc/";

use strict;
use warnings;

use Zaaksysteem::Soaptester;

use constant TEST_XML   => {
    'prs'       => {
        'bv01'      => {
            'default'   =>
qq{<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <StUF:bevestigingsBericht xmlns:StUF="http://www.egem.nl/StUF/StUF0204" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <StUF:stuurgegevens xmlns="http://www.egem.nl/StUF/StUF0204">
        <StUF:berichtsoort>Bv01</StUF:berichtsoort>
        <StUF:entiteittype>PRS</StUF:entiteittype>
        <StUF:sectormodel>BG</StUF:sectormodel>
        <StUF:versieStUF>0204</StUF:versieStUF>
        <StUF:versieSectormodel>0204</StUF:versieSectormodel>
        <StUF:zender>
          <StUF:applicatie>CGM</StUF:applicatie>
        </StUF:zender>
        <StUF:ontvanger>
          <StUF:applicatie>ZSNL</StUF:applicatie>
        </StUF:ontvanger>
        <StUF:referentienummer>MK0000170479</StUF:referentienummer>
        <StUF:tijdstipBericht>2013030515280431</StUF:tijdstipBericht>
        <StUF:bevestiging>
          <StUF:crossRefNummer>250</StUF:crossRefNummer>
        </StUF:bevestiging>
      </StUF:stuurgegevens>
    </StUF:bevestigingsBericht>
  </soapenv:Body>
</soapenv:Envelope>
}
        },
        'la01'      => {
            'multi' =>
qq{<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <SectorModel:synchroonAntwoordBericht xmlns:SectorModel="http://www.egem.nl/StUF/sector/bg/0204" xmlns:StUF="http://www.egem.nl/StUF/StUF0204" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <StUF:stuurgegevens xmlns="http://www.egem.nl/StUF/StUF0204">
        <StUF:berichtsoort>La01</StUF:berichtsoort>
        <StUF:entiteittype>PRS</StUF:entiteittype>
        <StUF:sectormodel>BG</StUF:sectormodel>
        <StUF:versieStUF>0204</StUF:versieStUF>
        <StUF:versieSectormodel>0204</StUF:versieSectormodel>
        <StUF:zender>
          <StUF:applicatie>CGM</StUF:applicatie>
        </StUF:zender>
        <StUF:ontvanger>
          <StUF:applicatie>ZSNL</StUF:applicatie>
        </StUF:ontvanger>
        <StUF:referentienummer>MK0000170605</StUF:referentienummer>
        <StUF:tijdstipBericht>2013030516205148</StUF:tijdstipBericht>
        <StUF:antwoord>
          <StUF:indicatorVervolgvraag>true</StUF:indicatorVervolgvraag>
          <StUF:crossRefNummer>10</StUF:crossRefNummer>
        </StUF:antwoord>
      </StUF:stuurgegevens>
      <SectorModel:body xmlns="http://www.egem.nl/StUF/sector/bg/0204">
        <SectorModel:PRS StUF:sleutelGegevensbeheer="9111012697086" StUF:sleutelVerzendend="9111012697086" soortEntiteit="F">
          <SectorModel:a-nummer StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:bsn-nummer>987654321</SectorModel:bsn-nummer>
          <SectorModel:voornamen StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:voorletters>A</SectorModel:voorletters>
          <SectorModel:voorvoegselGeslachtsnaam StUF:noValue="geenWaarde" xsi:nil="true" />
          <SectorModel:geslachtsnaam>Jansen</SectorModel:geslachtsnaam>
          <SectorModel:geboortedatum>20011223</SectorModel:geboortedatum>
          <SectorModel:geslachtsaanduiding>M</SectorModel:geslachtsaanduiding>
          <SectorModel:datumOverlijden StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:indicatieGeheim>1</SectorModel:indicatieGeheim>
          <SectorModel:burgerlijkeStaat StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:aanduidingNaamgebruik StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:tijdvakGeldigheid>
            <StUF:begindatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            <StUF:einddatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          </SectorModel:tijdvakGeldigheid>
          <SectorModel:PRSADRCOR StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
          <SectorModel:PRSADRVBL StUF:sleutelGegevensbeheer="10050717192886" StUF:sleutelVerzendend="10050717192886" soortEntiteit="R">
            <SectorModel:tijdvakRelatie>
              <StUF:begindatumRelatie>19950409</StUF:begindatumRelatie>
              <StUF:einddatumRelatie StUF:noValue="geenWaarde" xsi:nil="true" />
            </SectorModel:tijdvakRelatie>
            <SectorModel:ADR StUF:sleutelGegevensbeheer="9111012672151" StUF:sleutelVerzendend="9111012672151" soortEntiteit="F">
              <SectorModel:postcode>1051JL</SectorModel:postcode>
              <SectorModel:woonplaatsnaam>Amsterdam</SectorModel:woonplaatsnaam>
              <SectorModel:straatnaam>Donker Curtiusstraat</SectorModel:straatnaam>
              <SectorModel:huisnummer>7</SectorModel:huisnummer>
              <SectorModel:huisletter StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:huisnummertoevoeging StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:aanduidingBijHuisnummer StUF:noValue="geenWaarde" xsi:nil="true" />
            <SectorModel:gemeentecode StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            </SectorModel:ADR>
          </SectorModel:PRSADRVBL>
          <SectorModel:PRSPRSHUW StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
        </SectorModel:PRS>
        <SectorModel:PRS StUF:sleutelGegevensbeheer="9111012699973" StUF:sleutelVerzendend="9111012699973" soortEntiteit="F">
          <SectorModel:a-nummer StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:bsn-nummer>987654322</SectorModel:bsn-nummer>
          <SectorModel:voornamen StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:voorletters>B</SectorModel:voorletters>
          <SectorModel:voorvoegselGeslachtsnaam StUF:noValue="geenWaarde" xsi:nil="true" />
          <SectorModel:geslachtsnaam>Jansen</SectorModel:geslachtsnaam>
          <SectorModel:geboortedatum StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:geslachtsaanduiding StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:datumOverlijden StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:indicatieGeheim StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:burgerlijkeStaat StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:aanduidingNaamgebruik StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:tijdvakGeldigheid>
            <StUF:begindatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            <StUF:einddatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          </SectorModel:tijdvakGeldigheid>
          <SectorModel:PRSADRCOR StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
          <SectorModel:PRSADRVBL StUF:sleutelGegevensbeheer="9111107866669" StUF:sleutelVerzendend="9111107866669" soortEntiteit="R">
            <SectorModel:tijdvakRelatie>
              <StUF:begindatumRelatie>20060406</StUF:begindatumRelatie>
              <StUF:einddatumRelatie StUF:noValue="geenWaarde" xsi:nil="true" />
            </SectorModel:tijdvakRelatie>
            <SectorModel:ADR StUF:sleutelGegevensbeheer="9111012672151" StUF:sleutelVerzendend="9111012672151" soortEntiteit="F">
              <SectorModel:postcode>1051JL</SectorModel:postcode>
              <SectorModel:woonplaatsnaam>Amsterdam</SectorModel:woonplaatsnaam>
              <SectorModel:straatnaam>Donker Curtiusstraat</SectorModel:straatnaam>
              <SectorModel:huisnummer>8</SectorModel:huisnummer>
              <SectorModel:huisletter>a</SectorModel:huisletter>
              <SectorModel:huisnummertoevoeging StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:aanduidingBijHuisnummer StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:gemeentecode>363</SectorModel:gemeentecode>
            </SectorModel:ADR>
          </SectorModel:PRSADRVBL>
          <SectorModel:PRSPRSHUW StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
        </SectorModel:PRS>
        <SectorModel:PRS StUF:sleutelGegevensbeheer="9111012708233" StUF:sleutelVerzendend="9111012708233" soortEntiteit="F">
          <SectorModel:a-nummer StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:bsn-nummer>987654333</SectorModel:bsn-nummer>
          <SectorModel:voornamen StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:voorletters>C</SectorModel:voorletters>
          <SectorModel:voorvoegselGeslachtsnaam StUF:noValue="geenWaarde" xsi:nil="true" />
          <SectorModel:geslachtsnaam>Jansen</SectorModel:geslachtsnaam>
          <SectorModel:geboortedatum StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:geslachtsaanduiding StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:datumOverlijden StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:indicatieGeheim StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:burgerlijkeStaat StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:aanduidingNaamgebruik StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:tijdvakGeldigheid>
            <StUF:begindatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            <StUF:einddatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          </SectorModel:tijdvakGeldigheid>
          <SectorModel:PRSADRCOR StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
          <SectorModel:PRSADRVBL StUF:sleutelGegevensbeheer="9111214294944" StUF:sleutelVerzendend="9111214294944" soortEntiteit="R">
            <SectorModel:tijdvakRelatie>
              <StUF:begindatumRelatie>19770318</StUF:begindatumRelatie>
              <StUF:einddatumRelatie StUF:noValue="geenWaarde" xsi:nil="true" />
            </SectorModel:tijdvakRelatie>
            <SectorModel:ADR StUF:sleutelGegevensbeheer="9111012672151" StUF:sleutelVerzendend="9111012672151" soortEntiteit="F">
              <SectorModel:postcode>1051JL</SectorModel:postcode>
              <SectorModel:woonplaatsnaam>Amsterdam</SectorModel:woonplaatsnaam>
              <SectorModel:straatnaam>Donker Curtiusstraat</SectorModel:straatnaam>
              <SectorModel:huisnummer>9</SectorModel:huisnummer>
              <SectorModel:huisletter StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:huisnummertoevoeging StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:aanduidingBijHuisnummer StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:gemeentecode StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            </SectorModel:ADR>
          </SectorModel:PRSADRVBL>
          <SectorModel:PRSPRSHUW StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
        </SectorModel:PRS>
      </SectorModel:body>
    </SectorModel:synchroonAntwoordBericht>
  </soapenv:Body>
</soapenv:Envelope>
},
            single  =>
qq{<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <soapenv:Body>
    <SectorModel:synchroonAntwoordBericht xmlns:SectorModel="http://www.egem.nl/StUF/sector/bg/0204" xmlns:StUF="http://www.egem.nl/StUF/StUF0204" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <StUF:stuurgegevens xmlns="http://www.egem.nl/StUF/StUF0204">
        <StUF:berichtsoort>La01</StUF:berichtsoort>
        <StUF:entiteittype>PRS</StUF:entiteittype>
        <StUF:sectormodel>BG</StUF:sectormodel>
        <StUF:versieStUF>0204</StUF:versieStUF>
        <StUF:versieSectormodel>0204</StUF:versieSectormodel>
        <StUF:zender>
          <StUF:applicatie>CGM</StUF:applicatie>
        </StUF:zender>
        <StUF:ontvanger>
          <StUF:applicatie>ZSNL</StUF:applicatie>
        </StUF:ontvanger>
        <StUF:referentienummer>MK0000170605</StUF:referentienummer>
        <StUF:tijdstipBericht>2013030516205148</StUF:tijdstipBericht>
        <StUF:antwoord>
          <StUF:indicatorVervolgvraag>false</StUF:indicatorVervolgvraag>
          <StUF:crossRefNummer>10</StUF:crossRefNummer>
        </StUF:antwoord>
      </StUF:stuurgegevens>
      <SectorModel:body xmlns="http://www.egem.nl/StUF/sector/bg/0204">
        <SectorModel:PRS StUF:sleutelGegevensbeheer="9111012697086" StUF:sleutelVerzendend="9111012697086" soortEntiteit="F">
          <SectorModel:a-nummer StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:bsn-nummer>987654321</SectorModel:bsn-nummer>
          <SectorModel:voornamen StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:voorletters>A</SectorModel:voorletters>
          <SectorModel:voorvoegselGeslachtsnaam StUF:noValue="geenWaarde" xsi:nil="true" />
          <SectorModel:geslachtsnaam>Jansen</SectorModel:geslachtsnaam>
          <SectorModel:geboortedatum>20011223</SectorModel:geboortedatum>
          <SectorModel:geslachtsaanduiding>M</SectorModel:geslachtsaanduiding>
          <SectorModel:datumOverlijden StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:indicatieGeheim>1</SectorModel:indicatieGeheim>
          <SectorModel:burgerlijkeStaat StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:aanduidingNaamgebruik StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          <SectorModel:tijdvakGeldigheid>
            <StUF:begindatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            <StUF:einddatumTijdvakGeldigheid StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
          </SectorModel:tijdvakGeldigheid>
          <SectorModel:PRSADRCOR StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
          <SectorModel:PRSADRVBL StUF:sleutelGegevensbeheer="10050717192886" StUF:sleutelVerzendend="10050717192886" soortEntiteit="R">
            <SectorModel:tijdvakRelatie>
              <StUF:begindatumRelatie>19950409</StUF:begindatumRelatie>
              <StUF:einddatumRelatie StUF:noValue="geenWaarde" xsi:nil="true" />
            </SectorModel:tijdvakRelatie>
            <SectorModel:ADR StUF:sleutelGegevensbeheer="9111012672151" StUF:sleutelVerzendend="9111012672151" soortEntiteit="F">
              <SectorModel:postcode>1051JL</SectorModel:postcode>
              <SectorModel:woonplaatsnaam>Amsterdam</SectorModel:woonplaatsnaam>
              <SectorModel:straatnaam>Donker Curtiusstraat</SectorModel:straatnaam>
              <SectorModel:huisnummer>7</SectorModel:huisnummer>
              <SectorModel:huisletter StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:huisnummertoevoeging StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:aanduidingBijHuisnummer StUF:noValue="geenWaarde" xsi:nil="true" />
              <SectorModel:gemeentecode StUF:noValue="nietGeautoriseerd" xsi:nil="true" />
            </SectorModel:ADR>
          </SectorModel:PRSADRVBL>
          <SectorModel:PRSPRSHUW StUF:noValue="nietGeautoriseerd" soortEntiteit="R" xsi:nil="true" />
        </SectorModel:PRS>
      </SectorModel:body>
    </SectorModel:synchroonAntwoordBericht>
  </soapenv:Body>
</soapenv:Envelope>

}
        }
    }
};

use HTTP::Daemon;
use HTTP::Status;

my $d = HTTP::Daemon->new(LocalPort => 3331, Reuse => 1) || die;
print "Please contact me at: <URL:", $d->url, ">\n";

while (my $c = $d->accept) {
    while (my $r = $c->get_request) {
        print STDERR "== Accepted: " . $r->uri->path . " ==\n";
        if ($r->uri->path =~ "/stuf") {
            print STDERR "== Incoming request == \n";
            print STDERR $r->content . "\n";

            my $response = parse_stuf_request($r->content);

            print STDERR "\n\n== Outgoing request ==\n";
            print STDERR $response . "\n";

            my $ro    = HTTP::Response->new('200');
            $ro->content($response);
            $ro->header('Content-Type', 'application/soap+xml;charset=UTF-8');
            $c->send_response(
                $ro
            );
        }
        else {
            $c->send_error(RC_FORBIDDEN)
        }
    }
    $c->close;
    undef($c);
}

use constant ANSWER_MAP => {
    'prs'     => {
        'lv01'    => TEST_XML->{'prs'}->{'la01'}->{'multi'}
    }
};

sub parse_stuf_request {
    my $content         = shift;

    my ($entiteittype)  = $content =~ /entiteittype>(.*?)</;
    my ($berichtsoort)  = $content =~ /berichtsoort>(.*?)</;

    my $stuftester      = Zaaksysteem::Soaptester->new;

    if ($berichtsoort eq 'Lv01') {
        return $stuftester->prs_search(
            $content
        );
    }

    if ($berichtsoort eq 'Lv02') {
        return $stuftester->acknowledge(
            $content
        )
    }
}


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

